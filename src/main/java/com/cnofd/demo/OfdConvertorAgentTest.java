package com.cnofd.demo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfdConvertorAgent;
import com.cnofd.ofd.model.ResultInfo;

/**
 * @ClassName: OfdConvertorAgentTest
 * @Description: PDF、Word、Excel、PPT转OFD功能测试
 * @author cnofd
 * @date 2022年1月22日
 */
public class OfdConvertorAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(OfdConvertorAgentTest.class);

    private static String cfgDataBasePath;
    private static String sDataPath;
    private static String sOutPath;

    static long start = System.currentTimeMillis();

    public static void main(String[] args) {
        System.out.println("01-启动：" + System.currentTimeMillis());

        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        // 设置目录路径
        sDataPath = cfgDataBasePath + "DataPdf/";
        sOutPath = cfgDataBasePath + "Pdf2ofd/";

        // Pdf转Ofd
        pdfToOfdTest();

        // 设置目录路径
        sDataPath = cfgDataBasePath + "DataOffice/";
        sOutPath = cfgDataBasePath + "Office2Ofd/";

        // Word转Ofd
        wordToOfdTest();
        // Excel转Ofd
        excelToOfdTest();
        // PPT转Ofd
        pptToOfdTest();

        // Text转Ofd
        txtToOfdTest();

        // Html转Ofd
        htmlToOfdTest();
        // URL转Ofd
        urlToOfdTest();
    }

    /**
     * Pdf转Ofd
     */
    public static void pdfToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("05-开始PDF转OFD：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.pdf2ofd(sDataPath + "pdf-data-cnofd.pdf", sOutPath + "pdf-data-cnofd.ofd");
        logger.info("05-结束PDF转OFD：" + (System.currentTimeMillis() - start) + " " + result.getMessage());
        
        logger.info("05-开始PDF转OFD：" + (System.currentTimeMillis() - start));
        InputStream is = null;
        OutputStream os = null;
        try {
            is = new FileInputStream(sDataPath + "pdf-data-cnofd.pdf");
            os = new FileOutputStream(new File(sOutPath + "pdf-data-cnofd-2.ofd"));
            
            result = OfdConvertorAgent.pdf2ofd(is, os);
        } catch (FileNotFoundException e) {
            logger.error("open file failed", e);
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                logger.error("close FileInputStream failed", e);
            }
            try {
                if (os != null) {
                    os.close();
                }
            } catch (IOException e) {
                logger.error("close FileOutputStream failed", e);
            }            
        }
        logger.info("05-结束PDF转OFD：" + (System.currentTimeMillis() - start) + " " + result.getMessage());
    }

    /**
     * Word转Ofd
     */
    public static void wordToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("06-开始Word转Ofd：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.word2ofd(sDataPath + "word-data-test.doc", sOutPath + "word-data-test.ofd");
        logger.info("06-结束Word转Ofd：" + (System.currentTimeMillis() - start) + " " + result.getMessage());
    }

    /**
     * Excel转Ofd
     */
    public static void excelToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("07-开始Excel转Ofd：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.excel2ofd(sDataPath + "excel-data-test.xlsx", sOutPath + "excel-data-test.ofd");
        logger.info("07-结束Excel转Ofd：" + (System.currentTimeMillis() - start) + " " + result.getMessage());
    }

    /**
     * PPT转Ofd
     */
    public static void pptToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("08-开始PPT转Ofd：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.ppt2ofd(sDataPath + "ppt-data-test.pptx", sOutPath + "ppt-data-test.ofd");
        logger.info("08-结束PPT转Ofd：" + (System.currentTimeMillis() - start) + " " + result.getMessage());
    }

    /**
     * Text转Ofd
     */
    public static void txtToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("10-开始Text转OFD：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.txt2ofd(sDataPath + "text-data-test.txt", sOutPath + "text-data-test.ofd");
        logger.info("10-结束Text转OFD：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * Html转Ofd
     */
    public static void htmlToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("10-开始Html转OFD：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.html2ofd(sDataPath + "html-data-test.html", sOutPath + "html-data-test.ofd");
        logger.info("10-结束Html转OFD：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * URL转Ofd
     */
    public static void urlToOfdTest() {
        ResultInfo result = new ResultInfo();

        logger.info("12-开始URL转OFD：" + (System.currentTimeMillis() - start));
        result = OfdConvertorAgent.url2ofd("https://baidu.com/", sOutPath + "url-data-baidu.ofd", true);
        logger.info("12-结束URL转OFD：" + (System.currentTimeMillis() - start) + result.getMessage());
    }
}