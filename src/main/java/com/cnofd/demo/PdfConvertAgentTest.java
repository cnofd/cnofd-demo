package com.cnofd.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.PdfConvertorAgent;
import com.cnofd.ofd.model.ResultInfo;

/**
 * 
 * @ClassName: PdfConvertAgentTest
 * @Description: Pdf转图片、文本和Html功能测试；文本、Html和Url转Pdf功能测试
 * @author cnofd
 * @date 2022年5月4日
 */
public class PdfConvertAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(PdfConvertAgentTest.class);

    private static String cfgDataBasePath;
    private static String sDataPath;
    private static String sOutPath;

    static long start = System.currentTimeMillis();

    public static void main(String[] args) {
        logger.info("01-启动：" + System.currentTimeMillis());

        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        sDataPath = cfgDataBasePath;
        sOutPath = cfgDataBasePath;

        // //////////////////////////////////
        // PDF转图片
        pdfToImageTest();

        // PDF转文本
        pdfToTextTest();

        // PDF转Html
        pdfToHtmlTest();

        // PDF转Word
        pdfToWordTest();

        // //////////////////////////////////
        sDataPath = cfgDataBasePath + "DataOffice/";
        sOutPath = cfgDataBasePath + "Office2Pdf/";

        // 多张图片合成Pdf
        imageToPdfTest();

        // Word转Pdf
        wordToPdfTest();
        // Excel转Pdf
        excelToPdfTest();
        // PPT转Pdf
        pptToPdfTest();

        // Text转Pdf
        txtToPdfTest();
        // Html转Pdf
        htmlToPdfTest();
        // URL转Pdf
        urlToPdfTest();

    }

    /*
     * PDF转图片
     */
    public static void pdfToImageTest() {
        long start = System.currentTimeMillis();
        logger.info("01-PDF转图片：" + System.currentTimeMillis());

        sDataPath = "testdata/DataPdf/";
        sOutPath = "testdata/Pdf2Image/";

        try {
            ResultInfo result = new ResultInfo();

            // PDF转图片-Jpg
            logger.info("02-开始PDF转图片：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2image(sDataPath + "cnofd-result.pdf", sOutPath + "PDF2JPG", "JPG");
            logger.info("02-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转图片-指定DPI
            logger.info("03-开始PDF转图片：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2image(sDataPath + "cnofd-result.pdf", sOutPath + "PDF2JPG-150", "JPG", 150);
            logger.info("03-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转图片-Png
            logger.info("04-开始PDF转图片：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2image(sDataPath + "cnofd-result.pdf", sOutPath + "PDF2PNG", "PNG");
            logger.info("04-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转图片-Bmp
            logger.info("05-开始PDF转图片：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2image(sDataPath + "cnofd-result.pdf", sOutPath + "PDF2BMP", "BMP");
            logger.info("05-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转图片-Gif
            logger.info("06-开始PDF转图片：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2image(sDataPath + "cnofd-result.pdf", sOutPath + "PDF2GIF", "GIF");
            logger.info("06-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转图片-Tiff
            logger.info("07-开始PDF转图片：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2image(sDataPath + "cnofd-result.pdf", sOutPath + "PDF2TIFF", "TIFF");
            logger.info("07-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转多页Tiff
            logger.info("08-开始PDF转多页Tiff：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2tiff(sDataPath + "cnofd-result.pdf", sOutPath + "cnofd-result-150.tif", 150);
            logger.info("08-结束PDF转多页Tiff：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转长图-Jpg
            logger.info("02-开始PDF转长图：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2longimage(sDataPath + "ppt-data-test.pdf", sOutPath + "ppt-data-test-longimage-150.jpg", "JPG", 150);
            logger.info("02-结束PDF转长图：" + (System.currentTimeMillis() - start) + result.getMessage());

            // PDF转Gif动图
            logger.info("02-开始PDF转Gif动图：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2gif(sDataPath + "ppt-data-test.pdf", sOutPath + "ppt-data-test-gif-150.gif", 2000, 150);
            logger.info("02-结束PDF转Gif动图：" + (System.currentTimeMillis() - start) + result.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /*
     * PDF转Text
     */
    public static void pdfToTextTest() {
        long start = System.currentTimeMillis();
        logger.info("01-PDF转Text：" + System.currentTimeMillis());

        sDataPath = "testdata/DataPdf/";
        sOutPath = "testdata/Pdf2Text/";

        try {
            ResultInfo result = new ResultInfo();

            logger.info("02-开始PDF转Text：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2txt(sDataPath + "cnofd-result.pdf", sOutPath + "cnofd-result.txt");
            logger.info("02-结束PDF转Text：" + (System.currentTimeMillis() - start) + result.getMessage());

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /*
     * PDF转Html
     */
    public static void pdfToHtmlTest() {
        long start = System.currentTimeMillis();
        logger.info("01-PDF转Html：" + System.currentTimeMillis());

        sDataPath = "testdata/DataPdf/";
        sOutPath = "testdata/Pdf2Html/";

        try {
            ResultInfo result = new ResultInfo();
            // PDF转Html
            logger.info("02-开始PDF转Html：" + (System.currentTimeMillis() - start));
            result = PdfConvertorAgent.pdf2html(sDataPath + "word-data-test.pdf", sOutPath + "word-data-test.html");
            logger.info("02-结束PDF转Html：" + (System.currentTimeMillis() - start) + result.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }
    
    //PDF转Word
    public static void pdfToWordTest() {
        long start = System.currentTimeMillis();
        logger.info("01-PDF转Text：" + System.currentTimeMillis());

        sDataPath = "testdata/DataPdf/";
        sOutPath = "testdata/Pdf2Word/";
        
        try {
            ResultInfo result = new ResultInfo();
    
            logger.info("02-开始PDF转Text：" + (System.currentTimeMillis() - start));           
            result = PdfConvertorAgent.pdf2word(sDataPath + "cnofd-result.pdf", sOutPath + "cnofd-result.docx");
            logger.info("02-结束PDF转Text：" + (System.currentTimeMillis() - start) + result.getMessage());
    
       } catch (Exception e) {
            e.printStackTrace();
        }finally{
            //
        }        
    }
    
    /**
     * 多张图片合成Pdf
     */
    public static void imageToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("09-开始多张图片合成Pdf：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.image2pdf("testdata/Pdf2Image/PDF2JPG-150/", sOutPath + "image2pdf-jpg.pdf");
        logger.info("09-结束多张图片合成Pdf：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * Word转Pdf
     */
    public static void wordToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("03-开始Word转PDF：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.word2pdf(sDataPath + "word-data-test.doc", sOutPath + "word-data-test.pdf");
        logger.info("03-结束Word转PDF：" + (System.currentTimeMillis() - start) + " " + result.getMessage());
    }

    /**
     * Excel转Pdf
     */
    public static void excelToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("03-开始Excel转PDF：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.excel2pdf(sDataPath + "excel-data-test.xlsx", sOutPath + "excel-data-test.pdf");
        logger.info("03-结束Excel转PDF：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * PPT转Pdf
     */
    public static void pptToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("03-开始PPT转PDF：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.ppt2pdf(sDataPath + "ppt-data-test.pptx", sOutPath + "ppt-data-test.pdf");
        logger.info("03-结束PPT转PDF：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * Text转Pdf
     */
    public static void txtToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("09-开始Text转PDF：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.txt2pdf(sDataPath + "text-data-test.txt", sOutPath + "text-data-test.pdf");
        logger.info("09-结束Text转PDF：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * Html转Pdf
     */
    public static void htmlToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("09-开始Html转PDF：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.html2pdf(sDataPath + "html-data-test.html", sOutPath + "html-data-test.pdf");
        logger.info("09-结束Html转PDF：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /**
     * URL转Pdf
     */
    public static void urlToPdfTest() {
        ResultInfo result = new ResultInfo();

        logger.info("11-开始URL转PDF：" + (System.currentTimeMillis() - start));
        result = PdfConvertorAgent.url2pdf("https://baidu.com/", sOutPath + "url-data-baidu.pdf");
        logger.info("11-结束URL转PDF：" + (System.currentTimeMillis() - start) + result.getMessage());
    }
}