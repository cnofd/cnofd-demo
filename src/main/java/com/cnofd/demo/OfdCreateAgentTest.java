package com.cnofd.demo;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfdCreateAgent;
import com.cnofd.ofd.model.ImageBox;
import com.cnofd.ofd.model.MetaInfoModel;
import com.cnofd.ofd.model.ResultInfo;
import com.cnofd.ofd.model.ImageModel;
import com.cnofd.ofd.model.TableBox;
import com.cnofd.ofd.model.TemplateInfoModel;
import com.cnofd.ofd.model.TemplatePageModel;
import com.cnofd.ofd.model.TextBox;

/**
 * @ClassName: OfdCreateAgentTest
 * @Description: OFD生成功能测试
 * @author cnofd
 * @date 2022年1月22日
 */
public class OfdCreateAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(OfdCreateAgentTest.class);

    private static String cfgDataBasePath;
    private static String sDataPath;
    private static String sOutPath;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        logger.info("01-启动：" + System.currentTimeMillis());

        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        sDataPath = cfgDataBasePath;
        sOutPath = cfgDataBasePath + "OfdCreate/";

        // 基于底图文件生成OFD（以结婚证为例，单一A4版面）
        ofdCreateOne();

        // 基于图片生成OFD文件
        OfdCreateByImage();

        // 套模板生成OFD文件：基于图片文件或背景色+form.xml生成底版OFD
        OfdCreateByTemplateTest1();

        // 套模板生成OFD文件：底图路径+样式生成底版OFD，form.xml内含页面尺寸和底图文件等信息，data文件采用name结构
        OfdCreateByTemplateTest2();

        // 套模板生成OFD文件：基于OFD生成底版OFD
        OfdCreateByTemplateTest3();

        // 套模板生成OFD文件：单步生成OFD文件，form.xml内含页面尺寸和底图文件信息，data文件采用name结构
        OfdCreateByTemplateTest4();

        // 套模板生成OFD文件：底版OFD复用，支持模板页复用
        OfdCreateByTemplateTest5();
        
        // 套模板生成OFD文件：加载xml文件构建信息对象，先生成底版OFD，再合成OFD文件
        OfdCreateByTemplateTest6();
        
        // 套模板生成OFD文件，构建信息对象，先生成底版OFD，再合成OFD文件
        OfdCreateByTemplateTest7();
        
        // 套模板生成OFD文件，构建信息对象，单步生成OFD文件
        OfdCreateByTemplateTest8();
        
    }

    /**
     * 基于底图文件生成OFD（以结婚证为例，单一A4版面）
     */
    public static void ofdCreateOne() {
    	String outFilename = "cnofd-result-standard.ofd";
    	
        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);
        
        try {
            ResultInfo result = new ResultInfo();

            // 背景图片
            List<ImageModel> images = new ArrayList<ImageModel>();
            images.add(new ImageModel(210f, 297f, "jpg", "1", Paths.get(sDataPath + "temp1.jpg").toAbsolutePath()));

            File fFormOfd = new File(sOutPath + "form.ofd");

            File fileParent = fFormOfd.getParentFile();
            if (!fileParent.exists()) {
                fileParent.mkdirs();
            }

            OutputStream fosFormOfd = new FileOutputStream(fFormOfd);

            logger.info("03-开始生成底版OFD：" + (System.currentTimeMillis() - start));
            // 基于图片文件生成底版OFD
            result = OfdCreateAgent.imageToTemplate(images, fosFormOfd);

            fosFormOfd.close();

            if (result.getStatus() == 0) {
                logger.info("03-结束生成底版OFD：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("03-失败生成底版OFD：" + result.getMessage());
                return;
            }

            logger.info("04-开始生成版式文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sOutPath + "form.ofd",      //底版OFD文件
                                sDataPath + "form-standard.xml",                    //表单XML文件
                                sDataPath + "data-standard.xml",                    //数据XML文件
                                "name",                                             //数据结构类型：name结构、xpath结构
                                sDataPath + "customDatas.xml",                      //元数据XML文件
                                sOutPath + outFilename);            				//输出OFD文件
            logger.info("04-结束生成版式文件：" + (System.currentTimeMillis() - start) + result.getMessage());  
            
            fFormOfd.delete();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /**
     * 基于图片文件生成OFD
     */
    public static void OfdCreateByImage() {
    	String outFilename = "cnofd-result-byimage.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            List<ImageModel> images = new ArrayList<ImageModel>();
            images.add(new ImageModel(210f, 297f, "jpg", "1", Paths.get(sDataPath + "temp1.jpg").toAbsolutePath()));
            images.add(new ImageModel(200f, 200f, "jpg", "1", Paths.get(sDataPath + "temp2.jpg").toAbsolutePath()));
            images.add(new ImageModel(297f, 210f, "255 192 0", 255)); // 背景色

            logger.info("03-开始基于图片生成版式文件：" + (System.currentTimeMillis() - start));
            // 基于图片生成OFD文件
            result = OfdCreateAgent.imageToOfd(images,                                  //图片文件
                                    sDataPath + "customDatas.xml",                      //元数据xml文件
                                    sOutPath + outFilename);             				//输出ofd文件

            if (result.getStatus() == 0) {
                logger.info("04-结束基于图片生成版式文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败基于图片生成版式文件：" + result.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /**
     * 套模板生成OFD文件：基于图片文件或背景色生成底版OFD，data文件采用xpath结构
     */
    public static void OfdCreateByTemplateTest1() {
    	String outFilename = "cnofd-result-1.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);
        
        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            List<ImageModel> images = new ArrayList<ImageModel>();
            images.add(new ImageModel(210f, 297f, "jpg", "1", Paths.get(sDataPath + "temp1.jpg").toAbsolutePath()));
            images.add(new ImageModel(200f, 200f, "jpg", "1", Paths.get(sDataPath + "temp2.jpg").toAbsolutePath()));
            /*
             * //读取背景图片文件到缓存图像对象
             * File fileImage1 = new File(sDataPath + "temp1.jpg"); 
             * BufferedImage bi1 = ImageIO.read(fileImage1);
             * images.add(new ImageModel(210f,297f, "jpg", "1", bi1));
             * 
             * File fileImage2 = new File(sDataPath + "temp2.jpg");
             * BufferedImage bi2 = ImageIO.read(fileImage2); 
             * images.add(new ImageModel(200f,200f, "jpg", "2", bi2));
             */
            images.add(new ImageModel(297f, 210f, "255 192 0", 255)); // 背景色

            File fFormOfd = new File(sOutPath + "form.ofd");
            
            File fileParent = fFormOfd.getParentFile();  
            if(!fileParent.exists()){  
                fileParent.mkdirs();  
            }

            OutputStream fosFormOfd = new FileOutputStream(fFormOfd);

            logger.info("03-开始生成底版OFD：" + (System.currentTimeMillis() - start));
            // 基于图片文件或背景色生成底版OFD
            result = OfdCreateAgent.imageToTemplate(images, fosFormOfd);

            fosFormOfd.close();

            if (result.getStatus() == 0) {
                logger.info("03-结束生成底版OFD：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("03-失败生成底版OFD：" + result.getMessage());
                return;
            }

            logger.info("04-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sOutPath + "form.ofd",          //底版OFD文件
                                    sDataPath + "form.xml",                             //表单XML文件
                                    sDataPath + "data.xml",                             //数据XML文件
                                    "xpath",                                            //数据结构类型：name结构、xpath结构
                                    sDataPath + "customDatas.xml",                      //元数据XML文件
                                    sOutPath + outFilename);                			//输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /**
     * 套模板生成OFD文件：底图路径+样式生成底版OFD，form.xml内含页面尺寸和底图文件等信息，data文件采用name结构
     */
    public static void OfdCreateByTemplateTest2() {
    	String outFilename = "cnofd-result-2.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            File fFormOfd = new File(sDataPath + "form.ofd");
            OutputStream fosFormOfd = new FileOutputStream(fFormOfd);

            Path pathForm = Paths.get(sDataPath + "formNew.xml").toAbsolutePath();
            InputStream fisFormXml = new FileInputStream(pathForm.toFile());

            logger.info("03-开始生成底版OFD：" + (System.currentTimeMillis() - start));
            // 基于底图路径+样式生成底版OFD，form.xml内含页面尺寸和底图文件等信息
            result = OfdCreateAgent.imageToTemplate(sDataPath, fisFormXml, fosFormOfd);

            fosFormOfd.close();

            if (result.getStatus() == 0) {
                logger.info("03-结束生成底版OFD：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("03-失败生成底版OFD：" + result.getMessage());
                return;
            }

            logger.info("04-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sDataPath + "form.ofd",         //底版OFD文件
                                    sDataPath + "formNew.xml",                          //表单XML文件
                                    sDataPath + "dataNew.xml",                          //数据XML文件
                                    "name",                                             //数据结构类型：name结构、xpath结构
                                    sDataPath + "customDatas.xml",                      //元数据XML文件
                                    sOutPath + outFilename);                			//输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /**
     * 套模板生成OFD文件：基于OFD生成底版OFD
     */
    public static void OfdCreateByTemplateTest3() {
    	String outFilename = "cnofd-result-3.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);
        
        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            logger.info("03-开始生成底版OFD：" + (System.currentTimeMillis() - start));
            // 基于OFD生成底版OFD
            result = OfdCreateAgent.ofdToTemplate(sDataPath + "底图+表格3.ofd", sOutPath + "form.ofd");
            // 使用福昕OFD软件，把Word打印成Ofd，已经是模板OFD，可以直接使用。

            if (result.getStatus() == 0) {
                logger.info("03-结束生成模板文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("03-失败生成模板文件：" + result.getMessage());
                return;
            }

            logger.info("04-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sOutPath + "form.ofd",          //底版OFD文件
                                sDataPath + "form.xml",                                 //表单XML文件
                                sDataPath + "data.xml",                                 //数据XML文件
                                "xpath",                                                //数据结构类型：name结构、xpath结构
                                sDataPath + "customDatas.xml",                          //元数据XML文件
                                sOutPath + outFilename);                				//输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /**
     * 套模板生成OFD文件：单步生成OFD文件，form.xml内含页面尺寸和底图文件信息，data文件采用name结构
     */
    public static void OfdCreateByTemplateTest4() {
    	String outFilename = "cnofd-result-4.ofd";
    	
        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);
        
        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            logger.info("03-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 单步生成OFD文件
            result = OfdCreateAgent.createOfd(sDataPath,                                    //底图文件目录
                                        new FileInputStream(sDataPath + "formNew.xml"),     //表单XML文件（模板页复用）
                                        new FileInputStream(sDataPath + "dataNew.xml"),     //数据XML文件（name结构）
                                        "name",                                             //数据结构类型：name结构、xpath结构
                                        sDataPath + "customDatas.xml",                      //元数据XML文件
                                        sOutPath + outFilename);                			//输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /**
     * 套模板生成OFD文件：底版OFD复用，支持模板页复用
     */
    public static void OfdCreateByTemplateTest5() {
    	String outFilename = "cnofd-result-5.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            logger.info("03-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sOutPath + "form.ofd",              //底版OFD文件
                                        new FileInputStream(sDataPath + "formNew2.xml"),    //表单XML文件（模板页复用）
                                        new FileInputStream(sDataPath + "dataNew.xml"),     //数据XML文件（name结构）
                                        "name",                                             //数据结构类型：name结构、xpath结构
                                        sDataPath + "customDatas.xml",                      //元数据XML文件
                                        sOutPath + outFilename);                			//输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /*
     * 套模板生成OFD文件：底图路径+样式生成底版OFD，form.xml内含页面尺寸和底图文件等信息，data文件采用name结构
     */
    public static void OfdCreateByTemplateTest6() {
        String outFilename = "cnofd-result-6.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            // 加载form.xml，构建模板信息对象
            Path pathForm = Paths.get(sDataPath + "formNew.xml").toAbsolutePath();
            InputStream isFormXml = new FileInputStream(pathForm.toFile());
            TemplateInfoModel templateInfo = OfdCreateAgent.buildTemplateInfo(isFormXml, sDataPath);
            isFormXml.close();

            // 加载data.xml，构建数据Map键值对
            Path pathData = Paths.get((String) sDataPath + "dataNew.xml").toAbsolutePath();
            InputStream isDataXml = new FileInputStream(pathData.toFile());
            Map<String, String> mapData = OfdCreateAgent.buildDataInfo(isDataXml, "name");
            isDataXml.close();

            // 加载customDatas.xml，构建Ofd元数据信息对象
            Path pathMeta = Paths.get(sDataPath + "customDatas.xml").toAbsolutePath();
            InputStream isMetaXml = new FileInputStream(pathMeta.toFile());
            MetaInfoModel metaInfo = OfdCreateAgent.buildMetaInfo(isMetaXml);
            isMetaXml.close();

            File fFormOfd = new File(sDataPath + "form.ofd");
            OutputStream osFormOfd = new FileOutputStream(fFormOfd);

            logger.info("03-开始生成底版OFD：" + (System.currentTimeMillis() - start));
            // 基于底图路径+样式生成底版OFD
            result = OfdCreateAgent.imageToTemplate(sDataPath, templateInfo, osFormOfd);

            osFormOfd.close();

            if (result.getStatus() == 0) {
                logger.info("03-结束生成底版OFD：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("03-失败生成底版OFD：" + result.getMessage());
                return;
            }

            logger.info("04-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sDataPath + "form.ofd",        			//底版OFD文件
					            						templateInfo,                         	//模板信息
					            						mapData,                           		//数据信息
					            						metaInfo,                       		//元数据信息
					                                    sOutPath + outFilename);                //输出OFD文件
  
            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /*
     * 套模板生成OFD文件，构建信息对象，先生成底版OFD，再合成OFD文件
     */
    public static void OfdCreateByTemplateTest7() {
        String outFilename = "cnofd-result-7.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            // 构建模板信息对象
            TemplateInfoModel templateInfo = new TemplateInfoModel();
            TextBox textBox = null;
            ImageBox imageBox = null;
            TableBox tableBox = null;

	        TemplatePageModel templatePage1 = new TemplatePageModel(1, 210f, 297f, "jpg", "temp1.jpg", Paths.get(sDataPath + "temp1.jpg").toAbsolutePath());

            textBox = new TextBox(0, "/Certificate/Page/CZR", "50 76 50 10", "宋体", 14, "持证人");
            textBox.setColor("0 0 255");
            textBox.setMultiLine(true);
            textBox.setLineSpace(1);
            textBox.setFontSizeAuto(true);
            templatePage1.getListTextBox().add(textBox);

            textBox = new TextBox(1, "/Certificate/Page/DJRQ", "50 92 50 6", "楷体", 14);
            textBox.setTextAlign("center");
            textBox.setBold(true);
            templatePage1.getListTextBox().add(textBox);

            imageBox = new ImageBox(2, "/Certificate/Page/HYZP", "125 76 69 46", "合影照片");
            imageBox.setAlpha(255);
            templatePage1.getListImageBox().add(imageBox);

            templateInfo.getListTemplatePage().add(templatePage1);

            TemplatePageModel templatePage2 = new TemplatePageModel(2, 297f, 210f, "255 192 0", 255);

            textBox = new TextBox(0, "/Certificate/Page/CZR", "20 20 80 30", "宋体", 14);
            textBox.setColor("0 0 255");
            templatePage2.getListTextBox().add(textBox);

            tableBox = new TableBox(1, "/Certificate/Page/BG2", "150 20 50 40", "表格");
            tableBox.setBorderStyle("solid");
            tableBox.setRowHeight("8");
            tableBox.setColWidth("20 40 40 50");
            tableBox.setBorderWidth(0.8f);
            tableBox.setLineWidth(0.4f);
            tableBox.setColor("255 0 0");
            templatePage2.getListTableBox().add(tableBox);

            templateInfo.getListTemplatePage().add(templatePage2);

	        // 构建数据Map键值对
	        Map<String, String> mapData = new HashMap<String, String>();
	        mapData.put(1 + "/Certificate/Page/CZR", "张男很长很长很长很长的姓名");
	        mapData.put(1 + "/Certificate/Page/DJRQ", "2021年11月15日");
	        mapData.put(1 + "/Certificate/Page/HYZP", "/9j/4AAQSkZJRgABAQEAeAB4AAD/4QCyRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAE+AAUAAAACAAAAYgE/AAUAAAAGAAAAcgMBAAUAAAABAAAAolEQAAEAAAABAQAAAFERAAQAAAABAAAXEVESAAQAAAABAAAXEQAAAAAAAHomAAGGoAAAgIQAAYagAAD6AAABhqAAAIDoAAGGoAAAdTAAAYagAADqYAABhqAAADqYAAGGoAAAF3AAAYagAAGGoAAAsY//2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAFAAeADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD44+MfHxg8Wj/qN3o/8mJK5zbmuk+Mv/JYvFv/AGHL3/0oeudHSvzM/wBXMH/Ah6L8kN280bf85p1FB0DdtBWnUUANK/5zRtp1FADdtG2nUUAJ/nrSYz/+unUUANC4o/D9adRQA3bzQBgf/Xp1FACf560f560tFAxv4frQVzTqKBDcY/8A10Yz/wDrp1FADdtL/nrS0UANIzRjH/66dRQAmP8AOaTbxTqKAE/z1pMf5zTqKAGgYH/16X/PWlooAT/PWk/D9adRQMaRmjGD/wDXp1FAhv4frRt5/wDr06igBoGP/wBdGMinUUANC0v+etLRQA0rkUYx/wDrp1FACYz/APrpAtOooAT/AD1o/wA9aWigY38P1pcf5zS0UCG7aX/PWlooAbjmgDH/AOunUUAJ/nrSbf8AOadRQA3H+c0EZFOooAaRitz4YLn4meGl/wCovaf+j0rFPStv4X/8lQ8N/wDYXs//AEelHUxxP8GXo/yJ/jL/AMli8W/9hy9/9KHrnR0rovjL/wAli8W/9hy9/wDSh650dKCcJ/Ah6L8kFFFFB0BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAAelbfwv/5Kh4b/AOwvZ/8Ao9KxD0rb+F//ACVDw3/2F7P/ANHpQt/68jHE/wAGXo/yJ/jL/wAli8W/9hy9/wDSh650dK6L4y/8li8W/wDYcvf/AEoeudHSgnCfwIei/JBRRRQdAUUUUAFFFFABRRRQAUUUUAFFFFABRRQTigAJxQTikY1f8MeFdS8a6ulhpNjPqF443CKIdB6seij3JArOpVhTi51HZLdvYzq1YUoOpUaSW7ZnlqA1e0eH/wBiXXL6COTUtX03T93JjiRrhl9sghcg+hIrRn/Yan8k+V4ohaTsHsCq/mJK+bqcZZPCfI6yv83+Sa/E+TqcfZDCbhLEK/km/wAbHg5bFKDmvXtY/Yu8S2S5s9Q0jUPRSzQM3/fQ2/ma818WeBdY8B6h9l1jT5rGfsHwyN/uupKt68HNelg88wGLfLhqqk+3X8T1cv4jy3HS5cLWjJ9r6/czLooJxQDmvWPbCiiigAooooAKKKKACiiigAooooAKM0E0hORQAuaM5FN3Ub1UfeFAXVrscDmgmn2lncX3+pt55u/yRls/kK0IPBGt3ePL0jUmz/07v/hWMq9NbyX3nLUxuHp/xKkV6tGWTS1tH4aeIwN39iajt/65Gqd94Y1TTFLXGm30KryS8DgD8cYojiKT2kvvREMywk3aNWLf+Jf5lGikDZ/z0peR2ra/U7U01dBRRnmigAooooAKKKKACiiigAooooAKKKKACiiigAPStv4X/wDJUPDf/YXs/wD0elYh6Vt/C/8A5Kh4b/7C9n/6PShb/wBeRjif4MvR/kT/ABl/5LF4t/7Dl7/6UPXOjpXRfGX/AJLF4s/7Dl7/AOlD1zo6UE4P+BD0X5IKKKKDoCiiigAooooAKKKKACiiigAooooAKaetOpp+9QBe8L+Grvxn4is9KsVVru+kEcYP3QepY+wAJPsK+vfhn4c0L4QaQ2i20LW0qkNcXcq4a9fu5b09B2HvmvGP2KdEjvvHurX0ihm0+xVIsr91pHwTn12qfzr6OvbezvZxDcLDJJtyqt98A+ncfhX47x7nLni/7Pd+SKTdt7v/AC7H4H4pZ9Vni/7Mg2oRSb82119CzDdw3seYpoZB2KuDT2wmcsoC9STgCsO7+H9nMd0LTRZOeGz/ADqH/hX8I2+Zc3Eij+HgV+d/V8FL/l4/S2p+RcqHav4w8x/s+nr51xISgfHAPt6/ypt38ObHX/CF1pOrQrdR3w3XLE5ZZMYDqexXOQfXPY1qaXo9to0bfZ49pIwWJyx/GrkP3fXg054tUUlhPdtrfq3/AJGtGtUoyVSi7SWqfmfCvjDwvN4K8WalpFz802m3DQlh0cA8N+IwfxrPHSu0/aQ1CLUfjt4kkhbcqzpETjqyRorfqD+VcXX9K5XWlWwlOrPeUU39x/YmU1p1sFSrVPilFN+rWoUUUV6B6AUUUUAFFFFABRRQelABmk6Um7bXovwz+BEviOKG/wBY8y1sZBujgU7ZZx1Bz/Cvf1Pt1rlxeMpYaHPVdjys2zrC5dQdfFSsui6vyS6nDaJoF94mvRb2FrPdS9xGudo9z0H1Jr0Lw3+zRe3e1tS1CGz7mKAebJj/AHjgA/nXrGjaJZ+HtPW1sbeK1t16JGMA+57k+55q0o218bjOJK9R8tFcq/E/GM48TMdXk4YJKnHu9X/kjjtJ+AvhvSNu6zmvJAfv3MxbP/ARhf0rorDwvpulD/RdPsrcdf3cCDp05xmtEjd1orxauMr1Pjk/vPhcVnOYYh3rV5S9Ww8xgn3mAHAAOMU0sc9TRjJpduK86er1PKk237wmMHp+lAdkH3mGfQ07bzSbaS01Qlo7ox/EHgPR/FIP27T7eZif9Zt2yL9GGDXmnjb9ne40+JrjRp2vIl5NtL/rQP8AZI4b6HB+teybaay59q9PB5tiMPK8ZadmfSZPxZmWWzUqNRuP8r1T/ryPlOW2kt5njkV45I2KsGGCpHYjsRTT8pr3z4nfCS38b273FqI7fVkHySdFn/2X/wAe1eD31lNYXstvcRtDcQttkRuqEdRX6Dlea08bC8dGt0f0BwzxRhs4o81PSa3j28/NEdFHSivUPqAooooAKKKKACiiigAooooAKKKKAA9K2/hf/wAlQ8N/9hez/wDR6ViHpW38L/8AkqHhv/sL2f8A6PShb/15GOJ/gy9H+RP8Zf8AksXi3/sOXv8A6UPXOjpXRfGX/ksXi3/sOXv/AKUPXOjpQTg/4EPRfkgooooOgKKKKACiiigAooooAKKKKACiikzxQAtI3FGaQtg+mTge/wDnNTJpasLpas9u/Yev44vF/iCzZsNcWUcic9djkH/0IGvfte8Pf2rGskb+TdRcxyDv6A180/sveGfEWk/E3T9Xj0bUv7LZJIbmYwmNDGyHGC2M/OFPFfTlr4ls7l9vnLFNnBjkGxgfoa/B+OYyjm7r4eSd0tnfyaZ/M3iXGn/bTq05KSlFPR31St0KOjeKSrm21L9zcKcBiPlf6/4962pGwisuDu7jmq2qaPBqtvtlj3ehHDD6GsU+G9S035bG8bb2Vjj9CCP5V8tyYeu+ZS5JdU9n6dj4A32O4U+3OP5VgW7+IFlVNlqwxyW2/wBOa5/49fGpfg/4aiFvGtxreoBktoz9yLH3pW74BIwO59OaujlNbEVo4ehaTlpo729fI7cvy2vj8RHCYdXlJ2X+b8l1Pnf9pGSC4+O3iRrfbtE6I+3oXWJA/wD48Dn3zXFg5FFxcSXVxJJLI0s0zM7uxyzknJJPrSFsV/SeXYX6vhoUL35Ul9yP6+y3CvDYWnh278sUr+iFopN1LXadoUUUUAFFFFACMcUhPH9aGGa3Phz4RPjbxbbWJDCE5knYdo16/nwPxrKtWjSg6ktkc2NxdPC0J4ir8MU2/kjtvgd8I11JY9c1SHdCp3WcLjIkI/5aMPQHpnrjNevAU63ijjg2RqsccahFRR8qqBgAfQDH4U1a/LsdmE8VV9pL5eh/LHEOeV80xbxFV6fZXZdBwGKKKK5TxlsBGaKKKBiDrS0g60tYy3MwoooqQCmn71OpCOaANCz8L3WoaaLqFopA2cR5O7j9PwryT9oP4efb7T+3LaMi6tRsvF6F4xwH/wB5en0PtXr3hPU20/U1Tc3lTHYV9D2NT/EHS0e5Jkj3Q3kbRyrj5W4wR+Kmoy3NK2Dx6jLZ7W7dUelkOcVcsx8MRTez1XddUfGzfeoHSr/i7w+/hXxRf6fJz9lmZUOPvpn5G/FcH8az16V+z0qinFTjsz+tMPiIV6Ua1PaSuvmLRRRWhsFFFFABRRRQAUUUUAFFFFAAelbfwv8A+SoeG/8AsL2f/o9KxD0rb+F//JUPDf8A2F7P/wBHpQt/68jHE/wZej/In+Mv/JYvFv8A2HL3/wBKHrnR0rovjL/yWLxb/wBhy9/9KHrnR0oJwn8CHovyQUUUUHQFFFFABRRRQAUUUUAFBOKKa1ADs0xj37U+GB7iaOONHkllYIiINzOScAAdzkjivpD4E/swQeFVg1jxJHHcasCJILM4eGzx0Ldnkz+A9z08PPM/w2WUfaVtX0S3Z89xFxNhMmw/tcQ/efwxW7f6ep5z8KP2YNc+Ikcd7fE6LpEgDJNKmZrgHuiddv8AtNj6GvoHwF8CfC/w52yWOlxXF2ox9rvAJ5z9CRhf+AgV1ynI+tKOBX4bnXFuPx8mpScYfyrT7z+duIOOMzzSVpT5IdIxdvv7gW3MCWyR69qiv9MtdUTbcRxyY6HHzfnUiSK7MAV3KcMB2PpT6+XjKcXzJtM+RMlfDLWRJsr65t1znaTuX8qGttZi+7fWj9vmhxWtimydRXVDGVNpWfqkwMeTRdS1D/X6p5cfdIE2k/596rXfw10LVFj/ALQ0uz1J4shXvIhMRnGcZ6ZwOldAjBetIfmY1p9erKXuPl9NPyNKVapSlz020+6Ofm+DXhGVGH/CM6DgjHFlGP5AVy/iX9k3wbr0Ehgs7jSbgj5Xsp8KD/utuBr0fYKXFdVHOMdSlzU6sk/VnqYfiDMqEuelXkn/AImfNXxs/ZeufCHh7Tb/AEWGTUvsduYtT8mPazFTlZ9mSeQcNjuoPQnHjY+7nBx1r753sG4+8K8j+Pn7NFt4zhm1jw/BHa62P3ktunyx3578dFk9+jd+ea/QuF+OpKSwuY632n+jX6n6lwf4lPmWDzZ7vSfr0a/U+Y6KdPC8EjRujRyRsVdGUhkYdQR2I9KbX61GSauj9tjJSXNHZhRRRmqGIxyK9j/Zv8Hzabo11rVxDtXUsRWpb7xjRjuYD0LYA9dhrzP4f+D5/iB4103R4cqb2cLI4H+qjHLv+Cgn64r6t8V6Xb6JPZ2NrGIbWztY4YYx/Ai5UD8hXxfFWcKjKOBh8Uld+iPyvxOz5UMNHLafxVNX/hX+ZmAcUmcdqTpTq+RjK6ufhK1CijNFUUFFFFACDrS0g60uaxluZhRRnNFSAUZooxQA62uPss6ybd3lsGxnrg5ra8Z63b6n9njt38xVy7NjGM44x61gsea0n8L3EeiC+LR+WV3lDkNt9cVx16dH20KtR2avb5mbgnK54/8AtEfDF5tI/wCEos9zLC6W9+n9xTgRyD2ydp/4DXjynP8A9avtDwhplvrOk31neRCe1uVMU0ZGQ6spBH4ivkbx94LuPh54y1DRrhiz2EpRZD/y1Q4KN+KkH65r73hPPPbzqYCo/ehqvNf8Bn734Z8RSxOHll1d+9T1j/h7fIyc0U1adX25+qhRRRQAUUUUAFFFFABRRRQAHpW38L/+SoeG/wDsL2f/AKPSsQ9K2/hf/wAlQ8N/9hez/wDR6ULf+vIxxP8ABl6P8if4y/8AJYvFn/Ycvf8A0oeudHSui+Mv/JYvFv8A2HL3/wBKHrnR0oJwn8CHovyQUUUUHQFFFFABRRRQAUUUUAFIT1oJr0X9mj4Wf8LH8ei4uo92k6NtuLjcPllkz+7j/EgsR6KfWuHMsdTweGniau0V9/kefmmZUsBhJ4uv8MVf18vmem/sufAZPDum2/ibWIP+JndJ5ljA4/49IyOHI/56MOR/dB9Tx7O4+agnBG7JJoJ3Gv5szbNK2YYmWJrPfZdl2P5PzzOsRmmLlisQ99l0S6JeSHIPlpx4FNj+7SuMqfpXmKN5WPFe5meGZ/tOmNM2P380jYH+8R/hWpmsHwNMZNJaE/fhmIYEdM4NbvetsZFxrSTC+otNk6inU2TqK547lDaKKK2AKFXdQTiqOs6o1m9rBDg3FxKFAPOF7n+laU6UqkuWAF3GG/HrTi60Mp5NIeaxaUgPK/j1+zxpPjg3GuQ3kWi6kibriaQf6LMB/HKBypAABcduSDivnTxv8PtY+H19HDqlm0CzjNvOpDwXK9mjcfKwPXrn1Ar7fIV12sqsrZVlYZDAjkEelfLfxFh1L9nzxteaNDDHqHhPUs3MOm3gMlrJEzHKA9UdDxuXBxtPOTX6jwLxBi5t4OUublV0n1S3Sfddj9n8O+KMbUvgJz5+VXinvZb2a6+T3PKgKTvWx450rTNN13do10bnTbqJLmBXbdNahhzDJ/to2R7jae9Y55r9co1VVgprqfteHqe1gprr0e6PdP2JvCS3Ws61rsigmzjWyhOPus/zOR/wFVH41618QV26vAe5hx9eawP2P9K+wfBa3m2kNf3txO3HXDBAfyT+ddT8RoN32WQKfl3IT+v9DX4Rm+PlX4hqN7JuK9Ev+HP5c41xzxOe1m9ovlXol/mcwBzSNxQOlaGhaRHfCaa5d4bS3XLOv8R9B/nvXoyrKnC7PnB6eFprnXrHTLWRbq7vpIoY1XgGSRgqqPXkirnxL+GWufCDxzqHhvxFYyabrGmvsmgc8EfwujdGRhyGHBH417Z/wS/+CMnxa/af0vUZod2leCl/tm4Yr8vnqdtun+95nzj2iNfoN+1D+yJ4X/at8GLY6yslnqtiG/s3VYFH2iyJ5Ix0eNsDch68kFTzX1mU8N1cVgXXT96+l+qPncdniwuKVKSvG2r6q+x+N7Lselrv/wBon9mTxf8AsxeLzpfiixYW8zEWWpwKWstQXn7j44bHJQ/MPTGCfPt1eFXozozcKqs0e9QxEKsOeDuhR1pHqzoumtrWt2dikkcT308durv91C7BQTjnGT2q1428G6p8OvGOpeH9as5tP1fR7hra7t5Bho3H81IwwI4KkEcEVz8kmudLTuVKcVLlb1MwcU6vUf2U/wBnST9p7xD4q0GzmePWrHw/NqWkruwk1zHNCBG/+y6s6+zEHnGK8vubeayupre4hmt7i3dopYpV2vE6nDKwPIIIIINaTw840o1WtJbeqM44inKpKlF6x3EopuTWvc+BNWtPAVn4nktJP7Dvr6TTIrocp9oRFcxt/dYqwIz1AbH3TWMYSkm49DSUlG1+pksKtzeIbqfS1s2ZfIXHRcHA6A1TPPuKD1rP2UJv31e2o3Hqzrvh/Ds0uaT/AJ6SY/If/XrxT9tvwotvrOi67GoX7ZG1lOQPvMnzIT/wEkf8BFe9+HLD+z9FgiYbW27nHuea8+/a80gaj8F55tv7yxvIJ1IHYsYz+j14/D+ZOnn0asdpS5X6PQ+j4HxzwueUZLaT5X6M+UxwaWmilya/oI/qzlaFopPm9qPm9qA5WLRSfN7UfN7UBysWik+b2o+b2oDlYtFJ83tR83tQHKxT0rb+F/8AyVDw3/2F7P8A9HpWHzW58Mfl+Jvhv/sLWn/o9KFv/XkYYpfuZej/ACJ/jL/yWLxb/wBhy9/9KHrnR0rovjL/AMli8W/9hy9/9KHrnR0oIwf8CHovyQUUUUHQFFFFABRRRQAUUUZoAaTivsT9nfwF/wAIB8LNNhkj23l8v227+XB3vyqn/dTav1B9a+Vfhz4d/wCEt+IWi6aV3Le3kcbj1TOX/wDHQ1fcXU8fKOwHavynxMzBxhSwcevvP9D8b8Ws0cKdHAwfxe8/RaL8Ruyl8v6/nUV7fx6eitJuVGbbux8qe5PYVOGz3r8h95K7Pwp3BRgUHOaXNITmpi9bknOQJ/YHi943Zlt77lSezf5/nXSMuFrN8RaGus6cVXb50fzRsTjn0z71W8K+JhqVxHpcwb+0mcRRxKNzzMegAHJY+w5r1a1N4qmqtLWSVmv1sNvqbW7NMkPIrrNG+BnjbxGB/Z/gvxheAjIaHRbllYeoOzBH41qH9lL4oOBt+HPjg/8AcGn/APiaxp5XjXqqUn/26/8AI55Y7DrepFfNHn27ilzXdXX7LnxMs4y0vw78bqo5z/Ytwf5Ka5/xH8MvFPg7TJLzUvCfiqzt4zgvLo1ygz6coK0/s3GXt7GX/gL/AMhwxlCW0196MWVkgjaR22qgySe1YOgltf16TUZE/dQL5cI7KT/9b+dQTyX3jKfykjktLaM/PuyCTnpjGSfbtXRWNlHp1okMa4WMY9zVVP8AZIOEn+8ktl0Xb1Z0cy6Ep+4fpTacW+VqbXmUxgo5z715r+1b4CXxh8MJr6OPdeaCftiberRdJR/3z83/AACvSulNeNbmNopQrRygo49VIwR+Nd2W4yeExcMRH7LT+XU9DKMwngcdSxcN4tP5dT4JcnPr7U3+KtPxp4dfwf4r1DS5Ad2n3Dw89wDwfxGD+NZh4Nf1BQqxq0o1I7PVH9iUa0K1ONWO0kmvmkz7D/ZfTHwF8Okf8858/wDf+Wur8XWBv9CmCqWeL94oHt1/TNcP+yTfi7+BunJklrW4uIjx0/eM2P8Ax6vSGbPuM9PWv5pzipKhm9WXWM3+Z/IvE9NwzfERf88vzPN7K3jnuo1mm8mFj8zhdxA9q3NK0TUPiT4j07w34b0+4vLm9lEFpaQLmS5c/wATfTBJJwFAJOAM10nw1/Zj8YfG/wAbx6T4T0W6vY5pNrXrRstlYDrmaUAqgAOccsRjANfpl+x1+xR4b/ZM8PNJDt1bxXfRbNQ1eRcMy8ExQj/lnFkdOrYBYngL+lZDkM8znDEbQ7/5abnyeZZxTw8bL3pdr/n2L37Fn7Llv+yr8HodHZ4brXtRkN5rF2g4lmIACKf+ecajaPXluNxr19TtpIxinV+y4WjGhTVKGyPga1SVSbnPVvcxfH3gDQ/in4UutD8RaVZ6xpN4MS21ygdD6MO6sOzLgjsRXwH+1P8A8Ejta8JNcax8L5pPEGmAl30W6lVb62X0ikYhZl64VsOAAPnNfotTcbhXLmOT4bGxtWWvfqdGDx9fCy5qL9U9mfhLqNte+Dddkhu7e40/UdLnHmQ3ERjkt5EOfmVsEY64Ir9O/wBvD9he1/av0C38VeGfs1n42t7ZSjORHHrUBUFYpW/hdf4HPTJU/Lgr7L8a/wBmfwJ+0RZxx+MfDdhq8kI2x3J3Q3UQ/urNGVcLyeM45rtLKxi061it4V8uG3RY41ByFVRgD8AK8PL+F40Y1aFZ80JbdGn8z0MZnk686damuWcb+aPzS/4Jf6Brnwr/AG5JNB1vSLzSdSbRL2C6truExyxBTG2R2I3IMMMgg8E171+3T/wTTh+O+s3XjDwVNZ6V4qmXde2U37u11ZgAA+4D93NxgsQVbvg8n64ZMyiQhWcDaGI5A9M+ntS7q7aHDtCOFeDre8rtp7NXOWrmtaWJ+tU/dlZL1sfhz8R/hh4i+D3imTRfFGj32iapHlvIuEx5ig43Iwyrr/tKSPevrL/gmd4E0z9pD9nz4q/DHXMfZJprXULSUDc9nNIjoJkz0KNDGeMZyR3NfdXxg+Cfhn48eDptB8UaXb6lp8mShYYltn7SRP1Rx6j6HI4ryn9hz9iRv2Pr/wAZTSa1FrR1+4iSyZYTG0FrF5hUSZ48xjIc7fl+UYrx8HwrPC45Sj71J3T72a6nqYrPo4jCuMvdqKzVu9/wPy5+Jvw31X4PfEPVvC+vQ/Z9W0acwTqPuPz8siHujrhlPcMO4NVfC+k/2pq6BlzHD+8cHvjoPxOK/VD9uL9hXTf2sfD9vfWM9vo/jTSo/Ks76UHybmLJPkT7QTtySVYAlSe4JFfHf7RH7GF1+yZ4Y8JPNeJqU2tQSrqc8QPkpeq27ZHuAOzy2AG4AtsJ4zgfF8V5JisvoVa1KN4Lquif+R7GAzylXhGEnab0a9P8zysDFcF+05x8Dtd/3Iv/AEdHXfA15t+1le/YvgjfruKm5uIIAMfe+cMR+St+VfmOQpvMqCX8yPsuGYOWbYaK/nj+Z8lr0ooB5p1f1FZM/sC9woooo5QCiiijlAKKKKOUAoooo5QA1tfDHn4m+G/+wvaD/wAjJWI3St34Yf8AJTvDX/YXs/8A0elFrM58V/Bl6Mm+Mv8AyWLxb/2HL3/0oeudHSui+Mv/ACWLxb/2HL3/ANKHrnR0qTPB/wACHovyQUUUUHQFFFFABRRRQAHpR2oJxSe9AHpP7JOnrf8Axz09m4+x2884yuedhUf+hfpX1lLwq/zr5c/YyVT8Xbn1OmS4Hvvjr6hbjFfg/iJNyzXlfSKP5w8VKjlnSj0UF+o1o1nRo5FV0YEFT0IrJWebwrJtmZrjTiQEf+ODPY98e9bC8E0SxrNGysAytxgjgiviYVuT3ZK8e3+R+bBDMtxEskbK0bDIYHgivUPgv+yn4j+L/hTVPFEnl6B4J0OCW61DXL1D5QSJS0iwx5DTOMYwMLngsDxXWf8ABP79gCT4/wDin/hItYnu7TwHpdyVltR8p1eYD5oUbqIxkb2HP8I5yV+uP26JYta0j4e/BfQlgsW8eavDDc29sgRLXSrdlaXCrwF6cYxhGr9EyLgWNXCPMsY37N/BHZyb0V/K76bnyeacQKFdYXDP3ur7Lr80jh/2FP8Agn94T1b4SaX4u8daL/bOpa//AKdYWl3I6w2to3+q3xKwVndcOd2QNwAAxX1RofwX8HeGdRs7zTfCfhqwvLAFbW4t9MhjmtwRghHC7lz7Gui06yg02xhtoI1jt7WJYYUA4RFAVR+AAqQDNfvuT8O4LLsPCjQpxukruyu31d/U+BxWPrYmpKdST189A+YD5SfxNNbcFA3H1606TgD602vejHscnKgALck/dPFOLNjvz2zTaDz3NKVO4WRw/wAVP2a/Afxss5IvEnhfSr+RgQLpY/Juo891lTDj88e1fnp+1H/wT/1/4KfEJbXw/wCZrejasHk0jcQLiZkBd7X0edUBdQMGRVO0FlK1+oRXNcp8Z/hfD8YfhvqGgtO1leTBZ9OvY+JNOvIzuguEPZkcA57jI6Gvi+KuC8FmtBy5Eqi1TSs35Pvf8D1srzevg5+67xe6e3qfjIeCykMrqSrKwwVIOCCOxGOlNXmvtT42fs1x/tffA5Pid4Z02PTviJpZms/FWjW8YVb67tm8ufaoHE3y716eYrAH5iDXxZsIHT6g9vWv5tzvIa2V1VCfvQkrxktmv0a6q5+m5bmUMXTbWklo12f+XmBpAMil/A0KcGvF3PSPkv8Aau01dP8AjjqbrjF5Db3B+piVT/6DXnJHNeq/tjurfGJQq4ZdOgDH15Y15V3r+lOG5OWVUJPflR/WnCNRzybDSl/IvyPpr9iTVRdfDvVrU/es9RLDJz8rxp27cqfrXsTsFz823nr6V85fsSeJ47DxXrOkyPt/tC1W4iBPVomwf/HXJ/4DX1L4H8IzeP8AxppGhwbvO1m9hslKjJHmOFJ/AEn8DX4jxhgpRzupBL42rfM/nvxFw7w2d15yWkrS+TR+jP7BXwz/AOFafsyaAskXl32vK2r3Y95ceWD9IhGK9nRNtRWOnw6XZw21uix29siwxKv8KqMAfkBUw4Wv6VyvBxw2Ep0IbRil+B/P1Sp7Scpvq7i9KQHJrzj9rL9qjwj+xd8BNe+I3ji8e10LQYs+VFtNxfztkRW0KsQGlkYYAJAA3MSFUkfD3gb4a/tif8FR9Kt/Gvib4lXX7L/wu1lfO0Tw34Yic+ILy0bmOe4lyjqXXBBMi7hyIUBBPbs9TJy1sfpQzbRyCv1FL1FfnhrH/BKb9pH4FQ/258Hf2wviJrOv2wLjSPiBI2oaVqOOQjEmUJk8Z8o9eo616P8A8E/f+Cm+sfGv4ran8Efjd4UX4ZfH/wAOw+dJpg40/wASQAMxubFizZ+Rd5TcwKncjEK6pXMg5j7IoxSKcilqigpO/Az/AFpHdURmZlRVBLMxCqo6kkngDvk1+aOq/tCfG7/gsx8XPEfhv4F+LLz4P/s7+EbxtM1Px9bxsuseK7hceZHZFSCsfcbWQhGVpGy6xAuFz9Mvs8h/5Zv1/u1GBjsc9ea/PVf+DcL4RSWyzv8AE74/N4iADnWv+Eqi+0ecP+Wu37ORnPPXPv3rltf+K/7Q3/BFHXdOu/iZ4m1T9oT9m3ULuOxm8RSxM/iXwdvYKjzbmO+PJAAZ3V8bVaJiqsPTUm9tz9NMV4t+3x8NF+I37Mev+XHuvPD6jWbfAyR5AJkA9zEXHvXqXgLxzpHxP8GaT4j8P6la6xoOvWcWoadfWz74bu3lUPHIp9CpB55HQ4PFaGpadDqthPaTqr293G0MoPQowKkH6gmuDM8HHGYSphp7STRtRquE1NdGmfjaWyc5HPIrxz9tnVPsvw10u243XWph+vUJE/b6sPxr3jx34Ul8B+Ota0ObiXRdQuLFvfy5GQH8QAfxr5c/bc8RreeKtF0hG/48bZrmUD+FpGwuf+AoD+NfzZwjgZSzuFJr4G2/lp+Z/QHh1h/rWd0GtleXySPEV606mg5NOr+iIn9WBRRRVAFFFFABRRRQAUUUUAI3St34Yf8AJTvDX/YXs/8A0elYTdK3fhh/yU7w1/2F7P8A9HpUs58V/Bl6Mm+Mv/JYvFv/AGHL3/0oeudHSui+Mv8AyWLxb/2HL3/0oeudHSpM8H/Ah6L8kFFFFB0BRRRQAUUUUAB6U3dTjzRigD0j9kzUl0343aercfa4J4Bk9SULY/8AHa+sCOFr4h+GPiMeEPiLoupMfks7yN5DnGEJ2v8A+Ok19wSDDYHO3uK/E/ErDuGOp1uko/kz+ffFrCShmVLEdJRt80/+CRk13H7PHwS1D9ob4t6P4V05mi+3Sb7q427haWy4Msp9wvAHGWKjvXEhSDX6Mf8ABKH4BL4G+ENx43vbfGqeL222pcfNDZRswXH/AF0cM/uoSvF4N4f/ALYzOGHfwL3pPyXT5n4fn2ZLB4VzXxPRev8AwD6Y+H/gHS/hv4O0vw9otstrpelW6WtrCOyqMck8ljySx5JJJzXzz+z3P/w0D+258QPiMzGfQ/BcK+FdBYjMZfrPKh+u/n0lFeg/tv8Ax+/4Z2/Z71bVrWQDXdRX+z9IRT85uJARvAHPyLuf6qB1IrhPDPxD+G//AASq/Yv0O6+K3ivR/B9uqtNeS3Tl7i/v5AZHihiQGSeRVwmEBICAnAya/ovEQhXzKjgKS9yiueVtr6qC/N/JH5jTk4UZVZbz91efWT/JfefTQ+U4o3V+cx/4OXfhLqMrXeifCf8AaE8ReGUOX1+y8KxGxCDOX5nztwAecEgjgV9M/sTf8FPvgp/wUFspx8NfGNtfazZxma80K+iNlq1qo4ZjA/LqCQC8ZdAcAnNfUxqRbsmckqc4q8lZH0BJ0H1ptK7ZA+tJWq2EFBPFBOKx/H/xB0P4V+CdV8SeJdY03QPD+h2zXmoajfzrBbWcK/ed3bgD9SeBk0wNcNz0pD8vtX546v8A8HAUPxV17ULH9nn9nv4vfHq10yUwz61YWbafpbMDzscxyydAcb0Q+3erXgz/AIOCvCvg3xtp/hv9oL4Q/FL9nXU9TcRQX/iHT2udHkc/9N1RJAOD8wiKgDLFRWftI9x8ku39em57x4e1v/hnH9vHV9Eun8nwz8YoE1LTWPyxw6rCgSWMHpukHzepLIOa+df+CoH7LUfws8cx+OtEtvJ0LxROy38Ua/LaXxyxb2SUZOOgcN6ivsD9of4LaR+1z8F7Q6Pq9oLxRHrPhnX7KYSxxzYDRSxypndE/GSpPGCOQDXmHh34zWf7Rvw81v4J/FyOHwr8SWgNmn2lQkOqSLzDd2zH5WbeofaOvJXglR+Y8TZPTq4erleIVlJudGXRSerg30127p+R7eW4ydKrHE0t1ZTXVrv933WPzgyc0bd3tVnWdFu/DOtXmm6hC1vfadO9tcxMMNHIjFWH4EGsrxBr1v4W0G81K6YJb2EDTyE+ijOPx6D61/N0aU3V9klre3zvY/VsPTdaUY09eZpLzvsfJf7TWsLrXxv1xkO6O2aO2U54+SNQ3/j24VwZOKsapqcut6pcXk/zTXUrzSH1ZmLE/mar5xX9RZbhfq+Fp0P5Ul9x/ZWVYT6pgqWH/lil9y/zNLwf44n+Hnimw1izeP7Vp8nmBHbAlXoyH2YEjPbOa/ZD/gnj+yzqnijWfA/xYnudLbwrdacmtaesM5lnmkmgISNlwNpjZ2yf70fHWpf+CKPwJ8GwfsF6HrTaLpGp6p4qur241We6tIp3LpcywLESykhVjjUBfUseMmvtTwV4c03wVoFvpej6dYaTptnkQ2llbpbwwAksQqKAqgkk4A6k1OI4cwOMxdLF4iN507NfLv3P5O8VuN6eZYqeDoUeSVKUoOTd3KK0eltNb2d9jZA4p3am9BTq+pjofii2Pnf9uj/gn7p/7dfjL4STeIfElzZ+E/hr4k/4SPUfDf2Bbi28UOqqIopXLqYwuHXO1wUmkGASGGp+3J/wUM+Gv/BPX4dWuufELU7r7XqjPHpGi6dEJtR1dk27/KjJCrGm5d0jsqLkDJYqp9zP3q/nc/4LE6P4u/ay/a7/AGjfiBNrnh+38O/A/U9O8IRaVe6mIdQNvI5gj+xW5z5oM4mmkwwOJN2Dj5acb6k6X1PtS5/4OU9f0HTbLxVrX7KvxG0n4Z6kUW28RSajKsc+8/KySPZJbSbgDtVZuSMBj1r6Y+Hdp+z3/wAFgrj4Z/G7QLrUb7XvhLqwurCW3uG03VNKuMh/sV/GMs0RZA4UkqwDFGKu+74//aN/4Lq/BD4rf8EVv+FUw2OrXXxI1vwXbeEpfD39mulpo9zDDHF9sM7DyjCjRCWMIWckICq8lfln/g3M+M2s/Dj/AIKYaF4asriUaR8RdLv9N1a2BPlzfZrWa7t5COm6N42AJ6LNJ/eNXWhCLShK90Y4dynBuceWz+8/oeooBoqDoOY+NPwzh+NHwg8VeD7rUNQ0q28VaRdaRNeWLBbq1SeJomkjLAjcA2RkY4r561r4n/BT/gh1+xN4P8M67rV3a6HokL2Ok2qQLPrPiS6LNNPIkKbQWZ5CzudsaB0BYDbn6rYZFfzYf8F0vjrq/wAcP+Cm/wAR4dSmmbT/AANdJ4Z0e2Y/JaW8EUZk2jsZJ2lkJ77h2AqZWsEVeVj9FdF/4OShqWhSeLG/Zj+MA+GMMrJL4sgk861iUOEJZvs4twwY4K/acA8ZzX2r8Bv2jfhP/wAFK/2dNSvvDN9Y+MPBmvW8uka3pd5EY54BJGVktbqBsNGxVjjsRhlYjDV+d/7I3/Bdn4HfBj/gjLafCzxPYavqHj3w/wCGL3wqPDEWmO1vru9JUjm8/HkrE6yAyb2Dhg+Eb5d3if8AwQ7bVP2Nf+Cknw78GaZ488LeOtB+NHgtbjV08O3j3MFhN9mlureG5DAeXeQNC6sDnas7Djdit6tNR5XGV9NfI5adSblJTi1Z/eftv8E/gp4X/Z0+Fmi+CfBekR6D4X8OwfZtOsI5ZJlt49zMRvkZnYlmYksxJJOSa6tjxSt2pDyKyOpeR+f/APwUn/Zk1/wXr3ib4oaPp83iDSLtreSXS7BHm1GW7kaODy44lUlwzlWyOg3E4xz+b3x0/Yp+OWi+FdW+KHjL4e61o2hyyLLdTzPFvs1JCJvhDmVEHyjcygDqcZr+hbUWZYsqWUnjjivLP2vr/TdJ/ZN+J1xrKRyaXD4V1M3Ky/dZfssgx+JwB718rh+GcDg8XUxVCNpTu3d33d9Ox+ocC8eYnKKkKdGjGUpSUW3e/LdaJJ2T8+p/OOBj2pwORUdsCIYw3VVGakHSvQjuf27CTauwoooqiwooooAKKKKACiiigBG6Vu/DD/kp3hr/ALC9n/6PSsJulbvww/5Kd4a/7C9n/wCj0qWc+K/gy9GTfGX/AJLF4t/7Dl7/AOlD1zo6V0Xxl/5LF4t/7Dl7/wClD1zo6VJng/4EPRfkgooooOgKKKKACiiigAooooAQDcT78da+zfgP41Hj34VaTes2bmGP7Jc5+8JIvkJP+8MN9Gr4yU4avXf2RfiavhfxlNot1Jss9bA8pmPEdwoO3/voZX3IWvieOsneNy9zgvep6r06n5/4kZK8flTqQXv0nzL06/gfXHw38BXfxV+IWieGrHct1r17FZIyjJjDthn/AOArluf7tfs3Yafpfw38Hw2sXk6bouhWaxqZHCRWsESAZJPAAVepr80v+CaNjpOl/HjUPF2v3lnpui+B9Hn1Ca7u5AkMEkhWFMk9yGkwBySMDJPPtnjHxx44/wCCkuv/APCO+EYNQ8K/B+GYf2hrVxGYptZUEZVVz8ynnbGOM4aQ9FrzfDmtDLsuliYQc69Z2jFbtLTV9FfdvsfxLxIpYjFqk3ywgryfa/5u2yLHgLXD+3L+1RcfEG/juP8AhVHwpEh0xDA0g1K5jBcyBANzNwJCoBOEhXB3EV8+/wDBM/8AZn/4e0/FfVv2wPj7pLa5pl9qMtn8MfB+pqX03Q9Pt5douHhb5ZG8xGADgqzpJIdxMZT9Kvhd8MdG+DXgTT/Dvh+1Wz0zS4xHEmcu57u7cbnY5JbuT+A8n/4KU/tvaf8A8E7v2OfEfxHnsodQ1K18vTNB05xtivdSnysCPtIIjXDSPgg7I3A5Ir9cybKp4OjKpiHzVZvmm+77LyitEfMYjEOpPkoqy+FLrb/Nvc97gkXS7KOJGhtLeECNEGI441A4UDgAAcYHSviz/gpV/wAEhvDv7VNp/wALH+GbQ/DT9oLwy41Tw74s0b/Q2v7qPLJDd7MJIrnC+cQXTjO5N0bfmf8A8E1P2Ydf/wCDgn46fFDXPj38YPHckHg22sri30/Sr5IVWS7e4wLeCRJIbe3j8gjakYYmQZbOSeW1/wDaL+Kn/Bvp/wAFGvEXw78O+NtZ8d/DvQbm0nudA1O7aS11XTbiKKfAiLFLa9VGKiSMKCyqSpRtles5e4qjWj2ZlHDuNR04y95K7X/BP3B/4J6fFz4mfGv9knwxrXxi8F33gT4kJ5tjrNhdRpEbqWCQx/bEjU/u0nC+YF4AJO3KbWPtQ6Vg/C/4j6P8ZPhj4e8YeHrgXmheKtNttW0+fGDLbzxrJGSOx2sMjsQRW8OldEdjniIfvV+dn/BQD4F+Mv8Agpl/wUz8H/AfXdL8VaN+zr8PdGh8beKr5Laa2svF16ZCkVkl0oAbaGVNqtuX/SWwGSNh+iZOTXiH/BRj9tnSv+CfH7IviX4malbLql1p4Sz0fTGkMY1LUZsrBCW6qmQXcjkJG5HIFTNK12VHm5vd3PWvAXgTSfhn4M0/w/4d0nT9B8P6PCLaysLG3W3tbSMdFRFAVR/M88kmqHxh+C/hf48/DvUPCHjfw9pnibwzq8Ziu9O1GESQyrjhh3RgeVdSrKQCGBGa/AH9gjQ/iB/wX4/bo1fQfjV8YvHFjotjolzr7adod/8AZLeMLNDEtvZ2zb4IUUyqWYxu5CLkkksO9/aF+O/xP/4NyP8AgoHovgzQfiN4s+Knwl1zSLbxA/hzxPfm6eSzlnngljiz8sN0jWzskkQVGBUOpGRURqfu/acvu9zaWHaqexv71r2/4J+gP/BNn9ib4t/8E6P2mvHPw30+6XxL+yvqVmdb8I3d/qSNqPhi/eQGTT1iP7x0bdKxbAQ+WjjDvIrfWXxl+AHhD9oHQF0/xZotvqkUOTBNuaK4tie8cqkOv4HB7itr4cfEHSPi38OdC8VeH7pb/Q/Emnwapp9wBjzoJoxJG2Ox2sMjseK2FODSrYSlWpuFSKlF9HqjGNSfN7RO0u60Pyf/AG8P2f4v2dfjy+lWd/qmpafqljFqFvcahMJrh9xZGVnwNxDIeTzjGfWvir9sz4kDTtFtfDNrNi4vsXF7tP3IQcqh92I3fRfevvb/AILmfH3TPAvxW09Jljebw/oojVFB33E1wxkVGP8ACqqqsT2Bb1xX5FeJPEl34w1y61S9l866vpDLI/bJ6Af7IGAB2AFfguC4VpPiKviIx/dU5af4uy7pM/q/wX4fqZhGlmeM+GC0v1l0fyKW7H8NFCjilK1+nH9QcqP1G/4N4P2hfP0rxv8AC2+uF32jr4i0iNm5MbkRXSr9HET/APbQ+9fp7p8mJK/mx/Zz+PviD9l/41aD468MSxpq2hz7xHLnyryJvlkgkxzskXKnuCQRyBX7t/sZ/t+/D39tXwxb3HhnVIbXxFHEr6h4du5VTULJ8fNhP+WiZ6SJlSPQ8DohK7TP5E8Y+DMRgsynmtCDdGrq2vsy637X3v3bPoRW3ClzUcbE9cinL96vTUk0j8LA9a/ID/gsN/wTvt/hD+3Ha/tMXPwrvvjV8JdWjaXxx4VsLua1uLW7W0a3S4Lw5lEDt5MpdQQrwsHwrg1+wOKRSUO5flb1pkyjc/j0kv7e5nbySkYkc+XCJPMKAn5UyeWI4XOMnHrX7Hf8G6v/AAS78VfDnx9N8eviJot74d26dJY+ENMvoWhvJxOGW4vpI2w0SeXhIwwBcTSNgAKW/VO3+CXgnTvFP9uW/gvwfb64Tu/tKPRLZLzOc580R7855znrXTiTJJbLEnOTWfLZ6GknKSsODhfWlDZqPG9q+df2n/257z9nv9tf9n/4SW3h221SH4zXOpR3moy3TRtpcdrEjIY0AIdmd+d2AFHHJ40Jk0j6ObkV+HH/AAcQf8E0vFPg79oDWPj14U0i81rwb4wSGbxH9hhaWTQb6KFIWmlVckW8yRo3mfdSTeGwGQt+4wGRQV4PTDAqQR1B6ijcNndH8jfwR8a+E/A/xe8N614y8Ox+NvCOm38dxq+g/b2s/wC1rcZ3Q+cnzJnIOR1wAeCa/WX/AIN9P+CcOtWnx01n9onxH4UvvA/hllvYPAmhXrSNc7Lp2VpyZAJDFFATDG7gGbzGbooLfqFZ/spfCvT/ABQuuW/wv+G9vrSymddRi8L2CXYkPVxKIt24/wB7Ofeu/b52ySxY9ST1pR2swbbd/wBf0AtuNBOKOlNY849aYFXU3+6v418S/wDBdj40w/DX9h260CO4Eep+O9Tt9KhiB+drdD587Y/uhYlQ9syD6H3f9qz9u74X/slaLcXHjDxPZw6hChaHR7Rhcaldn+6kKnIyf4m2qO5GK/ED9un9tnxF+3P8Zn8SaxH/AGdo+no1pomkLJvTTrfdnk9Glc4LuOCQoHCgV51Z++2fr3hXwTjMzzOljKlNqhTkpOTVlJrVJX3u97bI8Xx/KnU0dadXOj+0loFFFFMAooooAKKKKACiiigBG6Vu/DD/AJKd4a/7C9n/AOj0rCbpW78MP+SneGv+wvZ/+j0qWc+K/gy9GTfGX/ksXi3/ALDl7/6UPXOjpXRfGX/ksXi3/sOXv/pQ9c6OlSZ4T+BD0X5IKKKKDoCiiigAooooAKKKKAE2819A/wDBKbwro/jT/got8JdN12CG602bWS7QTAGOaRIJXiUg9f3ipx3r5/rb+GPxE1T4Q/ErQPFmiTfZ9Y8NahBqVm5+75sTh1DeqnGGHcEiqp8vMuZXR43EWBq4zK8RhKDtOpCUU/NppH7+fsffs9+GfAvxu+N3hpbC31LR7HVdOFvBewrPHEhie4RMNnOxnwpPPyjnNfUsMKwwpGmFSMBUVRhUA6ADsK+Tf+CaX7Sui/tYfEL4meNdAEkdn4ng0jUZLeVSJLK4Fu8M8DZAyY5UZdw4OARkGvrZVyv/ANeu/hnDUKOE5aUUrSmtP8bP8386pV6OMlQxaaqRspJ7p2SdxNtfnh/wcu+DdRv/ANhjwX4wtdEtPEmmfDH4h6V4l1vSrtGa1vbJUngZJwvJhaSaKOTkfJI3tX6IYz90MSOozVDxj4I0r4jeDdT8P69pdprGh63aS2OoWF5CJre9gkUpJFIrcMrKSCD6179TWNjylUUZJ9j+XbxD8b/jV+xR8Y9P+Ofw9t7P4G2fxzsLvWPDlj4bmtL2wXSzOA1t5EiSIqJIFISSNSp5VVwVHzz8QPiJrvxT8Z6v4o8VaxqHiDxBrM7Xmoajeyma4u5COWZu/TAHQAAAYAFfsj+1F/wajJrvjKe++EHxOt9C0Ody0Og+Kbaa8XSlLbjHDcxHc0YJOFdNw7uxya9N/wCCev8AwbPeDv2Z/iTpfjn4p+Kbf4ma9ok6Xem6RbWBtdFtbhSGSWVZC0lyyMAyhtiAgEqxAx57o1HLlb0PWji6EY8y322/U+yf+CYPws1n4If8E7Pgv4U8RQyW+t6P4TslvYJARJbSOnmmJgeQyb9pz3U17uDmlfJ+9ySeT60i9K9KO2h5e+ofxfhX5uf8HS/w51fxv/wTd0XVdNhkms/Bvjix1fVQqk+VbPaXtmJD/srNcxDngbq/SM1k+PPAOi/FPwNq3hvxJpdnrnh/XrOWw1HT7tPMgvIJFKvG6+hB+o6jBqakeaNmVCXLJT7H8h/wM+PnjL9mL4p6b42+H3iK+8K+K9H3i11K0CO8aupR0ZJFaN0ZSQVdWU+mQCPbviz8avjZafBLX/it8TrHQPHWnftNWEmjWvirX0t77UrT+zbgeZ9jSNl+wshygURqhU5VQQK+8f2nf+DT3UrvxveX3wX+JOk2+hXDl4dF8VwTefYgkfIt3Creag5wWjDAAAljlj6h/wAE/f8Ag2T0H4B/EfSfGHxs8X2PxEvPD8wudL8NafaPHoiSghle4M2WuArDd5YVEJALbx8tcFOjWvyLY9apjMPK0lv2tr9/qfbP/BK74aax8G/+Cb3wX8NeILeSz1nTfCtp9qt5FKyWzSAyiNgeQyrIqkHkEEV72x3VIwYglt25uSTUTfh616sVZI8bmveR8u+HL5dN/wCCrvjCxvI1kt/EPhOBYUmRWSQxxQkjB6jCycezehr8p/8Aguh+zp4X/Zv/AG8bm08I2NtpOl+KdDt/EEun267YbS5kmuIpfLXOFRjEH2gAAswHFfqX+2frWn/Az9qTwb8SLy7t7BbHTBJKXlCNeRwXccE0KDgvK0F+xVAcs0QUAmvxQ/bz/asvP20/2rPFPxBuIZrSz1CUWek2kp+ezsIcrAjDsxGXYDjfI1fBYOUaf1nCz3VWUl6Ssz+jfAXA46rnixtFtUI0uWfZvZL8L+XzPIRQRkUL0orc/sQaeDToZpLa9huYZJILq3YPFNE5jkib1VlIKn3BBopCKCZRUouMldM/Wz/ggT+27qnxJtPE/wALfGGvX2sa1p4/trQrjULpp5pbT5I57cM53ERtscD0kbsBX6T96/mf/Z2+Ousfsz/HHwx480EsdQ8M3yXfk7yq3kXSWB8fwyRlkPpuB7V/R18G/i3ovx4+FXh7xl4euFutG8SWMd9bSDqAwyVYdnVsqw7MpFelh6vPGz3R/GnjPwfHKc3WOw0bUa+umymt18918+x04NL1pqnJp1dR+OEbj56KWQ4b8KQZI7UGnQCcGvk//gon+xz44+O37RH7NnxI+H/9ktq/wj8YG51eK+uvs3maTcNB9pKNg7mVYmXYOW83jpX1gOaMn/IoJlG45/vnH3c8fSnVHuI9Pyo8xqA5SSiow7EU5HO7FAco4muJ/aJ+M2n/ALO3wK8XeOtW2/YfC+lXF+UzhppEQmOMf7TvtUe7V2jnA/SvzL/4OIf2p10rwn4Y+D2l3X+k6xIuu68Iz0toyy20LezybpMekK9jU1JKEXJn0HCWQzznN6GXQ2lJc3lFayf3fiflb4n8TXnjjxfq3iDUpPO1XX72bUb2Y/elmmkaRzn/AHmP4VS6n8aVvlak7V4abvc/0KoYeFKnGlTVopJJeS/4AoGKKKK2NuVhRRRQHKwooooDlYUUUUBysKM0UYz/APqoDlYjdK3fhh/yU7w1/wBhez/9HpWCRit74Yf8lO8Nf9hez/8AR6VLOXFfwZejJvjL/wAli8W/9hy9/wDSh650dK6L4y/8li8W/wDYcvf/AEoeudHSpM8J/Ah6L8kFFFFB0BRRRQAUUUUAFFFFABQPv0U09aHsHkevfCr4m+KPBvwN1K88H+Itc8N+IPCeqrfJdaXePBMLadRGd20jcokAJDZHNf0FfsLftOWn7YP7KXgvx9bNGLrV7IR6jCuM2t7ExiuEIBOP3ikj1VlPev5uPhp44bwJ4ka4lg+26ddwvaX9pu2/aoHHzLnsRwQfUV+h/wDwb8ftfWXwg+PniH4MX2sG88NeNpf7Q8N3UqlAt/HH80W0j5WmhAyDgb4MDO8ZMhnPDYqrh5/BN80fJvdfefzh408EyxWX1cww9P36UvaXS1cGrTi3u+V2kr9L2PuT9sf9prxVoPxCuvC+iXEmi2dlFH508PE90XUNkPjKqM4BXkkH6V89XXjDWb67+0Taxqkk+d297uQsD165r79+LX7P/hr41xRnXNPJuoVKR3lu/l3Ea5zjcOo9jkV5TN/wTm8Pvcfu/EWuRx88GKJj+eK+mrUKknoz8m4Q4x4dwWBhRxNHlqJavl5uZ977/J7HjvwN/at8YeA/FWm2dxqV5rmj3VwkElncnz3+dgoMbH5gwz0yQcV93YIevLvhV+yF4P8AhLqkOoW9rc6pqkHMd1fOJGiPTKqAFU++Mj1r1JmwK2p05KNpHxPGecZbmOMVbLKPs421dkuZ97Lb9RX+6tQXV3FY2rzTSJDDEu53c7VUDqSalqnrOkw69pdxZ3Clre6QxyAHBIPpW0VY+Sja+uxYtrqO8t1lhkSaKRQyOjblceoPen54/wA8VW0bS4dE0yCzt1KwWyCOME5OB6mrD9KoUrX0PBf24fjf4g+F2naNpuhM1iNaExmv1XLx7NmI0zwGO4knqAOPb5D1bxBqOu3j3F9qF7eTSdXnneRj+JNfo9428C6R8RNDk03W9PttSsZDny5kzsP95T1U+4INeP6t/wAE+PBd5cbrO+16xQ/8sxOsqgegLAn8zXJiKNScrxZ+r8D8YZLlmE9hi6LU7u8lFSv69V6bHy14G+Mnij4a3kdxpGtX1sIuTDJKZIJPZkbgj8jX3/8ADfxfJ4x+G2i69qUK6Y1/YR3lzHIdq2+VDN16Dvz2rgfAf7EngPwRqUd5Ja3ms3EWGT+0JvMiRh38sAL+YNeXf8Fm/wBqBf2X/wBg/wAStZ3X2XxB42X/AIRrSNhw8bTqfOkUf7EAkIPY7fUU6cXRhebOXiPMMFxNmWHwWUUOWc5KPNazd/Jdt22fj5+3N+1v4i/as8S+MvEusa7qGoaLrHihl8IWM037nT9Pt2lAeGPomR5WXAyzFsnjj5xlmkuZmkkZpJJGLuzHLOx5JJ9c01DiKNf+eY2qM/dHoPz/AJ0Hivj4U/3k6st5O5/cvD2R0MqwscLh0lFdlbZJffpv8xw6UUDpRWx7wUN0oooAaDX6Hf8ABCv/AIKAJ8H/ABx/wp3xVerD4Z8VXTT6BczOFi07UGBLQMxOFSfA2/8ATXAxmSvzzpquUKsrNGykMrKxVlI5BBHQj17VdObhLmR83xZw1hs+yypl2J2lrF/yyWzX690f1KK3PP6U7dXwr/wSA/4KfW/7U/gy3+H3jbUI4/iVoNvthnlbB8SWyADzgennpkB16t98DBbb90Dr6jrmvXjNSV0fwTnmSYvKMbPAY2PLOP3NdGu6fQbIcN+FUfEHinTvCGktfapfW+n2cbKhmnfaoJOAM++avS/e/Csrxf4N03x94duNK1a1W6s7kfMpOGBHRgeoYHkEVR5sORte0vy9bb28jVhkjuIlkVlZZFDKwOQwPIIP0p21fSvE7L4U/Ej4Q/ufCGuWevaLGf3Wn6oArwL/AHQ3HQehUf7NWh8VPi1bN5c3w3s5ZO7xX6bT9PnoPVllPNrhq0JLzkov5qVrfiewlVI6Vz2r/EzQdB8Wafod1qEK6tqjbbe2TLueMjdj7ue2cZrzl/8AhcnxDDRNHovgq1kyryRyC4nC+2C3P0xXUfCn9nzSfhneyajJJNrOu3WTNqV580pyMHYDnaD9Se2aZFTB4ehBvEVVKXSMHfXzlsl6Ntne8Z4pVPz0MuxaE5bpSPLvaJg/FP4oaL8F/h3rXizxHdpY6H4ftHvbyZjjCKM4HqzHCgdywHU1/OH+0n8e9Y/ai+OfiXx9rnyX/iS688QA5WzhACwwD2jjVF44JBPevuT/AILs/t/w/EzxY3wX8I34m0Pw7cibxNcwyZS8voydlp7rCcM3YybR1jIr84yNp4rzsXUUvdR/WXgnwXLAYOWcYuNqlZWjdaqH6cz19EhzHDmg9KGOT+NB6VxJWP3+IUUUVsMKKKKACiiigAooooAKKKKAEb+lbvww/wCSneGv+wvZ/wDo9Kwm/pW78MP+SneGv+wvZ/8Ao9KlnLjP4U/Rk3xl/wCSxeLf+w5e/wDpQ9c6OldF8Zf+SxeLf+w5e/8ApQ9c6OlSY4P+BD0X5IKKKKDoCiiigAooooAKKKKACiiigBpGDV7wx4i1Dwf4j0/WNJvJtP1bSbqK9srqI7ZLaeJw8cgPqrKDVJhk0hBxTi7O6FUpQqU3TqK8ZJpruno0f0mf8E4P22NN/bv/AGZNJ8XReTb+ILILp3iKxQj/AEO/RB5hAyT5UmRIhP8AC+OoNe8gV/OJ/wAEzf2+9S/4J/8A7Q0GvMt1feDNcVbLxNpsPLT24JKTxj/ntCxLKP4gXX+LI/om8GeNNJ+IfhLTde0PUbXVtG1e3S6s722ffFcxOMq6n0I/GvpcHiVWhfr1P8/PFLw/qcNZs400/q9S7pvt3j6x/FWfprHimsuFb6UL17fhQ4yprqPzWMUtiPqc/wB2lBGfevIfjTL8Qvh146j8WeHRN4m8OmAQ3+gZ2tFj/lrHgEknqcAnjGCDkN8N/tv/AA/1iALfX19oF0vD2uoWjq8Zx0JUEfyPtVcyW56sMnxVWlGth17RPfl1cX2a3X3Wfc9fYc0Dk/Nx3ryHxL+3B8O9Ct2+yarNrVzj5LextndnPYZYBR+dYOjWPxA/aa8XabqmrQ3vgfwXpk6XdvZrK0d7fspypfGCAfcAAZAVidwOddDWOR4qMPa4peygus9G9NFFbtt6aK3c99KUECnk9eAPoOlMOTn3ppni37iDrtH3mPSvwO/4Lkftnx/tTftgXWgaLeG48I/DMS6LaFG3RXV7v/0u4XHBG5ViB7iHI+9X6N/8Fpv+CjEX7G3wKfwn4avkHxJ8dW8ttZeU583RrMjbNetgHa2DsizjLksOI2r8E4k8sfxHvycn868jNMVZKjH1Z/UH0feB5zrPiPGRtFXjSv1f2p+iXup97jyOaTH1p2eaK8M/rS1wHFGaKKA5QzRmiigOUM03H1p1GKA5S54Z8S6h4L8SWOsaRfXel6tpU6XNneWr7JraVDlXRuxB/wA81+z3/BMn/gsNoX7Udlp/gr4hXVr4f+JSqsMFwxWCx8SYHDREthLggZaI4BJymeVX8UyKNvI6qykEEHBUg5BB7EHkHqK0p1XBnwfHHAOA4kw3s6/uVY/DNbr17ruvusf1IsCW/Q0gG7tX4wfsQ/8ABdPxt8A7ay8OfEm1ufiD4WhxFHfCUJrNhHwPvt8tyoGflkKv/tnAFfqT+zh+3X8KP2sdOjl8E+MtL1C8ZAz6ZO5tNRh9Q1vJtk7HlQVOOCeK9OnWjJXR/HnFHAec8P1GsXScqfScdYv59H5Ox60i/L9KOvWlzg0u6td9T4zfUaeR3/OlxQTxXP8AxM+KPh/4NeBtQ8SeKNYstD0PS4xJdXt2+yKEZAGT1ySQAACSSAKWiKjByajBXb2XU3WbaufSvgv/AIK4f8FXbP8AZw0PUPhv8PdQjuviNqERg1C+hcMnhiF15OQf+PplbKLj5Adx/hDeI/t5f8F8brxdbXnhX4IQ3ml2UwMVx4svIfLuZV5BFpA4zFkYxLJ84zwqnDV+ad9PJeXUtxcTTXFxcO0ks0zmSSVyclmY8sxJJJPJJrkrYqK9yD1P6C8OfB3EV6scyz6HLTjZxpveT6cy6Ly3fWwksjTSM7u8kkjF3dyWZ2JyST3JJySepNNC4pM4pe9eef1NGKilFdA/xpT0pvWndRQdEWFFJ096Mn0q+ZFC0U3d7UbvancB1FN3e1G72ouA6im7vajd7UXAdRTd3tRu9qLgK39K3fhh/wAlO8Nf9hez/wDR6Vgk5re+GH/JTvDX/YXs/wD0elSzlxn8KXoyb4y/8li8W/8AYcvf/Sh650dK6L4y/wDJYvFv/Ycvf/Sh650dKkxwf8CHovyQUUUUHQFFFFABRRRQAUUUUAFFFFAB3oPSjvRQXHYaa+4P+CTX/BXK+/Yi1iLwV4zN1q3wp1GcyKEUy3HhqZ2y00K5+aEkszxDJByyZOQ/xAx5pQ4OOK0o1XTlzRPA4m4ZwGfYGWX5hHmjLr1i+kk+jX/Dn9U3w5+JOg/FzwXp/iTwxq1jr2g6tCJ7O/spRLBcIe4YdweCDyCCCAa3BJuHHNfzP/si/t7/ABQ/Yg8Rve+A/ED2+n3UnmX2i3i/aNM1A4xl4iRtf/bjKvwMkgYr9UP2Xv8Ag4o+FfxNsrOy+JWl6l8NtccBZbgB9Q0qRzgcSInmRg9fnTA5yx6172HzClU0ej8z+L+MPBbPcnqSqYSDxFHo4q8kv70d/mrr0P0Pxg5HB69KyNd8A6L4p51LSNL1Bj3uLVJDn/gQNZPwj+PPgn496G2peCfFnh7xXYxBTLLpd9HceTuztDhSShODwwB4PpXXDGK7rpo/I5KrQqOMrxktHumvXZmLonw60DwywbTdD0jTmXo1vZxxt+YANbS8e/vS5wajlnCtt9xRvoRUnKfvTbfrqOMiqetfOf8AwUT/AOCkPg7/AIJ9fDVrzUHg1jxpqkZ/sLw8kg867bkCaXvHbqw+Zz1+6uWIFfKn/BQD/g4K034ZahrPg34Q6Hdah4n0+eWxvNb1y0e2ttLmRiriO2kAkmdSGGXCIDz84r8jfiV8SvEXxl8eaj4o8Wa1qHiHxDq0hlu7+8k3ySk54HZVGThVAVRwABxXBiswhTXLT1l+CP3jw58Ecbm04Y7Ok6WH0fL9qa3X+GL7vVrbuanxu+N/ib9o74qa1418Zao+reItem825mPyogAwkca/wRouFVR0AHUkmuRA+agHHalzzXzzu3dn9m4PB0sLRjh8PFRhFJJLZJbJB3paTOTS0HZEKKKM0FBRmkPNIqlnCqCWbgADJNG+hnUqRhFyk7JdXp+Y7NG7muk0T4O+ItcCsmntbwtgiS5cQjH0PzfkK9U/Zn/Ysm+Knxy8O6L4gvrePRbuZ3vRayt5zxxxPIUU4GCxQLkcgEntXoSyjGqg8T7N8iV27dD89zLxa4SwNT6vWx1P2n8qkpNvtpdX+Z4KZPmx39DQzYHPFfsxcfsZfCW58KnRD8O/Cv8AZrIY8LZKs4HqJ/8AXbh2bfnPet79kz/gk18DfCPhqPVNQ8ExeJNUjvJlS41m7lvY2jVyYyIWbyh8pAIKnlc968LA46niZOK0PjZePGVKnKToT5ui019XfT8T8ffgf+zn47/aT15dP8CeFNZ8TTbwsk1pAfstt05lmOI4wP8AaYV+rH/BNH/gjna/su+KbD4hfEO6sdc8d2SltMsrQ77HQWdSrSByAZZ9pxuwFTnbuPzD7n0LR7LwvpcOn6XZWmm6fbqFitbWFYYYwOMBVAA4q2F3L8o716ei2PynjDxazTOqMsHSiqNGW6TvJrs3pp5JIet1IB95qX7fJ/s/lUJO00b6vmla1z8k5F2JheyEc/pWb4p8OWHjjw/faTrFlbalpmpwPbXdrcRiSG4icYZGU8EEdRVwNTs0rvuON4vmjoz8s/2pv+DfLUP7eutV+DviLTzp88hlGg69M8bWeTnZDcqrbkHQLIu4Dq7V8pfGL/gln8ePgj4bvNY1jwHc3Gkaehlub3Tb2C8jhjHV2Ctv2judvAHNfvwDhulcd+0P4fj8UfALx1p8nyrd+HdQj5G4A/ZpCDj2IBrSjRhUqxjLq0vxP1HL/GjiPLsPyScayitOda6f3k0/m7s/Ev8AZH/4JpeIP2lPDV3r2rat/wAIfo0U7WtsZLFp7q9dQNzKhZQsa5A3EnJBAGBmuN/a9/Ym8SfsiatZNqFxb65oOrMyWWq20TRqZAMmKVGzskxyBkhgCQTggfpz+ynGkf7OnhHZ/wAtLESNxj5mZi36k/Wq37X3w9sviX+zt4ksr6ygvlsbc6nDHKm7bJBlwwHrtDj3DEdK+f8ArE/7VeD5vd5+W79bXPTy/wAds3VX+0sZBSouN3Tilpp9mT1v5tn405+aj8K+jZfhd4buoNraLY4I42qUI/75Irntc/Zz0e9Rjp89xp0nUKT50efoTux9DX6XiuA8fSjzU3Gfpdfme7kv0sOFMVUVLGUqtC/VpSS9eVt/cmeJk+woz7Vu+Nfh3qngS5VbyJWhkJEVxF80cntnsfY/rWEQcV8diMPVoTdKtHlkujP6PyXOsDm2FjjsuqxqU5bOLuv+A/J2Y6iiisT1AooooAKKKKACiiigBG6Vu/DD/kp3hr/sL2f/AKPSsJulbvww/wCSneGv+wvZ/wDo9KF/X4HPiv4MvRk3xl/5LF4t/wCw5e/+lD1zo6V0Xxl/5LF4s/7Dl7/6UPXOjpQZ4T+BD0X5IKKKKDoCiiigAooooAKKKKACiig0AHeg9Kb1pHZUxuIXccD3PpQXsrsXPNAPPv2r61/Y0/Yp8O+PvhvD4o8Y2d3ftqMjizsWlkto4olYrvfaVZixDEc7duODmvZ/B/8AwSv+F/xn+Lug6Y11r3hGwvpjBMmmziYTMRlQPP37MkYyPUcZ5r85/wCIm5Qs4/sVt8/Ny81vd5trX/DsfJZjxpl+C9pKtzcsE22lfbst2fnCWEYyx2j3r2D9lf8AYX+KX7ZOuRWvgPwpe32nlwtxrN2pttKs1yAS87DDY67Yw78fdr9rPgV/wRt/Z7+A1zFeW/gW38S6tCRi+8Q3MmosCO4ic+Sp7/LGK+nrOzh02yitrWCG2toBtihhQRxxj0AHAFfq0MvWjmz8K4i+kNBwdLJMO7/z1Laeait/m/keDf8ABOz9gzQf2AfgYnhzT7hdW8QapKL3X9XMQjOoXGNoCDqsMYyqKScAkkksa9/+0Y/ib86Mc5oK5NetGNopRP5ozDHV8diZ4vFy5qk3dt9W/wCvlsL5rN/E350yTLdWJx0yacBijFVqcnKj4T/4Kgf8EZtM/bJ1qTxx4FvrHwz8RmjCXaXK7dO18LgKZiqlo5lAwJQDkYDKcKV/LP4m/wDBML9oL4RahNBq/wAJ/FlykRI+0aTbjU4HAONwaAscHqAQDjsK/o3KAikVivI4+hrjq4SnN8z38j9X4T8Yc9yLDxwceWtSjsp3vFdlJO9vLU/ll1PwZrWieJotFvtH1ax1q4lEMVhdWrwXMkjEAKEcA5JIHTvXrOs/8E+vidonhw6j/ZdhdSKnmPZW14JLtB3G3ADMO4VifTNfuD+3ppsMngQXU1rb3ckbRCKSWMM0DeYMupIyp9xg18jpI28Hv29q/nvxR8Q8dw7mlLA4KMWnFSfMr3TbVl223P6A4f8AFDF5tg44qNKNN3s1e6f4I/IsjaxBDKykgg9QR1B9/wDCnAc19vfHL/gmL4i+OfxE1bxN4J1jwfbNqUqs+jX961pcvPsG90OxlIcjPUck18y/G79kf4lfs5SN/wAJj4P1jSbXdhb/AMrzrF/pPHlM+xIPtX6rkeYf2nl1HMacbRqRT9L7r7z9My3ivLcVJUVWjGp1i3Z3672v8rnndNY80B9w47jIor0z6W6Lnh7QbrxTrENjZruuJzxn7qL3Zv8AZA5Ne+eBPhbpfgWBWhjW4vsDddyqC+e+3+6Pp+Oa539nPwvHZeHJtVdc3F+5jQn+GNSOn1Yc/wC6K9GI5r9c4PyGlRoRxdeN5y2v0Xkf50fSN8XsfmGb1eHcuquGHovllyu3PLre3RPS3rcMAGuz/Z51z/hH/jt4Sut2zbqcUTH2kzGR/wCPmuNxzWj4OvjpnjDSLpX8tre+gl35+7tkU5/Svr8zpqphKlNrRxa/A/l3A1OTE06i6ST/ABP0mK7ePSvUv2e/EMH2K+0pp0+1xt9qSEt8xiOFZgPQNgH0LL615fcKFmk2/Ku44H414j+178Rde+Cms+BPGnhq8Wx1bSb26t1YjckySRIWikXo0bBCCp9cjBAI/lThfBvE5nDDJ2crr520R/R2bY5YTBvFNXUbfdfc/QVBvp6LtWvC/wBlX9vjwd+01ZwWLzQeG/FuMS6PdzAee3rbSHAlBz93747jHNe5MSjEHgjr7e1fW4rBVsNUdKvFp+Zng8fQxVNVaEuZPsDfeNFNPJ5pQMVkdgtFBPFN/h680BsHevDf+Chvxyt/gp+zJra+cF1bxZC+i6bGp+YmVSs0g9kiLEn1ZR3rrP2g/wBqrwT+zRo5m8Taov8AaEibrfSLVll1C69MR5yi9t77VHqelfl3+07+0jr37U/xLk8Qa15drbwp5Gm6dE26LToOuwHALMx+ZnPJJ7AAD63hjIa2LxEcRNWpxd797dD4jiziWjhcNLD0pJ1JJqy6H1B+x1qS6l+zn4dAbd9lE1qePu7JnwPyIruPHlot/wCAtet5AStxpl1E2DgkNC4P6GvJf2AtV+2/BW+tSc/YNYmUc/wvHE/TtyT+VeyeI+fDepf9ec3/AKAa/M8+ouhxBVgulS/3tM9/Javt8lpyfWH6NH5q24/cx567RT16UkYxGv0FKBkV/VlKXur0P55la7SK+qaXb61YSWt3ClxbzDDxuOv+BHrXzv8AEXwdJ4E8SS2LM0kEiia3kPV4z0z7ggg/TNfSHQ157+0ZoSX3g23vtq+dp9wF3d9jggj/AL6CnFfJcZZTDE4N4hL34a/Lqf0R9G7xAxeS8S0spqTf1fEvlceik/hkuzurPyZ4tRTcZWnL0r8XP9NgooooAKKKKACiiigBG6Vu/DD/AJKd4a/7C9n/AOj0rCbpW78MP+SneGv+wvZ/+j0oX9fgc+K/gy9GTfGX/ksXi3/sOXv/AKUPXOjpXRfGX/ksXiz/ALDl7/6UPXOjpQZ4P+BD0X5IKKKKDoCiiigAooooAKKKKACkYZo3V3f7MvwD1L9p/wCO/h3wPpbPBJrNzi6uQu4WVqg3TzH/AHUBxngsVHeqjFydkc+MxdLC0J4ms7Rim2/JHefsQ/8ABP7xb+2x4jmfT5I9D8I6bL5Wpa5cR7kR+D5MKZHmzYwcZCrkZYHAP6NeA/2EPhv+zjrGleGfBXhyx1DxZdJ5l14j1mNL29tY8nLoWG2Inn/VqvGOpOa+kfhX8MdF+DPw90fwt4dso7DRtDtltbWJBg4Ucsx7uxyzMeWJJPNcpfXq+D/2jVuL5lhs9asRBbzP8qiQBRtz9R/48PWvWpYeMNOvc/l3OeO8dnWLqOLcaUVJxguttnL+bvbbTY8m+Mfgf/hAfGEdl9quLzzLVJmlmPzOSWB47DisLwzrLeHPE2l6ijbW0+7huVI6gpIrV6T+1laLF4u0m4GcyWhQk+ivn/2avJJScNX8H+I1N4LjDEyp9KikvwkeplVR4vLIqrq5Jp/ivyP04s7uO+tY5oWV45VDqc9QRUoNeRfBn4jTWvg3SpJd01tcWkUhX+JSUHTt+FeoaTr1rrkG+3lVx3H8S+xFf3hgayrYenWX2kn96R/M2Py2rhqrhJXSdrl6ik6etFd8djg5RaKbu96XPuaYcoppoORUV9fQ6dA0lxIsMa9WdgoFcV4r+JS30D2unhgrjDTEckd9o/qaTOjD4OrWlaC+fQ89/a+u/wDhJfAmuRw/vI7ONWUjvtZWY/ofyr5BUfMK+wvHmnLqngnV7fAbzrOZen+wcfrXx7E27afUV/In0iMLy5phsQvtQa+5/wDBP37gGKp4SdBfZa/Ff8A7n4W+H9L1rwb4smv4Va40+3Se3m6PEcPwPqQv517d8LD/AMJh8I9PXWIYb5Ly28udLhBKlyvT5lbIbIAPPXNfP3w40y+8VX8ug2KlV1Z4vtLgZ2RRsWJPoOc+5AHevqbSdKi0XTLezt12wWkaxRj0VQAP5V+yeDOIdXhPDp/Zcl90n/mcPFX7up7Nu7bUl5Ky+67/ACPzx/4KI/8ABHnTZNC1Lxv8INPXT7yzRrm/8MQL/o90gGWa0H/LNxgnyuVb+HaeD+aDIVB/UEc1/SG6Njr8ynIr8d/+Cx37LsPwE/aQj8SaTbeRoHxCjl1DagxHBfq3+kqPQPvSQD1d/QV+gYrDpJzR+oeFXHGIr1lk2PnzNr3JPfT7LfXTZ+pyfwwtVsvh3osafd+yq3Tuck/zrcxmsH4XXX2v4eaO4OdtuI/++SV/pXQHkV+8ZW08LTa25V+R/m7xupriDGqp8Xtal/XmYGmrJ5EiueQpDH8DTj0qOc/uX/3TXZWSdOSfZnztGVpJ+Z+nAm+0QxyLwsgDD6EZrw/9vuy+1/BexuM/8eerRH/vpJF/qK9m0WbztEs2zndbxkf98CvMP22rL7X+zhrDKm429zaS8H7o+0ICfyY1/KfDNT2efUZLpUt+Nj+iM8gquT1U/wCT9D4mTdHKrqzLJGwdGBwyMOhBHII619JfAH/gp/8AEH4QRW+n69s8c6JCQoXUJmXUIlHZLnkt9JFf6ivmtzg/rTQfev6mxuW4bFQ9niIKS8/8z+fMDmeKwkufDzcX/XQ/Uv4af8FP/hJ4/gjXUNYuvCN4w+aDV7ZhGD7TRB4/XqV6dK9G/wCGsPhf9k88fEPwa0I/i/tWL+Wc1+NwYjkE/nSGPLbtq7vXFfI1uA8JKV4Tkl8mfZ4fxCxsIqNSEZPvt+p+qPxJ/wCCnPwf+HkLLBr9x4ovF+7baPZvKGPp5rhYx/30a+Wfjj/wVi8dfEWCay8I2tv4H02QEfaInNxqTD/rqcLGf9xcj+8a+VTQvFd+B4TwGGkp25mv5n+h5uYcZ5jio8ilyx7JfruWdV1O61rUpry+uLm9vLhy81zcStNNMx/iZ2JZieeST1qDJzR1pD1r6flSVkfJSu3eTPqH/gnVes2ieLLXI2pcWso9eUkB/wDQR+dfQXi1zF4R1hl4K2Fxg+n7pq+c/wDgnSjF/GD/AMOLRfx/emvoL4kXJs/ht4kmX70Oj3kgPoRbyH+lfzNxpTX+s00uso/ikfv3Cs7cPxb6Rl+p+ccP+pT6CpF6UyI5hX/dFOWv6Xpqysfgzd22KRmuX+McSzfDHVtw3bY1cexDrXUE4rm/i3CZ/hjrSj+G23H6BgTXHm0b4Kqv7r/I+u4Bm4cS4CSdv31P/wBKR865py9KaQQOeM04dK/nfpqf7JdAooooAKKKKACiiigBG6Vu/DD/AJKd4a/7C9n/AOj0rCbpW78MP+SneGv+wvZ/+j0oX9fgc+K/gy9GTfGX/ksXi3/sOXv/AKUPXOjpXRfGX/ksXi3/ALDl7/6UPXOjpQZ4P+BD0X5IKKKKDoCiiigAooooAKKKKAGgfNX6Kf8ABAX4SQ6hr3xB8dTw/vLCO20K0cjOzzMzT4+oEIr866/XD/gg9psNn+yBrV0qkS3vie5Mpz97ZDCo/QCunCK9T5H5t4sYqdLh6pCD+OUYv0bu/wAj7XVAo4FY3jjwDpvxA0r7JqEbFVO6ORDtkib1U/06GtrctLuAr1db3P5SpynTkp03Zrqj5s+OngC78DLpcc+tXmrW7GSO3W4XDW4G04zk5z+HSvOz1r3P9r5MaNocn/TxIn5oP8K8LJyc1/C3jPT5OLMQ11UX/wCSo/XOG8ROtgIznvd/n5H1F8Adfi1v4XaXsZmazT7LJ7MnH8sH8a7mC4ktZlkikaKRejIcNXzn8DfGTfDe4t5r6XboeuO0YlIOyCdDjk9BwR+BHoa9N+JHxetdI0uO10W5g1LWdQIitYrdhLtJ6MccYHv1+ma/sng2s62R4Sq+tOH5I/P82yif1uVOkuaMm7Pou935dT1/SfiZfWEapcxrdKv8Wdrn056VtW/xT09x++W4iPc7N36ivn34f/E7UNP1pfD/AIsAt9VYZt7kgLHdD0yON3bjAPTr19Cds4r6iO2p8tjuHaVOfLNavVNPRruj0K4+Kmmwp+7+0TH0CY/nWPqvxTurhCtlAsH+0/zN/h/OuT71558cPE979u0TQdFvJ7PVNQuVcvE2GWMZHPfGeT7LRzE5fw7Rq1VTX3vZJK92ekalfXWqTeZdSvM/T5j0/CoenH55rzKw+L+tfDzUI9P8ZWf7pvlh1OAbopAO7D/AZ9u9d7pvjTSNXtRPb6lYyxkZ3CdePrzS1PXq5XUo7K8ejjrF/cW9ReMafcNIyrGsbF2J6LjnNfGeFEh2nK7iFPqO1fQXxR8ff8Jy/wDwi3hyRbqa8/4/ruI7oreEY3fMODnvj6deK8AmRY5JFU5VWIU+oz1/Gv5g+kZy8uCa3vP/ANtPveD8LKjGbqaOVtPLu/V7eR6n+yZz431Tjj7EBn/gYr3yvCf2SUDeItab+7bxjP1Y/wCFe7Gv0TwRp8vClF95T/8ASj5rij/kZTt5fkORsV8af8FzfAcPib9jKDWtq/aPDGv2k8b4GQs263YD6mRTj/Z9q+yxwK+Y/wDgsPcw23/BPnxos+395Pp6RZ/56G9hxj8Aa/Va1nTkn2K4Uqyp5zhZQ39pH8XZ/gfmv8DpPN+GOm9SVMqn8JGrrM1x/wAA3B+GlmP+msw/8iNXYdz9a/Y8ilfL6L/ur8j+T/FKmqfF+YwXStP/ANKYUyf/AFL/AO6f5U+muMg/QivUkrpo+Fjuj9IPA0jS+CdHdmLO1hbkk9z5S1yf7VNsb79njxZHz8tmsvH+xKjf+y10Hwluft3wo8MzNy0mlWrE+uYlqz8QvDreLvAWuaXH/rNRsJ7dBnqzIQv64r+Q8PW+r5uqj0Uan5SP6Xq0/bZY4R+1DT5xPzkPJ9sUg5FK8LWsjRyK0c0ZKSIwwUYHBBHbB4pv4V/X8JqS5ovRn8ySi4vlY4dKKbjijNWPZajqKbuozg1PKgv2HUh5pDx+daHhTwrqXjrWYdP0ayn1K8mYKqQru254yx6Kvqx4A5rGtWp0oudSSSXVmlOnOpJQgm2+x9Sf8E9fD7Wnw61zU2UqNQ1EQRnPVYoxn/x6Qj8K9f8AjBL5Hwg8XMeP+JJff+k8gqD4LfDn/hU3wu0fQWZZJrOHdcyIfledzvkI9txwD6AU347Hb8D/ABjj/oC3f/opq/lbM8yjj+IXiKe0qit6JpI/onL8C8JkioPdQd/Vq7Pz0hG23UeiinjpSDp+ApR0r+q4n88u1tAPWsfx/bfa/Aesxld26xm+X1whP9K2DVXWrf7Zo17F93zbeRM+mVIrHGR5qE0+z/I9bh6v7HNcNW/lqQf3SR8uvylA6UgP7sUo4r+b3uf7S05Xgn5BRRRQWFFFFABRRRQAjdK3fhh/yU7w1/2F7P8A9HpWE3St34Yf8lO8Nf8AYXs//R6UL+vwOfFfwZejJvjL/wAli8W/9hy9/wDSh650dK6L4y/8li8W/wDYcvf/AEoeudHSgzwf8CHovyQUUUUHQFFFFABRRRQAUUUUANPGK/YD/ghpZG1/YieY5xdeI7+QfQeWn81Nfj+Wya/Yv/giMw/4YM01R95Nb1IN9TcEj9CK7MBG9S/l/kfk/jFJrJIrp7SP5SPrsDdSlTikRsU7dXr8p/MZ4/8AtfRlvDWin+7eP/6LrwcnivoD9rVN3grTW/iW/wAD8UavADX8PeOcOXiup5wh+R+pcI3/ALPXqz3v9nHTLLxf8K7zTb62huoYr51aNxkAkKw+nXqK7jwr8LND8FXEk2m6fDBNIMGRmMjj2BbOPwrxX9nv4qWngDULyz1KTyLG+IkE38MUg4+b2Ixz2wK+gtP1+01eBZra6tp42GVaOQMCPwr+jPCfiLC43h3D0I1F7SnHllG6urOy031Wp8bxBRxNDFVIJtQk76Xs/wBDJ8f/AA5sviHof2S7XZJH89vOn34W9R/UVxun+MvF/wAMl+y65pM2v2cRIjvbTmTA/vDHP44P1r1JpeM5A981Xk1S1ibDXNqnc7pVWv014qlH4ppfNHlYbGSjD2NSPNDs76ejWx53N8b9U16Iw6H4V1SS6Y7Q90uyOPPc9v1FaXwz+Fd1pGrXGva9cLfa9egru/gtk4+VffjH04HeuuuPE2nwKfN1CxVfUzpj+dU5/iP4fs03S63paKvHN0n+Nc1XNsHD460F/wBvL/M1qYmXI6eHpcie71bfld9DSvtLt9StWguIYp4ZBho5FDK31Brj7/8AZ58J30+7+zWi3HJSOd1T8s4H4VbvPjt4Rs927XLJtpwRGTIf0BrI1P8Aaa8K2YYxTX10y9BFbnn6FsV4+I40yLDJ+3xdNW/vL9LjwdDMIfwFNX7XSNLxLpWk/Cj4bavcabax2gjtnCkHLSORtXJPJ5I6+tfLZm3RquB+VegfGP46TfEu3WxtrZrPS4237XbMkzDpuxwAOoHPP0rz3pX8k+MXGmFz7M6ccDLmpUo2T7tu7a8j9D4Zy6th6MquJvzzd9dXbpc9h/ZB+bWNePpBD/6E1e6dhXhH7IkuzX9cj7vbxfox/wAa92PA+lf0R4Ju/CVD1n/6Uz4fibTMp38vyQ6vkT/gt1qDWv7Cd3EG+W81/T4mHqA7SfzQflX1zuNfEf8AwXk17+zP2TfDtjuXdqfieEbe7COCZ8j6HH51+pYj+FL0Ojg2n7TPMJFfzx/B3Pg/9nlt/wAOUz/BdSgfjg12xOa8/wD2bbjzfBN0n/PO9bA9Mopr0A8V+vcNz5stov8Auo/l3xooOjxxmUX/AM/W/vs/1CjvRQOW/GvcPzE/QL9ny7+3fAzwjMTuLaVADz6Lt/pXYHmvNf2QtRTUP2c/Dez/AJdkmtznuVnk5/IivSia/jvPqfs8yr0pdJy/M/pzJ6nPgaM1s4r8jzX4rfsoeEfizqb6hcW9xpuqTHMt3YuI2nPq6EFWPvgH3rz2T/gnbpe47fFupKvYGwjOP/H6+jCPamtHk16OC4xznB0lSw9dqK2Ts7fejjxnC+V4ip7WrSTk/VfkfPln/wAE79BjVjP4m1qY9tkEUf8A8V/Sr9v/AME/vCMY/eap4ilPqJok/wDaZr3UDC4pR0refHOez3xD+Vv0Rzx4RymP/LhHi9l+wX4Dt/8AXN4guD23XwUD/vlBWxYfsZfDnTgv/EhkuGUYzPfTvu9yNwGfpXqFB6Vx1OK84npLEz/8CZ2w4cyyHw0I/dc43Sf2d/Auij/R/CPh/dxky2omJx6788+/Wuq0zSLXRbcw2dpaWcP/ADzt4ViX8lAFWAeKcDkV5OIx2Kr6VqkperbO+jgMNSd6VOMfRJDSSRzXO/F22+2fCHxdFjc0miXwA9T9nkx+uK6M9ag1CwXVtMubRiVS6heBiOoDqVJ/WowUlTxEJvo0/wATTFRc6E4LqmvwPzPiOY1PsKepyKlv9Pk0fULizmXbLZytBIP7rI20/qKhB4r+zaMlKCktmj+X6lPlk0+mg49KZOnmQsv95SKXORSk5p1NYNeTNMG2sTTa/mX5o+VJRslkUfwsR+RoHSiYj7RJ/vt/M0L0r+bq2lRrzZ/tdgJN4Wm3/KvyQUUUVmdQUUUUAFFFFACN0rd+GH/JTvDX/YXs/wD0elYTdK3fhh/yU7w1/wBhez/9HpQv6/A58V/Bl6Mm+Mv/ACWLxb/2HL3/ANKHrnAeK6H4w/N8YPFp/wCo3ff+lElc7jihq2jM8JpRgvJfkh2aN1NxyaMdP8KDo0HZozTQOPxoI/zigB2aM03H8vSgDpQA7NGaaRx+NBFAAePev0L/AOCOH7dngb4KfDfW/APjfXbXw3JJqp1LS7u8DLazLIirJG0mCEZWTI3YBD8cjFfnoB/L0psjGKCRh95VJHFa0ans5cyPneKuH6GcZdPB120viTW6a23+4/pStvD2oXAHl2dy+4ZB2HBHYg1o2vw/1a6XP2ZYl9ZHC/411fwu1j/hJfhn4Z1IsrtqGk2lyWH3SXhRjj25roBwnavfP4BrZ1iVJwSSadvuPln9s/wdeeHvhzps9x5IVtSVRtfdyY3Pp7V81da+wP8AgoYP+LR6T/2F0/8ARUtfH2cV/EPjv/yVU3/ch+R+2eHmLnWynnqvXmkvyHFyaIZGtzlMx/7pxTep9aP+A1+P06s4O8G0/J2PuJRjLcmkvZpE2tNMw95WOP1qIbSmNqn69aTqRQWwOtbfWcRUdnNv5tmXs6cdbJAY0J/1a/lTlwn8IpuQeN35VY07SrrWLgw2drcXUijJWGMyMB74qIUa1Sfs4Jt9ldsUpU4x5m0l6kJfd2pd/tS3VtLZXLQzRyQyKcMkiFWX6g1H07GoqQnCXLNWa76MqMoyXMth2c0jUnWgH61OhopI9o/Yg0VfEXxI1WzabyWbTjIrYz92Rf8AEV9HXvwx1KAbofJuV7hW2t+Rr5r/AGFr/wCyfH+3hL7VvNPuYsDuQFcf+gk19sxrgfrX9ueBNbn4XjBfZnNfin+p+C8eYyvhs4lGD0cYu34foeQ6zo114c0m8vr63kt7LT4ZLm5mYfJFGilmYkZ4Cgmvx0/4K8ft1+GP2tfFXhfQ/A91JqXhvwqs9xLqDRPCl5dS4T92rgMURF+8QMmQ44GT+2X7TV2tj+zb8RZmbb5fhfU3BzjkWkpr+YWwbdYwnbt/dqSMdOK/V8bKSiorqfqHgXgaOZYqtmGKXvUWlFdLyTTb9Oh7B+zDfr5OsWu75t8c6j2wVP8AIfnXquzJ/GvlrRtevPD16t1Y3E1rOoxvjOCR6H1Hsa/U7/gid8LfAf7UPwF8R33jbw3p/iLxJ4d142xuLtnObeSFHjBjVgmAfMHK849q+uyDi6jg8FHD14tuPY/N/pAeCeYVs2r8VYarH2VRwUou91KyjfZ6Ox8r2kLX9ysMCyXE7HCxxKZHJ6cKMnrxXXab+zz4/wBWETWvgXxlcCb/AFZTRrjD/Q7MV+w/g/4d+Hvh3B5Ph7w/oegx9CNPsYrbdj1KKCfxrc8yRurbh7nNdNbxAlf91S082fz7Q8Oo8t61bXyS/U+GP2VfhZ4o+EfwatbHxVpdzo11eXc93a2tzhZlhbaAXUfdJYOcHnGMgV6P/FXqP7R2mKbXS70fwu8DH1BAYflg15d6V+D8RYiVfM6leSs5O+nmfqGV4SOFwsMNF3UVa4tFFFeSegMkcRqzMVVVBYkkKFA6kk8fnXhXiz9vvwvoetSWunaZqetwRNta7jdYY5D6oGGWHuQAf1r1z4j+Hrjxb8Ptd0m1ZUutS0+e2hYnADujAZP1P86/Oma3ltLh4po3hmhYxujDBjZTggjsQQRX6b4e8K5fmvtamNbbjZKKdt+p8BxpxBjculTjhdFLd2v8j9DPhb8WdD+MXh/+0tEuXljjbZPDIuya2fGdrr/Ijg9u9ea/tD/tjWvwq1p9E0G1ttY1e3JF3LM7C3sz/cwvLv6gEBe/PA+Yfhd8Xdc+D19qN1oc/wBnm1KzazdjnCZIKyAf3052k9M1zDFnZmdpJGYlmZiWZiepJPUk9z1r7HL/AAuwtLMZ1MQ+ajpyx6v19D5jG+IOJqYOFOj7tX7TS/I+8f2bvjp/wvfwRNfzWcen6jp9x9mu4Y3LRsxUMrpnkAg9DnBB5NehrxXgH/BPXSpLb4Y65et/q7zVPLT/ALZxLn9Wr39a/I+LMFQwmbVsPhlaEXou2iv+J+lcO4mviMvpVsQ7ya3BjSZ/PrTicU0jatfOvY9t7Hnes/8ABJ5fjHdTeKYPHEmhz65e3V5PaTaULqNFedzGY2WVD9wrkMDz37CvqX/BFcmIfYfiV8+DuFzoGFPpgrcZ/SvtjwXYf2b4S0yDp5drGCPfaCf5mtTymI9q/YMv4ozSjQhTjPRJLVLsfIYnhPLK9SVWpT1k7vVo/OPX/wDgjp480kTSWfirwhfQRIXLyG4tmAHJyNjDge5z6V+fPiH9pCzexkj0mznmmkVlSaUhI1zkBwOSfUA496/ef9qHx8vwu/Zo+IXiJnKNovh2+u4yG2kOsD7cHsd2Ofev5trGNorSJWO7YoUt68V6FTjDM503DnWvkfr3g34LcN5tWrYzH0nJUnHlXM0m9Xrbe2mhJs2HmnDignI60hA/T1r5WUbu5/ckYqKUVshc0ZpABx/jQQMde9TylC5ozSEDn/GlA5/+vRyhcM0ZpMDjmjAx/wDXo5QBulbvwwP/ABc7w1/2F7P/ANHpWEe9bfww4+Jvhv8A7C9n/wCj0pNWOfFfwZej/I//2Q==");
	        mapData.put(2 + "/Certificate/Page/CZR", "张男");

            // 加载customDatas.xml，构建Ofd元数据信息对象
            Path pathMeta = Paths.get(sDataPath + "customDatas.xml").toAbsolutePath();
            InputStream isMetaXml = new FileInputStream(pathMeta.toFile());
            MetaInfoModel metaInfo = OfdCreateAgent.buildMetaInfo(isMetaXml);
            isMetaXml.close();

            File fFormOfd = new File(sDataPath + "form.ofd");
            OutputStream osFormOfd = new FileOutputStream(fFormOfd);

            logger.info("03-开始生成底版OFD：" + (System.currentTimeMillis() - start));
            // 基于底图路径+样式生成底版OFD
            result = OfdCreateAgent.imageToTemplate(sDataPath, templateInfo, osFormOfd);

            osFormOfd.close();

            if (result.getStatus() == 0) {
                logger.info("03-结束生成底版OFD：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("03-失败生成底版OFD：" + result.getMessage());
                return;
            }

            logger.info("04-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 套模板转换生成ofd
            result = OfdCreateAgent.createOfdByTemplate(sDataPath + "form.ofd",             //底版OFD文件
													templateInfo,                     		//模板信息
													mapData,                           		//数据信息
													metaInfo,                       		//元数据信息
													sOutPath + outFilename);                //输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

    /*
     * 套模板生成OFD文件，构建信息对象，单步生成OFD文件
     */
    public static void OfdCreateByTemplateTest8() {
        String outFilename = "cnofd-result-8.ofd";

        long start = System.currentTimeMillis();
        logger.info("01-开始测试：" + outFilename);

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));
        start = System.currentTimeMillis();

        try {
            ResultInfo result = new ResultInfo();

            // 构建模板信息对象
            TemplateInfoModel templateInfo = new TemplateInfoModel();
            TextBox textBox = null;
            ImageBox imageBox = null;
            TableBox tableBox = null;

	        TemplatePageModel templatePage1 = new TemplatePageModel(1, 210f, 297f, "jpg", "temp1.jpg", Paths.get(sDataPath + "temp1.jpg").toAbsolutePath());

            textBox = new TextBox(0, "/Certificate/Page/CZR", "50 76 50 10", "宋体", 14, "持证人");
            textBox.setColor("0 0 255");
            textBox.setMultiLine(true);
            textBox.setLineSpace(1);
            textBox.setFontSizeAuto(true);
            templatePage1.getListTextBox().add(textBox);

            textBox = new TextBox(1, "/Certificate/Page/DJRQ", "50 92 50 6", "楷体", 14);
            textBox.setTextAlign("center");
            textBox.setBold(true);
            templatePage1.getListTextBox().add(textBox);

            imageBox = new ImageBox(2, "/Certificate/Page/HYZP", "125 76 69 46", "合影照片");
            imageBox.setAlpha(255);
            templatePage1.getListImageBox().add(imageBox);

            templateInfo.getListTemplatePage().add(templatePage1);

            TemplatePageModel templatePage2 = new TemplatePageModel(2, 297f, 210f, "255 192 0", 255);

            textBox = new TextBox(0, "/Certificate/Page/CZR", "20 20 80 30", "宋体", 14);
            textBox.setColor("0 0 255");
            templatePage2.getListTextBox().add(textBox);

            tableBox = new TableBox(1, "/Certificate/Page/BG2", "150 20 50 40", "表格");
            tableBox.setBorderStyle("solid");
            tableBox.setRowHeight("8");
            tableBox.setColWidth("20 40 40 50");
            tableBox.setBorderWidth(0.8f);
            tableBox.setLineWidth(0.4f);
            tableBox.setColor("255 0 0");
            templatePage2.getListTableBox().add(tableBox);

            templateInfo.getListTemplatePage().add(templatePage2);

            // 构建数据Map键值对
            Map<String, String> mapData = new HashMap<String, String>();
            mapData.put(1 + "/Certificate/Page/CZR", "张男很长很长很长很长的姓名");
            mapData.put(1 + "/Certificate/Page/DJRQ", "2021年11月15日");
	        mapData.put(1 + "/Certificate/Page/HYZP", "/9j/4AAQSkZJRgABAQEAeAB4AAD/4QCyRXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAE+AAUAAAACAAAAYgE/AAUAAAAGAAAAcgMBAAUAAAABAAAAolEQAAEAAAABAQAAAFERAAQAAAABAAAXEVESAAQAAAABAAAXEQAAAAAAAHomAAGGoAAAgIQAAYagAAD6AAABhqAAAIDoAAGGoAAAdTAAAYagAADqYAABhqAAADqYAAGGoAAAF3AAAYagAAGGoAAAsY//2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAFAAeADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD44+MfHxg8Wj/qN3o/8mJK5zbmuk+Mv/JYvFv/AGHL3/0oeudHSvzM/wBXMH/Ah6L8kN280bf85p1FB0DdtBWnUUANK/5zRtp1FADdtG2nUUAJ/nrSYz/+unUUANC4o/D9adRQA3bzQBgf/Xp1FACf560f560tFAxv4frQVzTqKBDcY/8A10Yz/wDrp1FADdtL/nrS0UANIzRjH/66dRQAmP8AOaTbxTqKAE/z1pMf5zTqKAGgYH/16X/PWlooAT/PWk/D9adRQMaRmjGD/wDXp1FAhv4frRt5/wDr06igBoGP/wBdGMinUUANC0v+etLRQA0rkUYx/wDrp1FACYz/APrpAtOooAT/AD1o/wA9aWigY38P1pcf5zS0UCG7aX/PWlooAbjmgDH/AOunUUAJ/nrSbf8AOadRQA3H+c0EZFOooAaRitz4YLn4meGl/wCovaf+j0rFPStv4X/8lQ8N/wDYXs//AEelHUxxP8GXo/yJ/jL/AMli8W/9hy9/9KHrnR0rovjL/wAli8W/9hy9/wDSh650dKCcJ/Ah6L8kFFFFB0BRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAAelbfwv/5Kh4b/AOwvZ/8Ao9KxD0rb+F//ACVDw3/2F7P/ANHpQt/68jHE/wAGXo/yJ/jL/wAli8W/9hy9/wDSh650dK6L4y/8li8W/wDYcvf/AEoeudHSgnCfwIei/JBRRRQdAUUUUAFFFFABRRRQAUUUUAFFFFABRRQTigAJxQTikY1f8MeFdS8a6ulhpNjPqF443CKIdB6seij3JArOpVhTi51HZLdvYzq1YUoOpUaSW7ZnlqA1e0eH/wBiXXL6COTUtX03T93JjiRrhl9sghcg+hIrRn/Yan8k+V4ohaTsHsCq/mJK+bqcZZPCfI6yv83+Sa/E+TqcfZDCbhLEK/km/wAbHg5bFKDmvXtY/Yu8S2S5s9Q0jUPRSzQM3/fQ2/ma818WeBdY8B6h9l1jT5rGfsHwyN/uupKt68HNelg88wGLfLhqqk+3X8T1cv4jy3HS5cLWjJ9r6/czLooJxQDmvWPbCiiigAooooAKKKKACiiigAooooAKM0E0hORQAuaM5FN3Ub1UfeFAXVrscDmgmn2lncX3+pt55u/yRls/kK0IPBGt3ePL0jUmz/07v/hWMq9NbyX3nLUxuHp/xKkV6tGWTS1tH4aeIwN39iajt/65Gqd94Y1TTFLXGm30KryS8DgD8cYojiKT2kvvREMywk3aNWLf+Jf5lGikDZ/z0peR2ra/U7U01dBRRnmigAooooAKKKKACiiigAooooAKKKKACiiigAPStv4X/wDJUPDf/YXs/wD0elYh6Vt/C/8A5Kh4b/7C9n/6PShb/wBeRjif4MvR/kT/ABl/5LF4t/7Dl7/6UPXOjpXRfGX/AJLF4s/7Dl7/AOlD1zo6UE4P+BD0X5IKKKKDoCiiigAooooAKKKKACiiigAooooAKaetOpp+9QBe8L+Grvxn4is9KsVVru+kEcYP3QepY+wAJPsK+vfhn4c0L4QaQ2i20LW0qkNcXcq4a9fu5b09B2HvmvGP2KdEjvvHurX0ihm0+xVIsr91pHwTn12qfzr6OvbezvZxDcLDJJtyqt98A+ncfhX47x7nLni/7Pd+SKTdt7v/AC7H4H4pZ9Vni/7Mg2oRSb82119CzDdw3seYpoZB2KuDT2wmcsoC9STgCsO7+H9nMd0LTRZOeGz/ADqH/hX8I2+Zc3Eij+HgV+d/V8FL/l4/S2p+RcqHav4w8x/s+nr51xISgfHAPt6/ypt38ObHX/CF1pOrQrdR3w3XLE5ZZMYDqexXOQfXPY1qaXo9to0bfZ49pIwWJyx/GrkP3fXg054tUUlhPdtrfq3/AJGtGtUoyVSi7SWqfmfCvjDwvN4K8WalpFz802m3DQlh0cA8N+IwfxrPHSu0/aQ1CLUfjt4kkhbcqzpETjqyRorfqD+VcXX9K5XWlWwlOrPeUU39x/YmU1p1sFSrVPilFN+rWoUUUV6B6AUUUUAFFFFABRRQelABmk6Um7bXovwz+BEviOKG/wBY8y1sZBujgU7ZZx1Bz/Cvf1Pt1rlxeMpYaHPVdjys2zrC5dQdfFSsui6vyS6nDaJoF94mvRb2FrPdS9xGudo9z0H1Jr0Lw3+zRe3e1tS1CGz7mKAebJj/AHjgA/nXrGjaJZ+HtPW1sbeK1t16JGMA+57k+55q0o218bjOJK9R8tFcq/E/GM48TMdXk4YJKnHu9X/kjjtJ+AvhvSNu6zmvJAfv3MxbP/ARhf0rorDwvpulD/RdPsrcdf3cCDp05xmtEjd1orxauMr1Pjk/vPhcVnOYYh3rV5S9Ww8xgn3mAHAAOMU0sc9TRjJpduK86er1PKk237wmMHp+lAdkH3mGfQ07bzSbaS01Qlo7ox/EHgPR/FIP27T7eZif9Zt2yL9GGDXmnjb9ne40+JrjRp2vIl5NtL/rQP8AZI4b6HB+teybaay59q9PB5tiMPK8ZadmfSZPxZmWWzUqNRuP8r1T/ryPlOW2kt5njkV45I2KsGGCpHYjsRTT8pr3z4nfCS38b273FqI7fVkHySdFn/2X/wAe1eD31lNYXstvcRtDcQttkRuqEdRX6Dlea08bC8dGt0f0BwzxRhs4o81PSa3j28/NEdFHSivUPqAooooAKKKKACiiigAooooAKKKKAA9K2/hf/wAlQ8N/9hez/wDR6ViHpW38L/8AkqHhv/sL2f8A6PShb/15GOJ/gy9H+RP8Zf8AksXi3/sOXv8A6UPXOjpXRfGX/ksXi3/sOXv/AKUPXOjpQTg/4EPRfkgooooOgKKKKACiiigAooooAKKKKACiikzxQAtI3FGaQtg+mTge/wDnNTJpasLpas9u/Yev44vF/iCzZsNcWUcic9djkH/0IGvfte8Pf2rGskb+TdRcxyDv6A180/sveGfEWk/E3T9Xj0bUv7LZJIbmYwmNDGyHGC2M/OFPFfTlr4ls7l9vnLFNnBjkGxgfoa/B+OYyjm7r4eSd0tnfyaZ/M3iXGn/bTq05KSlFPR31St0KOjeKSrm21L9zcKcBiPlf6/4962pGwisuDu7jmq2qaPBqtvtlj3ehHDD6GsU+G9S035bG8bb2Vjj9CCP5V8tyYeu+ZS5JdU9n6dj4A32O4U+3OP5VgW7+IFlVNlqwxyW2/wBOa5/49fGpfg/4aiFvGtxreoBktoz9yLH3pW74BIwO59OaujlNbEVo4ehaTlpo729fI7cvy2vj8RHCYdXlJ2X+b8l1Pnf9pGSC4+O3iRrfbtE6I+3oXWJA/wD48Dn3zXFg5FFxcSXVxJJLI0s0zM7uxyzknJJPrSFsV/SeXYX6vhoUL35Ul9yP6+y3CvDYWnh278sUr+iFopN1LXadoUUUUAFFFFACMcUhPH9aGGa3Phz4RPjbxbbWJDCE5knYdo16/nwPxrKtWjSg6ktkc2NxdPC0J4ir8MU2/kjtvgd8I11JY9c1SHdCp3WcLjIkI/5aMPQHpnrjNevAU63ijjg2RqsccahFRR8qqBgAfQDH4U1a/LsdmE8VV9pL5eh/LHEOeV80xbxFV6fZXZdBwGKKKK5TxlsBGaKKKBiDrS0g60tYy3MwoooqQCmn71OpCOaANCz8L3WoaaLqFopA2cR5O7j9PwryT9oP4efb7T+3LaMi6tRsvF6F4xwH/wB5en0PtXr3hPU20/U1Tc3lTHYV9D2NT/EHS0e5Jkj3Q3kbRyrj5W4wR+Kmoy3NK2Dx6jLZ7W7dUelkOcVcsx8MRTez1XddUfGzfeoHSr/i7w+/hXxRf6fJz9lmZUOPvpn5G/FcH8az16V+z0qinFTjsz+tMPiIV6Ua1PaSuvmLRRRWhsFFFFABRRRQAUUUUAFFFFAAelbfwv8A+SoeG/8AsL2f/o9KxD0rb+F//JUPDf8A2F7P/wBHpQt/68jHE/wZej/In+Mv/JYvFv8A2HL3/wBKHrnR0rovjL/yWLxb/wBhy9/9KHrnR0oJwn8CHovyQUUUUHQFFFFABRRRQAUUUUAFBOKKa1ADs0xj37U+GB7iaOONHkllYIiINzOScAAdzkjivpD4E/swQeFVg1jxJHHcasCJILM4eGzx0Ldnkz+A9z08PPM/w2WUfaVtX0S3Z89xFxNhMmw/tcQ/efwxW7f6ep5z8KP2YNc+Ikcd7fE6LpEgDJNKmZrgHuiddv8AtNj6GvoHwF8CfC/w52yWOlxXF2ox9rvAJ5z9CRhf+AgV1ynI+tKOBX4bnXFuPx8mpScYfyrT7z+duIOOMzzSVpT5IdIxdvv7gW3MCWyR69qiv9MtdUTbcRxyY6HHzfnUiSK7MAV3KcMB2PpT6+XjKcXzJtM+RMlfDLWRJsr65t1znaTuX8qGttZi+7fWj9vmhxWtimydRXVDGVNpWfqkwMeTRdS1D/X6p5cfdIE2k/596rXfw10LVFj/ALQ0uz1J4shXvIhMRnGcZ6ZwOldAjBetIfmY1p9erKXuPl9NPyNKVapSlz020+6Ofm+DXhGVGH/CM6DgjHFlGP5AVy/iX9k3wbr0Ehgs7jSbgj5Xsp8KD/utuBr0fYKXFdVHOMdSlzU6sk/VnqYfiDMqEuelXkn/AImfNXxs/ZeufCHh7Tb/AEWGTUvsduYtT8mPazFTlZ9mSeQcNjuoPQnHjY+7nBx1r753sG4+8K8j+Pn7NFt4zhm1jw/BHa62P3ktunyx3578dFk9+jd+ea/QuF+OpKSwuY632n+jX6n6lwf4lPmWDzZ7vSfr0a/U+Y6KdPC8EjRujRyRsVdGUhkYdQR2I9KbX61GSauj9tjJSXNHZhRRRmqGIxyK9j/Zv8Hzabo11rVxDtXUsRWpb7xjRjuYD0LYA9dhrzP4f+D5/iB4103R4cqb2cLI4H+qjHLv+Cgn64r6t8V6Xb6JPZ2NrGIbWztY4YYx/Ai5UD8hXxfFWcKjKOBh8Uld+iPyvxOz5UMNHLafxVNX/hX+ZmAcUmcdqTpTq+RjK6ufhK1CijNFUUFFFFACDrS0g60uaxluZhRRnNFSAUZooxQA62uPss6ybd3lsGxnrg5ra8Z63b6n9njt38xVy7NjGM44x61gsea0n8L3EeiC+LR+WV3lDkNt9cVx16dH20KtR2avb5mbgnK54/8AtEfDF5tI/wCEos9zLC6W9+n9xTgRyD2ydp/4DXjynP8A9avtDwhplvrOk31neRCe1uVMU0ZGQ6spBH4ivkbx94LuPh54y1DRrhiz2EpRZD/y1Q4KN+KkH65r73hPPPbzqYCo/ehqvNf8Bn734Z8RSxOHll1d+9T1j/h7fIyc0U1adX25+qhRRRQAUUUUAFFFFABRRRQAHpW38L/+SoeG/wDsL2f/AKPSsQ9K2/hf/wAlQ8N/9hez/wDR6ULf+vIxxP8ABl6P8if4y/8AJYvFn/Ycvf8A0oeudHSui+Mv/JYvFv8A2HL3/wBKHrnR0oJwn8CHovyQUUUUHQFFFFABRRRQAUUUUAFIT1oJr0X9mj4Wf8LH8ei4uo92k6NtuLjcPllkz+7j/EgsR6KfWuHMsdTweGniau0V9/kefmmZUsBhJ4uv8MVf18vmem/sufAZPDum2/ibWIP+JndJ5ljA4/49IyOHI/56MOR/dB9Tx7O4+agnBG7JJoJ3Gv5szbNK2YYmWJrPfZdl2P5PzzOsRmmLlisQ99l0S6JeSHIPlpx4FNj+7SuMqfpXmKN5WPFe5meGZ/tOmNM2P380jYH+8R/hWpmsHwNMZNJaE/fhmIYEdM4NbvetsZFxrSTC+otNk6inU2TqK547lDaKKK2AKFXdQTiqOs6o1m9rBDg3FxKFAPOF7n+laU6UqkuWAF3GG/HrTi60Mp5NIeaxaUgPK/j1+zxpPjg3GuQ3kWi6kibriaQf6LMB/HKBypAABcduSDivnTxv8PtY+H19HDqlm0CzjNvOpDwXK9mjcfKwPXrn1Ar7fIV12sqsrZVlYZDAjkEelfLfxFh1L9nzxteaNDDHqHhPUs3MOm3gMlrJEzHKA9UdDxuXBxtPOTX6jwLxBi5t4OUublV0n1S3Sfddj9n8O+KMbUvgJz5+VXinvZb2a6+T3PKgKTvWx450rTNN13do10bnTbqJLmBXbdNahhzDJ/to2R7jae9Y55r9co1VVgprqfteHqe1gprr0e6PdP2JvCS3Ws61rsigmzjWyhOPus/zOR/wFVH41618QV26vAe5hx9eawP2P9K+wfBa3m2kNf3txO3HXDBAfyT+ddT8RoN32WQKfl3IT+v9DX4Rm+PlX4hqN7JuK9Ev+HP5c41xzxOe1m9ovlXol/mcwBzSNxQOlaGhaRHfCaa5d4bS3XLOv8R9B/nvXoyrKnC7PnB6eFprnXrHTLWRbq7vpIoY1XgGSRgqqPXkirnxL+GWufCDxzqHhvxFYyabrGmvsmgc8EfwujdGRhyGHBH417Z/wS/+CMnxa/af0vUZod2leCl/tm4Yr8vnqdtun+95nzj2iNfoN+1D+yJ4X/at8GLY6yslnqtiG/s3VYFH2iyJ5Ix0eNsDch68kFTzX1mU8N1cVgXXT96+l+qPncdniwuKVKSvG2r6q+x+N7Lselrv/wBon9mTxf8AsxeLzpfiixYW8zEWWpwKWstQXn7j44bHJQ/MPTGCfPt1eFXozozcKqs0e9QxEKsOeDuhR1pHqzoumtrWt2dikkcT308durv91C7BQTjnGT2q1428G6p8OvGOpeH9as5tP1fR7hra7t5Bho3H81IwwI4KkEcEVz8kmudLTuVKcVLlb1MwcU6vUf2U/wBnST9p7xD4q0GzmePWrHw/NqWkruwk1zHNCBG/+y6s6+zEHnGK8vubeayupre4hmt7i3dopYpV2vE6nDKwPIIIIINaTw840o1WtJbeqM44inKpKlF6x3EopuTWvc+BNWtPAVn4nktJP7Dvr6TTIrocp9oRFcxt/dYqwIz1AbH3TWMYSkm49DSUlG1+pksKtzeIbqfS1s2ZfIXHRcHA6A1TPPuKD1rP2UJv31e2o3Hqzrvh/Ds0uaT/AJ6SY/If/XrxT9tvwotvrOi67GoX7ZG1lOQPvMnzIT/wEkf8BFe9+HLD+z9FgiYbW27nHuea8+/a80gaj8F55tv7yxvIJ1IHYsYz+j14/D+ZOnn0asdpS5X6PQ+j4HxzwueUZLaT5X6M+UxwaWmilya/oI/qzlaFopPm9qPm9qA5WLRSfN7UfN7UBysWik+b2o+b2oDlYtFJ83tR83tQHKxT0rb+F/8AyVDw3/2F7P8A9HpWHzW58Mfl+Jvhv/sLWn/o9KFv/XkYYpfuZej/ACJ/jL/yWLxb/wBhy9/9KHrnR0rovjL/AMli8W/9hy9/9KHrnR0oIwf8CHovyQUUUUHQFFFFABRRRQAUUUZoAaTivsT9nfwF/wAIB8LNNhkj23l8v227+XB3vyqn/dTav1B9a+Vfhz4d/wCEt+IWi6aV3Le3kcbj1TOX/wDHQ1fcXU8fKOwHavynxMzBxhSwcevvP9D8b8Ws0cKdHAwfxe8/RaL8Ruyl8v6/nUV7fx6eitJuVGbbux8qe5PYVOGz3r8h95K7Pwp3BRgUHOaXNITmpi9bknOQJ/YHi943Zlt77lSezf5/nXSMuFrN8RaGus6cVXb50fzRsTjn0z71W8K+JhqVxHpcwb+0mcRRxKNzzMegAHJY+w5r1a1N4qmqtLWSVmv1sNvqbW7NMkPIrrNG+BnjbxGB/Z/gvxheAjIaHRbllYeoOzBH41qH9lL4oOBt+HPjg/8AcGn/APiaxp5XjXqqUn/26/8AI55Y7DrepFfNHn27ilzXdXX7LnxMs4y0vw78bqo5z/Ytwf5Ka5/xH8MvFPg7TJLzUvCfiqzt4zgvLo1ygz6coK0/s3GXt7GX/gL/AMhwxlCW0196MWVkgjaR22qgySe1YOgltf16TUZE/dQL5cI7KT/9b+dQTyX3jKfykjktLaM/PuyCTnpjGSfbtXRWNlHp1okMa4WMY9zVVP8AZIOEn+8ktl0Xb1Z0cy6Ep+4fpTacW+VqbXmUxgo5z715r+1b4CXxh8MJr6OPdeaCftiberRdJR/3z83/AACvSulNeNbmNopQrRygo49VIwR+Nd2W4yeExcMRH7LT+XU9DKMwngcdSxcN4tP5dT4JcnPr7U3+KtPxp4dfwf4r1DS5Ad2n3Dw89wDwfxGD+NZh4Nf1BQqxq0o1I7PVH9iUa0K1ONWO0kmvmkz7D/ZfTHwF8Okf8858/wDf+Wur8XWBv9CmCqWeL94oHt1/TNcP+yTfi7+BunJklrW4uIjx0/eM2P8Ax6vSGbPuM9PWv5pzipKhm9WXWM3+Z/IvE9NwzfERf88vzPN7K3jnuo1mm8mFj8zhdxA9q3NK0TUPiT4j07w34b0+4vLm9lEFpaQLmS5c/wATfTBJJwFAJOAM10nw1/Zj8YfG/wAbx6T4T0W6vY5pNrXrRstlYDrmaUAqgAOccsRjANfpl+x1+xR4b/ZM8PNJDt1bxXfRbNQ1eRcMy8ExQj/lnFkdOrYBYngL+lZDkM8znDEbQ7/5abnyeZZxTw8bL3pdr/n2L37Fn7Llv+yr8HodHZ4brXtRkN5rF2g4lmIACKf+ecajaPXluNxr19TtpIxinV+y4WjGhTVKGyPga1SVSbnPVvcxfH3gDQ/in4UutD8RaVZ6xpN4MS21ygdD6MO6sOzLgjsRXwH+1P8A8Ejta8JNcax8L5pPEGmAl30W6lVb62X0ikYhZl64VsOAAPnNfotTcbhXLmOT4bGxtWWvfqdGDx9fCy5qL9U9mfhLqNte+Dddkhu7e40/UdLnHmQ3ERjkt5EOfmVsEY64Ir9O/wBvD9he1/av0C38VeGfs1n42t7ZSjORHHrUBUFYpW/hdf4HPTJU/Lgr7L8a/wBmfwJ+0RZxx+MfDdhq8kI2x3J3Q3UQ/urNGVcLyeM45rtLKxi061it4V8uG3RY41ByFVRgD8AK8PL+F40Y1aFZ80JbdGn8z0MZnk686damuWcb+aPzS/4Jf6Brnwr/AG5JNB1vSLzSdSbRL2C6truExyxBTG2R2I3IMMMgg8E171+3T/wTTh+O+s3XjDwVNZ6V4qmXde2U37u11ZgAA+4D93NxgsQVbvg8n64ZMyiQhWcDaGI5A9M+ntS7q7aHDtCOFeDre8rtp7NXOWrmtaWJ+tU/dlZL1sfhz8R/hh4i+D3imTRfFGj32iapHlvIuEx5ig43Iwyrr/tKSPevrL/gmd4E0z9pD9nz4q/DHXMfZJprXULSUDc9nNIjoJkz0KNDGeMZyR3NfdXxg+Cfhn48eDptB8UaXb6lp8mShYYltn7SRP1Rx6j6HI4ryn9hz9iRv2Pr/wAZTSa1FrR1+4iSyZYTG0FrF5hUSZ48xjIc7fl+UYrx8HwrPC45Sj71J3T72a6nqYrPo4jCuMvdqKzVu9/wPy5+Jvw31X4PfEPVvC+vQ/Z9W0acwTqPuPz8siHujrhlPcMO4NVfC+k/2pq6BlzHD+8cHvjoPxOK/VD9uL9hXTf2sfD9vfWM9vo/jTSo/Ks76UHybmLJPkT7QTtySVYAlSe4JFfHf7RH7GF1+yZ4Y8JPNeJqU2tQSrqc8QPkpeq27ZHuAOzy2AG4AtsJ4zgfF8V5JisvoVa1KN4Lquif+R7GAzylXhGEnab0a9P8zysDFcF+05x8Dtd/3Iv/AEdHXfA15t+1le/YvgjfruKm5uIIAMfe+cMR+St+VfmOQpvMqCX8yPsuGYOWbYaK/nj+Z8lr0ooB5p1f1FZM/sC9woooo5QCiiijlAKKKKOUAoooo5QA1tfDHn4m+G/+wvaD/wAjJWI3St34Yf8AJTvDX/YXs/8A0elFrM58V/Bl6Mm+Mv8AyWLxb/2HL3/0oeudHSui+Mv/ACWLxb/2HL3/ANKHrnR0qTPB/wACHovyQUUUUHQFFFFABRRRQAHpR2oJxSe9AHpP7JOnrf8Axz09m4+x2884yuedhUf+hfpX1lLwq/zr5c/YyVT8Xbn1OmS4Hvvjr6hbjFfg/iJNyzXlfSKP5w8VKjlnSj0UF+o1o1nRo5FV0YEFT0IrJWebwrJtmZrjTiQEf+ODPY98e9bC8E0SxrNGysAytxgjgiviYVuT3ZK8e3+R+bBDMtxEskbK0bDIYHgivUPgv+yn4j+L/hTVPFEnl6B4J0OCW61DXL1D5QSJS0iwx5DTOMYwMLngsDxXWf8ABP79gCT4/wDin/hItYnu7TwHpdyVltR8p1eYD5oUbqIxkb2HP8I5yV+uP26JYta0j4e/BfQlgsW8eavDDc29sgRLXSrdlaXCrwF6cYxhGr9EyLgWNXCPMsY37N/BHZyb0V/K76bnyeacQKFdYXDP3ur7Lr80jh/2FP8Agn94T1b4SaX4u8daL/bOpa//AKdYWl3I6w2to3+q3xKwVndcOd2QNwAAxX1RofwX8HeGdRs7zTfCfhqwvLAFbW4t9MhjmtwRghHC7lz7Gui06yg02xhtoI1jt7WJYYUA4RFAVR+AAqQDNfvuT8O4LLsPCjQpxukruyu31d/U+BxWPrYmpKdST189A+YD5SfxNNbcFA3H1606TgD602vejHscnKgALck/dPFOLNjvz2zTaDz3NKVO4WRw/wAVP2a/Afxss5IvEnhfSr+RgQLpY/Juo891lTDj88e1fnp+1H/wT/1/4KfEJbXw/wCZrejasHk0jcQLiZkBd7X0edUBdQMGRVO0FlK1+oRXNcp8Z/hfD8YfhvqGgtO1leTBZ9OvY+JNOvIzuguEPZkcA57jI6Gvi+KuC8FmtBy5Eqi1TSs35Pvf8D1srzevg5+67xe6e3qfjIeCykMrqSrKwwVIOCCOxGOlNXmvtT42fs1x/tffA5Pid4Z02PTviJpZms/FWjW8YVb67tm8ufaoHE3y716eYrAH5iDXxZsIHT6g9vWv5tzvIa2V1VCfvQkrxktmv0a6q5+m5bmUMXTbWklo12f+XmBpAMil/A0KcGvF3PSPkv8Aau01dP8AjjqbrjF5Db3B+piVT/6DXnJHNeq/tjurfGJQq4ZdOgDH15Y15V3r+lOG5OWVUJPflR/WnCNRzybDSl/IvyPpr9iTVRdfDvVrU/es9RLDJz8rxp27cqfrXsTsFz823nr6V85fsSeJ47DxXrOkyPt/tC1W4iBPVomwf/HXJ/4DX1L4H8IzeP8AxppGhwbvO1m9hslKjJHmOFJ/AEn8DX4jxhgpRzupBL42rfM/nvxFw7w2d15yWkrS+TR+jP7BXwz/AOFafsyaAskXl32vK2r3Y95ceWD9IhGK9nRNtRWOnw6XZw21uix29siwxKv8KqMAfkBUw4Wv6VyvBxw2Ep0IbRil+B/P1Sp7Scpvq7i9KQHJrzj9rL9qjwj+xd8BNe+I3ji8e10LQYs+VFtNxfztkRW0KsQGlkYYAJAA3MSFUkfD3gb4a/tif8FR9Kt/Gvib4lXX7L/wu1lfO0Tw34Yic+ILy0bmOe4lyjqXXBBMi7hyIUBBPbs9TJy1sfpQzbRyCv1FL1FfnhrH/BKb9pH4FQ/258Hf2wviJrOv2wLjSPiBI2oaVqOOQjEmUJk8Z8o9eo616P8A8E/f+Cm+sfGv4ran8Efjd4UX4ZfH/wAOw+dJpg40/wASQAMxubFizZ+Rd5TcwKncjEK6pXMg5j7IoxSKcilqigpO/Az/AFpHdURmZlRVBLMxCqo6kkngDvk1+aOq/tCfG7/gsx8XPEfhv4F+LLz4P/s7+EbxtM1Px9bxsuseK7hceZHZFSCsfcbWQhGVpGy6xAuFz9Mvs8h/5Zv1/u1GBjsc9ea/PVf+DcL4RSWyzv8AE74/N4iADnWv+Eqi+0ecP+Wu37ORnPPXPv3rltf+K/7Q3/BFHXdOu/iZ4m1T9oT9m3ULuOxm8RSxM/iXwdvYKjzbmO+PJAAZ3V8bVaJiqsPTUm9tz9NMV4t+3x8NF+I37Mev+XHuvPD6jWbfAyR5AJkA9zEXHvXqXgLxzpHxP8GaT4j8P6la6xoOvWcWoadfWz74bu3lUPHIp9CpB55HQ4PFaGpadDqthPaTqr293G0MoPQowKkH6gmuDM8HHGYSphp7STRtRquE1NdGmfjaWyc5HPIrxz9tnVPsvw10u243XWph+vUJE/b6sPxr3jx34Ul8B+Ota0ObiXRdQuLFvfy5GQH8QAfxr5c/bc8RreeKtF0hG/48bZrmUD+FpGwuf+AoD+NfzZwjgZSzuFJr4G2/lp+Z/QHh1h/rWd0GtleXySPEV606mg5NOr+iIn9WBRRRVAFFFFABRRRQAUUUUAI3St34Yf8AJTvDX/YXs/8A0elYTdK3fhh/yU7w1/2F7P8A9HpUs58V/Bl6Mm+Mv/JYvFv/AGHL3/0oeudHSui+Mv8AyWLxb/2HL3/0oeudHSpM8H/Ah6L8kFFFFB0BRRRQAUUUUAB6U3dTjzRigD0j9kzUl0343aercfa4J4Bk9SULY/8AHa+sCOFr4h+GPiMeEPiLoupMfks7yN5DnGEJ2v8A+Ok19wSDDYHO3uK/E/ErDuGOp1uko/kz+ffFrCShmVLEdJRt80/+CRk13H7PHwS1D9ob4t6P4V05mi+3Sb7q427haWy4Msp9wvAHGWKjvXEhSDX6Mf8ABKH4BL4G+ENx43vbfGqeL222pcfNDZRswXH/AF0cM/uoSvF4N4f/ALYzOGHfwL3pPyXT5n4fn2ZLB4VzXxPRev8AwD6Y+H/gHS/hv4O0vw9otstrpelW6WtrCOyqMck8ljySx5JJJzXzz+z3P/w0D+258QPiMzGfQ/BcK+FdBYjMZfrPKh+u/n0lFeg/tv8Ax+/4Z2/Z71bVrWQDXdRX+z9IRT85uJARvAHPyLuf6qB1IrhPDPxD+G//AASq/Yv0O6+K3ivR/B9uqtNeS3Tl7i/v5AZHihiQGSeRVwmEBICAnAya/ovEQhXzKjgKS9yiueVtr6qC/N/JH5jTk4UZVZbz91efWT/JfefTQ+U4o3V+cx/4OXfhLqMrXeifCf8AaE8ReGUOX1+y8KxGxCDOX5nztwAecEgjgV9M/sTf8FPvgp/wUFspx8NfGNtfazZxma80K+iNlq1qo4ZjA/LqCQC8ZdAcAnNfUxqRbsmckqc4q8lZH0BJ0H1ptK7ZA+tJWq2EFBPFBOKx/H/xB0P4V+CdV8SeJdY03QPD+h2zXmoajfzrBbWcK/ed3bgD9SeBk0wNcNz0pD8vtX546v8A8HAUPxV17ULH9nn9nv4vfHq10yUwz61YWbafpbMDzscxyydAcb0Q+3erXgz/AIOCvCvg3xtp/hv9oL4Q/FL9nXU9TcRQX/iHT2udHkc/9N1RJAOD8wiKgDLFRWftI9x8ku39em57x4e1v/hnH9vHV9Eun8nwz8YoE1LTWPyxw6rCgSWMHpukHzepLIOa+df+CoH7LUfws8cx+OtEtvJ0LxROy38Ua/LaXxyxb2SUZOOgcN6ivsD9of4LaR+1z8F7Q6Pq9oLxRHrPhnX7KYSxxzYDRSxypndE/GSpPGCOQDXmHh34zWf7Rvw81v4J/FyOHwr8SWgNmn2lQkOqSLzDd2zH5WbeofaOvJXglR+Y8TZPTq4erleIVlJudGXRSerg30127p+R7eW4ydKrHE0t1ZTXVrv933WPzgyc0bd3tVnWdFu/DOtXmm6hC1vfadO9tcxMMNHIjFWH4EGsrxBr1v4W0G81K6YJb2EDTyE+ijOPx6D61/N0aU3V9klre3zvY/VsPTdaUY09eZpLzvsfJf7TWsLrXxv1xkO6O2aO2U54+SNQ3/j24VwZOKsapqcut6pcXk/zTXUrzSH1ZmLE/mar5xX9RZbhfq+Fp0P5Ul9x/ZWVYT6pgqWH/lil9y/zNLwf44n+Hnimw1izeP7Vp8nmBHbAlXoyH2YEjPbOa/ZD/gnj+yzqnijWfA/xYnudLbwrdacmtaesM5lnmkmgISNlwNpjZ2yf70fHWpf+CKPwJ8GwfsF6HrTaLpGp6p4qur241We6tIp3LpcywLESykhVjjUBfUseMmvtTwV4c03wVoFvpej6dYaTptnkQ2llbpbwwAksQqKAqgkk4A6k1OI4cwOMxdLF4iN507NfLv3P5O8VuN6eZYqeDoUeSVKUoOTd3KK0eltNb2d9jZA4p3am9BTq+pjofii2Pnf9uj/gn7p/7dfjL4STeIfElzZ+E/hr4k/4SPUfDf2Bbi28UOqqIopXLqYwuHXO1wUmkGASGGp+3J/wUM+Gv/BPX4dWuufELU7r7XqjPHpGi6dEJtR1dk27/KjJCrGm5d0jsqLkDJYqp9zP3q/nc/4LE6P4u/ay/a7/AGjfiBNrnh+38O/A/U9O8IRaVe6mIdQNvI5gj+xW5z5oM4mmkwwOJN2Dj5acb6k6X1PtS5/4OU9f0HTbLxVrX7KvxG0n4Z6kUW28RSajKsc+8/KySPZJbSbgDtVZuSMBj1r6Y+Hdp+z3/wAFgrj4Z/G7QLrUb7XvhLqwurCW3uG03VNKuMh/sV/GMs0RZA4UkqwDFGKu+74//aN/4Lq/BD4rf8EVv+FUw2OrXXxI1vwXbeEpfD39mulpo9zDDHF9sM7DyjCjRCWMIWckICq8lfln/g3M+M2s/Dj/AIKYaF4asriUaR8RdLv9N1a2BPlzfZrWa7t5COm6N42AJ6LNJ/eNXWhCLShK90Y4dynBuceWz+8/oeooBoqDoOY+NPwzh+NHwg8VeD7rUNQ0q28VaRdaRNeWLBbq1SeJomkjLAjcA2RkY4r561r4n/BT/gh1+xN4P8M67rV3a6HokL2Ok2qQLPrPiS6LNNPIkKbQWZ5CzudsaB0BYDbn6rYZFfzYf8F0vjrq/wAcP+Cm/wAR4dSmmbT/AANdJ4Z0e2Y/JaW8EUZk2jsZJ2lkJ77h2AqZWsEVeVj9FdF/4OShqWhSeLG/Zj+MA+GMMrJL4sgk861iUOEJZvs4twwY4K/acA8ZzX2r8Bv2jfhP/wAFK/2dNSvvDN9Y+MPBmvW8uka3pd5EY54BJGVktbqBsNGxVjjsRhlYjDV+d/7I3/Bdn4HfBj/gjLafCzxPYavqHj3w/wCGL3wqPDEWmO1vru9JUjm8/HkrE6yAyb2Dhg+Eb5d3if8AwQ7bVP2Nf+Cknw78GaZ488LeOtB+NHgtbjV08O3j3MFhN9mlureG5DAeXeQNC6sDnas7Djdit6tNR5XGV9NfI5adSblJTi1Z/eftv8E/gp4X/Z0+Fmi+CfBekR6D4X8OwfZtOsI5ZJlt49zMRvkZnYlmYksxJJOSa6tjxSt2pDyKyOpeR+f/APwUn/Zk1/wXr3ib4oaPp83iDSLtreSXS7BHm1GW7kaODy44lUlwzlWyOg3E4xz+b3x0/Yp+OWi+FdW+KHjL4e61o2hyyLLdTzPFvs1JCJvhDmVEHyjcygDqcZr+hbUWZYsqWUnjjivLP2vr/TdJ/ZN+J1xrKRyaXD4V1M3Ky/dZfssgx+JwB718rh+GcDg8XUxVCNpTu3d33d9Ox+ocC8eYnKKkKdGjGUpSUW3e/LdaJJ2T8+p/OOBj2pwORUdsCIYw3VVGakHSvQjuf27CTauwoooqiwooooAKKKKACiiigBG6Vu/DD/kp3hr/ALC9n/6PSsJulbvww/5Kd4a/7C9n/wCj0qWc+K/gy9GTfGX/AJLF4t/7Dl7/AOlD1zo6V0Xxl/5LF4t/7Dl7/wClD1zo6VJng/4EPRfkgooooOgKKKKACiiigAooooAQDcT78da+zfgP41Hj34VaTes2bmGP7Jc5+8JIvkJP+8MN9Gr4yU4avXf2RfiavhfxlNot1Jss9bA8pmPEdwoO3/voZX3IWvieOsneNy9zgvep6r06n5/4kZK8flTqQXv0nzL06/gfXHw38BXfxV+IWieGrHct1r17FZIyjJjDthn/AOArluf7tfs3Yafpfw38Hw2sXk6bouhWaxqZHCRWsESAZJPAAVepr80v+CaNjpOl/HjUPF2v3lnpui+B9Hn1Ca7u5AkMEkhWFMk9yGkwBySMDJPPtnjHxx44/wCCkuv/APCO+EYNQ8K/B+GYf2hrVxGYptZUEZVVz8ynnbGOM4aQ9FrzfDmtDLsuliYQc69Z2jFbtLTV9FfdvsfxLxIpYjFqk3ywgryfa/5u2yLHgLXD+3L+1RcfEG/juP8AhVHwpEh0xDA0g1K5jBcyBANzNwJCoBOEhXB3EV8+/wDBM/8AZn/4e0/FfVv2wPj7pLa5pl9qMtn8MfB+pqX03Q9Pt5douHhb5ZG8xGADgqzpJIdxMZT9Kvhd8MdG+DXgTT/Dvh+1Wz0zS4xHEmcu57u7cbnY5JbuT+A8n/4KU/tvaf8A8E7v2OfEfxHnsodQ1K18vTNB05xtivdSnysCPtIIjXDSPgg7I3A5Ir9cybKp4OjKpiHzVZvmm+77LyitEfMYjEOpPkoqy+FLrb/Nvc97gkXS7KOJGhtLeECNEGI441A4UDgAAcYHSviz/gpV/wAEhvDv7VNp/wALH+GbQ/DT9oLwy41Tw74s0b/Q2v7qPLJDd7MJIrnC+cQXTjO5N0bfmf8A8E1P2Ydf/wCDgn46fFDXPj38YPHckHg22sri30/Sr5IVWS7e4wLeCRJIbe3j8gjakYYmQZbOSeW1/wDaL+Kn/Bvp/wAFGvEXw78O+NtZ8d/DvQbm0nudA1O7aS11XTbiKKfAiLFLa9VGKiSMKCyqSpRtles5e4qjWj2ZlHDuNR04y95K7X/BP3B/4J6fFz4mfGv9knwxrXxi8F33gT4kJ5tjrNhdRpEbqWCQx/bEjU/u0nC+YF4AJO3KbWPtQ6Vg/C/4j6P8ZPhj4e8YeHrgXmheKtNttW0+fGDLbzxrJGSOx2sMjsQRW8OldEdjniIfvV+dn/BQD4F+Mv8Agpl/wUz8H/AfXdL8VaN+zr8PdGh8beKr5Laa2svF16ZCkVkl0oAbaGVNqtuX/SWwGSNh+iZOTXiH/BRj9tnSv+CfH7IviX4malbLql1p4Sz0fTGkMY1LUZsrBCW6qmQXcjkJG5HIFTNK12VHm5vd3PWvAXgTSfhn4M0/w/4d0nT9B8P6PCLaysLG3W3tbSMdFRFAVR/M88kmqHxh+C/hf48/DvUPCHjfw9pnibwzq8Ziu9O1GESQyrjhh3RgeVdSrKQCGBGa/AH9gjQ/iB/wX4/bo1fQfjV8YvHFjotjolzr7adod/8AZLeMLNDEtvZ2zb4IUUyqWYxu5CLkkksO9/aF+O/xP/4NyP8AgoHovgzQfiN4s+Knwl1zSLbxA/hzxPfm6eSzlnngljiz8sN0jWzskkQVGBUOpGRURqfu/acvu9zaWHaqexv71r2/4J+gP/BNn9ib4t/8E6P2mvHPw30+6XxL+yvqVmdb8I3d/qSNqPhi/eQGTT1iP7x0bdKxbAQ+WjjDvIrfWXxl+AHhD9oHQF0/xZotvqkUOTBNuaK4tie8cqkOv4HB7itr4cfEHSPi38OdC8VeH7pb/Q/Emnwapp9wBjzoJoxJG2Ox2sMjseK2FODSrYSlWpuFSKlF9HqjGNSfN7RO0u60Pyf/AG8P2f4v2dfjy+lWd/qmpafqljFqFvcahMJrh9xZGVnwNxDIeTzjGfWvir9sz4kDTtFtfDNrNi4vsXF7tP3IQcqh92I3fRfevvb/AILmfH3TPAvxW09Jljebw/oojVFB33E1wxkVGP8ACqqqsT2Bb1xX5FeJPEl34w1y61S9l866vpDLI/bJ6Af7IGAB2AFfguC4VpPiKviIx/dU5af4uy7pM/q/wX4fqZhGlmeM+GC0v1l0fyKW7H8NFCjilK1+nH9QcqP1G/4N4P2hfP0rxv8AC2+uF32jr4i0iNm5MbkRXSr9HET/APbQ+9fp7p8mJK/mx/Zz+PviD9l/41aD468MSxpq2hz7xHLnyryJvlkgkxzskXKnuCQRyBX7t/sZ/t+/D39tXwxb3HhnVIbXxFHEr6h4du5VTULJ8fNhP+WiZ6SJlSPQ8DohK7TP5E8Y+DMRgsynmtCDdGrq2vsy637X3v3bPoRW3ClzUcbE9cinL96vTUk0j8LA9a/ID/gsN/wTvt/hD+3Ha/tMXPwrvvjV8JdWjaXxx4VsLua1uLW7W0a3S4Lw5lEDt5MpdQQrwsHwrg1+wOKRSUO5flb1pkyjc/j0kv7e5nbySkYkc+XCJPMKAn5UyeWI4XOMnHrX7Hf8G6v/AAS78VfDnx9N8eviJot74d26dJY+ENMvoWhvJxOGW4vpI2w0SeXhIwwBcTSNgAKW/VO3+CXgnTvFP9uW/gvwfb64Tu/tKPRLZLzOc580R7855znrXTiTJJbLEnOTWfLZ6GknKSsODhfWlDZqPG9q+df2n/257z9nv9tf9n/4SW3h221SH4zXOpR3moy3TRtpcdrEjIY0AIdmd+d2AFHHJ40Jk0j6ObkV+HH/AAcQf8E0vFPg79oDWPj14U0i81rwb4wSGbxH9hhaWTQb6KFIWmlVckW8yRo3mfdSTeGwGQt+4wGRQV4PTDAqQR1B6ijcNndH8jfwR8a+E/A/xe8N614y8Ox+NvCOm38dxq+g/b2s/wC1rcZ3Q+cnzJnIOR1wAeCa/WX/AIN9P+CcOtWnx01n9onxH4UvvA/hllvYPAmhXrSNc7Lp2VpyZAJDFFATDG7gGbzGbooLfqFZ/spfCvT/ABQuuW/wv+G9vrSymddRi8L2CXYkPVxKIt24/wB7Ofeu/b52ySxY9ST1pR2swbbd/wBf0AtuNBOKOlNY849aYFXU3+6v418S/wDBdj40w/DX9h260CO4Eep+O9Tt9KhiB+drdD587Y/uhYlQ9syD6H3f9qz9u74X/slaLcXHjDxPZw6hChaHR7Rhcaldn+6kKnIyf4m2qO5GK/ED9un9tnxF+3P8Zn8SaxH/AGdo+no1pomkLJvTTrfdnk9Glc4LuOCQoHCgV51Z++2fr3hXwTjMzzOljKlNqhTkpOTVlJrVJX3u97bI8Xx/KnU0dadXOj+0loFFFFMAooooAKKKKACiiigBG6Vu/DD/AJKd4a/7C9n/AOj0rCbpW78MP+SneGv+wvZ/+j0qWc+K/gy9GTfGX/ksXi3/ALDl7/6UPXOjpXRfGX/ksXi3/sOXv/pQ9c6OlSZ4T+BD0X5IKKKKDoCiiigAooooAKKKKAE2819A/wDBKbwro/jT/got8JdN12CG602bWS7QTAGOaRIJXiUg9f3ipx3r5/rb+GPxE1T4Q/ErQPFmiTfZ9Y8NahBqVm5+75sTh1DeqnGGHcEiqp8vMuZXR43EWBq4zK8RhKDtOpCUU/NppH7+fsffs9+GfAvxu+N3hpbC31LR7HVdOFvBewrPHEhie4RMNnOxnwpPPyjnNfUsMKwwpGmFSMBUVRhUA6ADsK+Tf+CaX7Sui/tYfEL4meNdAEkdn4ng0jUZLeVSJLK4Fu8M8DZAyY5UZdw4OARkGvrZVyv/ANeu/hnDUKOE5aUUrSmtP8bP8386pV6OMlQxaaqRspJ7p2SdxNtfnh/wcu+DdRv/ANhjwX4wtdEtPEmmfDH4h6V4l1vSrtGa1vbJUngZJwvJhaSaKOTkfJI3tX6IYz90MSOozVDxj4I0r4jeDdT8P69pdprGh63aS2OoWF5CJre9gkUpJFIrcMrKSCD6179TWNjylUUZJ9j+XbxD8b/jV+xR8Y9P+Ofw9t7P4G2fxzsLvWPDlj4bmtL2wXSzOA1t5EiSIqJIFISSNSp5VVwVHzz8QPiJrvxT8Z6v4o8VaxqHiDxBrM7Xmoajeyma4u5COWZu/TAHQAAAYAFfsj+1F/wajJrvjKe++EHxOt9C0Ody0Og+Kbaa8XSlLbjHDcxHc0YJOFdNw7uxya9N/wCCev8AwbPeDv2Z/iTpfjn4p+Kbf4ma9ok6Xem6RbWBtdFtbhSGSWVZC0lyyMAyhtiAgEqxAx57o1HLlb0PWji6EY8y322/U+yf+CYPws1n4If8E7Pgv4U8RQyW+t6P4TslvYJARJbSOnmmJgeQyb9pz3U17uDmlfJ+9ySeT60i9K9KO2h5e+ofxfhX5uf8HS/w51fxv/wTd0XVdNhkms/Bvjix1fVQqk+VbPaXtmJD/srNcxDngbq/SM1k+PPAOi/FPwNq3hvxJpdnrnh/XrOWw1HT7tPMgvIJFKvG6+hB+o6jBqakeaNmVCXLJT7H8h/wM+PnjL9mL4p6b42+H3iK+8K+K9H3i11K0CO8aupR0ZJFaN0ZSQVdWU+mQCPbviz8avjZafBLX/it8TrHQPHWnftNWEmjWvirX0t77UrT+zbgeZ9jSNl+wshygURqhU5VQQK+8f2nf+DT3UrvxveX3wX+JOk2+hXDl4dF8VwTefYgkfIt3Creag5wWjDAAAljlj6h/wAE/f8Ag2T0H4B/EfSfGHxs8X2PxEvPD8wudL8NafaPHoiSghle4M2WuArDd5YVEJALbx8tcFOjWvyLY9apjMPK0lv2tr9/qfbP/BK74aax8G/+Cb3wX8NeILeSz1nTfCtp9qt5FKyWzSAyiNgeQyrIqkHkEEV72x3VIwYglt25uSTUTfh616sVZI8bmveR8u+HL5dN/wCCrvjCxvI1kt/EPhOBYUmRWSQxxQkjB6jCycezehr8p/8Aguh+zp4X/Zv/AG8bm08I2NtpOl+KdDt/EEun267YbS5kmuIpfLXOFRjEH2gAAswHFfqX+2frWn/Az9qTwb8SLy7t7BbHTBJKXlCNeRwXccE0KDgvK0F+xVAcs0QUAmvxQ/bz/asvP20/2rPFPxBuIZrSz1CUWek2kp+ezsIcrAjDsxGXYDjfI1fBYOUaf1nCz3VWUl6Ssz+jfAXA46rnixtFtUI0uWfZvZL8L+XzPIRQRkUL0orc/sQaeDToZpLa9huYZJILq3YPFNE5jkib1VlIKn3BBopCKCZRUouMldM/Wz/ggT+27qnxJtPE/wALfGGvX2sa1p4/trQrjULpp5pbT5I57cM53ERtscD0kbsBX6T96/mf/Z2+Ousfsz/HHwx480EsdQ8M3yXfk7yq3kXSWB8fwyRlkPpuB7V/R18G/i3ovx4+FXh7xl4euFutG8SWMd9bSDqAwyVYdnVsqw7MpFelh6vPGz3R/GnjPwfHKc3WOw0bUa+umymt18918+x04NL1pqnJp1dR+OEbj56KWQ4b8KQZI7UGnQCcGvk//gon+xz44+O37RH7NnxI+H/9ktq/wj8YG51eK+uvs3maTcNB9pKNg7mVYmXYOW83jpX1gOaMn/IoJlG45/vnH3c8fSnVHuI9Pyo8xqA5SSiow7EU5HO7FAco4muJ/aJ+M2n/ALO3wK8XeOtW2/YfC+lXF+UzhppEQmOMf7TvtUe7V2jnA/SvzL/4OIf2p10rwn4Y+D2l3X+k6xIuu68Iz0toyy20LezybpMekK9jU1JKEXJn0HCWQzznN6GXQ2lJc3lFayf3fiflb4n8TXnjjxfq3iDUpPO1XX72bUb2Y/elmmkaRzn/AHmP4VS6n8aVvlak7V4abvc/0KoYeFKnGlTVopJJeS/4AoGKKKK2NuVhRRRQHKwooooDlYUUUUBysKM0UYz/APqoDlYjdK3fhh/yU7w1/wBhez/9HpWCRit74Yf8lO8Nf9hez/8AR6VLOXFfwZejJvjL/wAli8W/9hy9/wDSh650dK6L4y/8li8W/wDYcvf/AEoeudHSpM8J/Ah6L8kFFFFB0BRRRQAUUUUAFFFFABQPv0U09aHsHkevfCr4m+KPBvwN1K88H+Itc8N+IPCeqrfJdaXePBMLadRGd20jcokAJDZHNf0FfsLftOWn7YP7KXgvx9bNGLrV7IR6jCuM2t7ExiuEIBOP3ikj1VlPev5uPhp44bwJ4ka4lg+26ddwvaX9pu2/aoHHzLnsRwQfUV+h/wDwb8ftfWXwg+PniH4MX2sG88NeNpf7Q8N3UqlAt/HH80W0j5WmhAyDgb4MDO8ZMhnPDYqrh5/BN80fJvdfefzh408EyxWX1cww9P36UvaXS1cGrTi3u+V2kr9L2PuT9sf9prxVoPxCuvC+iXEmi2dlFH508PE90XUNkPjKqM4BXkkH6V89XXjDWb67+0Taxqkk+d297uQsD165r79+LX7P/hr41xRnXNPJuoVKR3lu/l3Ea5zjcOo9jkV5TN/wTm8Pvcfu/EWuRx88GKJj+eK+mrUKknoz8m4Q4x4dwWBhRxNHlqJavl5uZ977/J7HjvwN/at8YeA/FWm2dxqV5rmj3VwkElncnz3+dgoMbH5gwz0yQcV93YIevLvhV+yF4P8AhLqkOoW9rc6pqkHMd1fOJGiPTKqAFU++Mj1r1JmwK2p05KNpHxPGecZbmOMVbLKPs421dkuZ97Lb9RX+6tQXV3FY2rzTSJDDEu53c7VUDqSalqnrOkw69pdxZ3Clre6QxyAHBIPpW0VY+Sja+uxYtrqO8t1lhkSaKRQyOjblceoPen54/wA8VW0bS4dE0yCzt1KwWyCOME5OB6mrD9KoUrX0PBf24fjf4g+F2naNpuhM1iNaExmv1XLx7NmI0zwGO4knqAOPb5D1bxBqOu3j3F9qF7eTSdXnneRj+JNfo9428C6R8RNDk03W9PttSsZDny5kzsP95T1U+4INeP6t/wAE+PBd5cbrO+16xQ/8sxOsqgegLAn8zXJiKNScrxZ+r8D8YZLlmE9hi6LU7u8lFSv69V6bHy14G+Mnij4a3kdxpGtX1sIuTDJKZIJPZkbgj8jX3/8ADfxfJ4x+G2i69qUK6Y1/YR3lzHIdq2+VDN16Dvz2rgfAf7EngPwRqUd5Ja3ms3EWGT+0JvMiRh38sAL+YNeXf8Fm/wBqBf2X/wBg/wAStZ3X2XxB42X/AIRrSNhw8bTqfOkUf7EAkIPY7fUU6cXRhebOXiPMMFxNmWHwWUUOWc5KPNazd/Jdt22fj5+3N+1v4i/as8S+MvEusa7qGoaLrHihl8IWM037nT9Pt2lAeGPomR5WXAyzFsnjj5xlmkuZmkkZpJJGLuzHLOx5JJ9c01DiKNf+eY2qM/dHoPz/AJ0Hivj4U/3k6st5O5/cvD2R0MqwscLh0lFdlbZJffpv8xw6UUDpRWx7wUN0oooAaDX6Hf8ABCv/AIKAJ8H/ABx/wp3xVerD4Z8VXTT6BczOFi07UGBLQMxOFSfA2/8ATXAxmSvzzpquUKsrNGykMrKxVlI5BBHQj17VdObhLmR83xZw1hs+yypl2J2lrF/yyWzX690f1KK3PP6U7dXwr/wSA/4KfW/7U/gy3+H3jbUI4/iVoNvthnlbB8SWyADzgennpkB16t98DBbb90Dr6jrmvXjNSV0fwTnmSYvKMbPAY2PLOP3NdGu6fQbIcN+FUfEHinTvCGktfapfW+n2cbKhmnfaoJOAM++avS/e/Csrxf4N03x94duNK1a1W6s7kfMpOGBHRgeoYHkEVR5sORte0vy9bb28jVhkjuIlkVlZZFDKwOQwPIIP0p21fSvE7L4U/Ej4Q/ufCGuWevaLGf3Wn6oArwL/AHQ3HQehUf7NWh8VPi1bN5c3w3s5ZO7xX6bT9PnoPVllPNrhq0JLzkov5qVrfiewlVI6Vz2r/EzQdB8Wafod1qEK6tqjbbe2TLueMjdj7ue2cZrzl/8AhcnxDDRNHovgq1kyryRyC4nC+2C3P0xXUfCn9nzSfhneyajJJNrOu3WTNqV580pyMHYDnaD9Se2aZFTB4ehBvEVVKXSMHfXzlsl6Ntne8Z4pVPz0MuxaE5bpSPLvaJg/FP4oaL8F/h3rXizxHdpY6H4ftHvbyZjjCKM4HqzHCgdywHU1/OH+0n8e9Y/ai+OfiXx9rnyX/iS688QA5WzhACwwD2jjVF44JBPevuT/AILs/t/w/EzxY3wX8I34m0Pw7cibxNcwyZS8voydlp7rCcM3YybR1jIr84yNp4rzsXUUvdR/WXgnwXLAYOWcYuNqlZWjdaqH6cz19EhzHDmg9KGOT+NB6VxJWP3+IUUUVsMKKKKACiiigAooooAKKKKAEb+lbvww/wCSneGv+wvZ/wDo9Kwm/pW78MP+SneGv+wvZ/8Ao9KlnLjP4U/Rk3xl/wCSxeLf+w5e/wDpQ9c6OldF8Zf+SxeLf+w5e/8ApQ9c6OlSY4P+BD0X5IKKKKDoCiiigAooooAKKKKACiiigBpGDV7wx4i1Dwf4j0/WNJvJtP1bSbqK9srqI7ZLaeJw8cgPqrKDVJhk0hBxTi7O6FUpQqU3TqK8ZJpruno0f0mf8E4P22NN/bv/AGZNJ8XReTb+ILILp3iKxQj/AEO/RB5hAyT5UmRIhP8AC+OoNe8gV/OJ/wAEzf2+9S/4J/8A7Q0GvMt1feDNcVbLxNpsPLT24JKTxj/ntCxLKP4gXX+LI/om8GeNNJ+IfhLTde0PUbXVtG1e3S6s722ffFcxOMq6n0I/GvpcHiVWhfr1P8/PFLw/qcNZs400/q9S7pvt3j6x/FWfprHimsuFb6UL17fhQ4yprqPzWMUtiPqc/wB2lBGfevIfjTL8Qvh146j8WeHRN4m8OmAQ3+gZ2tFj/lrHgEknqcAnjGCDkN8N/tv/AA/1iALfX19oF0vD2uoWjq8Zx0JUEfyPtVcyW56sMnxVWlGth17RPfl1cX2a3X3Wfc9fYc0Dk/Nx3ryHxL+3B8O9Ct2+yarNrVzj5LextndnPYZYBR+dYOjWPxA/aa8XabqmrQ3vgfwXpk6XdvZrK0d7fspypfGCAfcAAZAVidwOddDWOR4qMPa4peygus9G9NFFbtt6aK3c99KUECnk9eAPoOlMOTn3ppni37iDrtH3mPSvwO/4Lkftnx/tTftgXWgaLeG48I/DMS6LaFG3RXV7v/0u4XHBG5ViB7iHI+9X6N/8Fpv+CjEX7G3wKfwn4avkHxJ8dW8ttZeU583RrMjbNetgHa2DsizjLksOI2r8E4k8sfxHvycn868jNMVZKjH1Z/UH0feB5zrPiPGRtFXjSv1f2p+iXup97jyOaTH1p2eaK8M/rS1wHFGaKKA5QzRmiigOUM03H1p1GKA5S54Z8S6h4L8SWOsaRfXel6tpU6XNneWr7JraVDlXRuxB/wA81+z3/BMn/gsNoX7Udlp/gr4hXVr4f+JSqsMFwxWCx8SYHDREthLggZaI4BJymeVX8UyKNvI6qykEEHBUg5BB7EHkHqK0p1XBnwfHHAOA4kw3s6/uVY/DNbr17ruvusf1IsCW/Q0gG7tX4wfsQ/8ABdPxt8A7ay8OfEm1ufiD4WhxFHfCUJrNhHwPvt8tyoGflkKv/tnAFfqT+zh+3X8KP2sdOjl8E+MtL1C8ZAz6ZO5tNRh9Q1vJtk7HlQVOOCeK9OnWjJXR/HnFHAec8P1GsXScqfScdYv59H5Ox60i/L9KOvWlzg0u6td9T4zfUaeR3/OlxQTxXP8AxM+KPh/4NeBtQ8SeKNYstD0PS4xJdXt2+yKEZAGT1ySQAACSSAKWiKjByajBXb2XU3WbaufSvgv/AIK4f8FXbP8AZw0PUPhv8PdQjuviNqERg1C+hcMnhiF15OQf+PplbKLj5Adx/hDeI/t5f8F8brxdbXnhX4IQ3ml2UwMVx4svIfLuZV5BFpA4zFkYxLJ84zwqnDV+ad9PJeXUtxcTTXFxcO0ks0zmSSVyclmY8sxJJJPJJrkrYqK9yD1P6C8OfB3EV6scyz6HLTjZxpveT6cy6Ly3fWwksjTSM7u8kkjF3dyWZ2JyST3JJySepNNC4pM4pe9eef1NGKilFdA/xpT0pvWndRQdEWFFJ096Mn0q+ZFC0U3d7UbvancB1FN3e1G72ouA6im7vajd7UXAdRTd3tRu9qLgK39K3fhh/wAlO8Nf9hez/wDR6Vgk5re+GH/JTvDX/YXs/wD0elSzlxn8KXoyb4y/8li8W/8AYcvf/Sh650dK6L4y/wDJYvFv/Ycvf/Sh650dKkxwf8CHovyQUUUUHQFFFFABRRRQAUUUUAFFFFAB3oPSjvRQXHYaa+4P+CTX/BXK+/Yi1iLwV4zN1q3wp1GcyKEUy3HhqZ2y00K5+aEkszxDJByyZOQ/xAx5pQ4OOK0o1XTlzRPA4m4ZwGfYGWX5hHmjLr1i+kk+jX/Dn9U3w5+JOg/FzwXp/iTwxq1jr2g6tCJ7O/spRLBcIe4YdweCDyCCCAa3BJuHHNfzP/si/t7/ABQ/Yg8Rve+A/ED2+n3UnmX2i3i/aNM1A4xl4iRtf/bjKvwMkgYr9UP2Xv8Ag4o+FfxNsrOy+JWl6l8NtccBZbgB9Q0qRzgcSInmRg9fnTA5yx6172HzClU0ej8z+L+MPBbPcnqSqYSDxFHo4q8kv70d/mrr0P0Pxg5HB69KyNd8A6L4p51LSNL1Bj3uLVJDn/gQNZPwj+PPgn496G2peCfFnh7xXYxBTLLpd9HceTuztDhSShODwwB4PpXXDGK7rpo/I5KrQqOMrxktHumvXZmLonw60DwywbTdD0jTmXo1vZxxt+YANbS8e/vS5wajlnCtt9xRvoRUnKfvTbfrqOMiqetfOf8AwUT/AOCkPg7/AIJ9fDVrzUHg1jxpqkZ/sLw8kg867bkCaXvHbqw+Zz1+6uWIFfKn/BQD/g4K034ZahrPg34Q6Hdah4n0+eWxvNb1y0e2ttLmRiriO2kAkmdSGGXCIDz84r8jfiV8SvEXxl8eaj4o8Wa1qHiHxDq0hlu7+8k3ySk54HZVGThVAVRwABxXBiswhTXLT1l+CP3jw58Ecbm04Y7Ok6WH0fL9qa3X+GL7vVrbuanxu+N/ib9o74qa1418Zao+reItem825mPyogAwkca/wRouFVR0AHUkmuRA+agHHalzzXzzu3dn9m4PB0sLRjh8PFRhFJJLZJbJB3paTOTS0HZEKKKM0FBRmkPNIqlnCqCWbgADJNG+hnUqRhFyk7JdXp+Y7NG7muk0T4O+ItcCsmntbwtgiS5cQjH0PzfkK9U/Zn/Ysm+Knxy8O6L4gvrePRbuZ3vRayt5zxxxPIUU4GCxQLkcgEntXoSyjGqg8T7N8iV27dD89zLxa4SwNT6vWx1P2n8qkpNvtpdX+Z4KZPmx39DQzYHPFfsxcfsZfCW58KnRD8O/Cv8AZrIY8LZKs4HqJ/8AXbh2bfnPet79kz/gk18DfCPhqPVNQ8ExeJNUjvJlS41m7lvY2jVyYyIWbyh8pAIKnlc968LA46niZOK0PjZePGVKnKToT5ui019XfT8T8ffgf+zn47/aT15dP8CeFNZ8TTbwsk1pAfstt05lmOI4wP8AaYV+rH/BNH/gjna/su+KbD4hfEO6sdc8d2SltMsrQ77HQWdSrSByAZZ9pxuwFTnbuPzD7n0LR7LwvpcOn6XZWmm6fbqFitbWFYYYwOMBVAA4q2F3L8o716ei2PynjDxazTOqMsHSiqNGW6TvJrs3pp5JIet1IB95qX7fJ/s/lUJO00b6vmla1z8k5F2JheyEc/pWb4p8OWHjjw/faTrFlbalpmpwPbXdrcRiSG4icYZGU8EEdRVwNTs0rvuON4vmjoz8s/2pv+DfLUP7eutV+DviLTzp88hlGg69M8bWeTnZDcqrbkHQLIu4Dq7V8pfGL/gln8ePgj4bvNY1jwHc3Gkaehlub3Tb2C8jhjHV2Ctv2judvAHNfvwDhulcd+0P4fj8UfALx1p8nyrd+HdQj5G4A/ZpCDj2IBrSjRhUqxjLq0vxP1HL/GjiPLsPyScayitOda6f3k0/m7s/Ev8AZH/4JpeIP2lPDV3r2rat/wAIfo0U7WtsZLFp7q9dQNzKhZQsa5A3EnJBAGBmuN/a9/Ym8SfsiatZNqFxb65oOrMyWWq20TRqZAMmKVGzskxyBkhgCQTggfpz+ynGkf7OnhHZ/wAtLESNxj5mZi36k/Wq37X3w9sviX+zt4ksr6ygvlsbc6nDHKm7bJBlwwHrtDj3DEdK+f8ArE/7VeD5vd5+W79bXPTy/wAds3VX+0sZBSouN3Tilpp9mT1v5tn405+aj8K+jZfhd4buoNraLY4I42qUI/75Irntc/Zz0e9Rjp89xp0nUKT50efoTux9DX6XiuA8fSjzU3Gfpdfme7kv0sOFMVUVLGUqtC/VpSS9eVt/cmeJk+woz7Vu+Nfh3qngS5VbyJWhkJEVxF80cntnsfY/rWEQcV8diMPVoTdKtHlkujP6PyXOsDm2FjjsuqxqU5bOLuv+A/J2Y6iiisT1AooooAKKKKACiiigBG6Vu/DD/kp3hr/sL2f/AKPSsJulbvww/wCSneGv+wvZ/wDo9KF/X4HPiv4MvRk3xl/5LF4t/wCw5e/+lD1zo6V0Xxl/5LF4s/7Dl7/6UPXOjpQZ4T+BD0X5IKKKKDoCiiigAooooAKKKKACiig0AHeg9Kb1pHZUxuIXccD3PpQXsrsXPNAPPv2r61/Y0/Yp8O+PvhvD4o8Y2d3ftqMjizsWlkto4olYrvfaVZixDEc7duODmvZ/B/8AwSv+F/xn+Lug6Y11r3hGwvpjBMmmziYTMRlQPP37MkYyPUcZ5r85/wCIm5Qs4/sVt8/Ny81vd5trX/DsfJZjxpl+C9pKtzcsE22lfbst2fnCWEYyx2j3r2D9lf8AYX+KX7ZOuRWvgPwpe32nlwtxrN2pttKs1yAS87DDY67Yw78fdr9rPgV/wRt/Z7+A1zFeW/gW38S6tCRi+8Q3MmosCO4ic+Sp7/LGK+nrOzh02yitrWCG2toBtihhQRxxj0AHAFfq0MvWjmz8K4i+kNBwdLJMO7/z1Laeait/m/keDf8ABOz9gzQf2AfgYnhzT7hdW8QapKL3X9XMQjOoXGNoCDqsMYyqKScAkkksa9/+0Y/ib86Mc5oK5NetGNopRP5ozDHV8diZ4vFy5qk3dt9W/wCvlsL5rN/E350yTLdWJx0yacBijFVqcnKj4T/4Kgf8EZtM/bJ1qTxx4FvrHwz8RmjCXaXK7dO18LgKZiqlo5lAwJQDkYDKcKV/LP4m/wDBML9oL4RahNBq/wAJ/FlykRI+0aTbjU4HAONwaAscHqAQDjsK/o3KAikVivI4+hrjq4SnN8z38j9X4T8Yc9yLDxwceWtSjsp3vFdlJO9vLU/ll1PwZrWieJotFvtH1ax1q4lEMVhdWrwXMkjEAKEcA5JIHTvXrOs/8E+vidonhw6j/ZdhdSKnmPZW14JLtB3G3ADMO4VifTNfuD+3ppsMngQXU1rb3ckbRCKSWMM0DeYMupIyp9xg18jpI28Hv29q/nvxR8Q8dw7mlLA4KMWnFSfMr3TbVl223P6A4f8AFDF5tg44qNKNN3s1e6f4I/IsjaxBDKykgg9QR1B9/wDCnAc19vfHL/gmL4i+OfxE1bxN4J1jwfbNqUqs+jX961pcvPsG90OxlIcjPUck18y/G79kf4lfs5SN/wAJj4P1jSbXdhb/AMrzrF/pPHlM+xIPtX6rkeYf2nl1HMacbRqRT9L7r7z9My3ivLcVJUVWjGp1i3Z3672v8rnndNY80B9w47jIor0z6W6Lnh7QbrxTrENjZruuJzxn7qL3Zv8AZA5Ne+eBPhbpfgWBWhjW4vsDddyqC+e+3+6Pp+Oa539nPwvHZeHJtVdc3F+5jQn+GNSOn1Yc/wC6K9GI5r9c4PyGlRoRxdeN5y2v0Xkf50fSN8XsfmGb1eHcuquGHovllyu3PLre3RPS3rcMAGuz/Z51z/hH/jt4Sut2zbqcUTH2kzGR/wCPmuNxzWj4OvjpnjDSLpX8tre+gl35+7tkU5/Svr8zpqphKlNrRxa/A/l3A1OTE06i6ST/ABP0mK7ePSvUv2e/EMH2K+0pp0+1xt9qSEt8xiOFZgPQNgH0LL615fcKFmk2/Ku44H414j+178Rde+Cms+BPGnhq8Wx1bSb26t1YjckySRIWikXo0bBCCp9cjBAI/lThfBvE5nDDJ2crr520R/R2bY5YTBvFNXUbfdfc/QVBvp6LtWvC/wBlX9vjwd+01ZwWLzQeG/FuMS6PdzAee3rbSHAlBz93747jHNe5MSjEHgjr7e1fW4rBVsNUdKvFp+Zng8fQxVNVaEuZPsDfeNFNPJ5pQMVkdgtFBPFN/h680BsHevDf+Chvxyt/gp+zJra+cF1bxZC+i6bGp+YmVSs0g9kiLEn1ZR3rrP2g/wBqrwT+zRo5m8Taov8AaEibrfSLVll1C69MR5yi9t77VHqelfl3+07+0jr37U/xLk8Qa15drbwp5Gm6dE26LToOuwHALMx+ZnPJJ7AAD63hjIa2LxEcRNWpxd797dD4jiziWjhcNLD0pJ1JJqy6H1B+x1qS6l+zn4dAbd9lE1qePu7JnwPyIruPHlot/wCAtet5AStxpl1E2DgkNC4P6GvJf2AtV+2/BW+tSc/YNYmUc/wvHE/TtyT+VeyeI+fDepf9ec3/AKAa/M8+ouhxBVgulS/3tM9/Javt8lpyfWH6NH5q24/cx567RT16UkYxGv0FKBkV/VlKXur0P55la7SK+qaXb61YSWt3ClxbzDDxuOv+BHrXzv8AEXwdJ4E8SS2LM0kEiia3kPV4z0z7ggg/TNfSHQ157+0ZoSX3g23vtq+dp9wF3d9jggj/AL6CnFfJcZZTDE4N4hL34a/Lqf0R9G7xAxeS8S0spqTf1fEvlceik/hkuzurPyZ4tRTcZWnL0r8XP9NgooooAKKKKACiiigBG6Vu/DD/AJKd4a/7C9n/AOj0rCbpW78MP+SneGv+wvZ/+j0oX9fgc+K/gy9GTfGX/ksXi3/sOXv/AKUPXOjpXRfGX/ksXiz/ALDl7/6UPXOjpQZ4P+BD0X5IKKKKDoCiiigAooooAKKKKACkYZo3V3f7MvwD1L9p/wCO/h3wPpbPBJrNzi6uQu4WVqg3TzH/AHUBxngsVHeqjFydkc+MxdLC0J4ms7Rim2/JHefsQ/8ABP7xb+2x4jmfT5I9D8I6bL5Wpa5cR7kR+D5MKZHmzYwcZCrkZYHAP6NeA/2EPhv+zjrGleGfBXhyx1DxZdJ5l14j1mNL29tY8nLoWG2Inn/VqvGOpOa+kfhX8MdF+DPw90fwt4dso7DRtDtltbWJBg4Ucsx7uxyzMeWJJPNcpfXq+D/2jVuL5lhs9asRBbzP8qiQBRtz9R/48PWvWpYeMNOvc/l3OeO8dnWLqOLcaUVJxguttnL+bvbbTY8m+Mfgf/hAfGEdl9quLzzLVJmlmPzOSWB47DisLwzrLeHPE2l6ijbW0+7huVI6gpIrV6T+1laLF4u0m4GcyWhQk+ivn/2avJJScNX8H+I1N4LjDEyp9KikvwkeplVR4vLIqrq5Jp/ivyP04s7uO+tY5oWV45VDqc9QRUoNeRfBn4jTWvg3SpJd01tcWkUhX+JSUHTt+FeoaTr1rrkG+3lVx3H8S+xFf3hgayrYenWX2kn96R/M2Py2rhqrhJXSdrl6ik6etFd8djg5RaKbu96XPuaYcoppoORUV9fQ6dA0lxIsMa9WdgoFcV4r+JS30D2unhgrjDTEckd9o/qaTOjD4OrWlaC+fQ89/a+u/wDhJfAmuRw/vI7ONWUjvtZWY/ofyr5BUfMK+wvHmnLqngnV7fAbzrOZen+wcfrXx7E27afUV/In0iMLy5phsQvtQa+5/wDBP37gGKp4SdBfZa/Ff8A7n4W+H9L1rwb4smv4Va40+3Se3m6PEcPwPqQv517d8LD/AMJh8I9PXWIYb5Ly28udLhBKlyvT5lbIbIAPPXNfP3w40y+8VX8ug2KlV1Z4vtLgZ2RRsWJPoOc+5AHevqbSdKi0XTLezt12wWkaxRj0VQAP5V+yeDOIdXhPDp/Zcl90n/mcPFX7up7Nu7bUl5Ky+67/ACPzx/4KI/8ABHnTZNC1Lxv8INPXT7yzRrm/8MQL/o90gGWa0H/LNxgnyuVb+HaeD+aDIVB/UEc1/SG6Njr8ynIr8d/+Cx37LsPwE/aQj8SaTbeRoHxCjl1DagxHBfq3+kqPQPvSQD1d/QV+gYrDpJzR+oeFXHGIr1lk2PnzNr3JPfT7LfXTZ+pyfwwtVsvh3osafd+yq3Tuck/zrcxmsH4XXX2v4eaO4OdtuI/++SV/pXQHkV+8ZW08LTa25V+R/m7xupriDGqp8Xtal/XmYGmrJ5EiueQpDH8DTj0qOc/uX/3TXZWSdOSfZnztGVpJ+Z+nAm+0QxyLwsgDD6EZrw/9vuy+1/BexuM/8eerRH/vpJF/qK9m0WbztEs2zndbxkf98CvMP22rL7X+zhrDKm429zaS8H7o+0ICfyY1/KfDNT2efUZLpUt+Nj+iM8gquT1U/wCT9D4mTdHKrqzLJGwdGBwyMOhBHII619JfAH/gp/8AEH4QRW+n69s8c6JCQoXUJmXUIlHZLnkt9JFf6ivmtzg/rTQfev6mxuW4bFQ9niIKS8/8z+fMDmeKwkufDzcX/XQ/Uv4af8FP/hJ4/gjXUNYuvCN4w+aDV7ZhGD7TRB4/XqV6dK9G/wCGsPhf9k88fEPwa0I/i/tWL+Wc1+NwYjkE/nSGPLbtq7vXFfI1uA8JKV4Tkl8mfZ4fxCxsIqNSEZPvt+p+qPxJ/wCCnPwf+HkLLBr9x4ovF+7baPZvKGPp5rhYx/30a+Wfjj/wVi8dfEWCay8I2tv4H02QEfaInNxqTD/rqcLGf9xcj+8a+VTQvFd+B4TwGGkp25mv5n+h5uYcZ5jio8ilyx7JfruWdV1O61rUpry+uLm9vLhy81zcStNNMx/iZ2JZieeST1qDJzR1pD1r6flSVkfJSu3eTPqH/gnVes2ieLLXI2pcWso9eUkB/wDQR+dfQXi1zF4R1hl4K2Fxg+n7pq+c/wDgnSjF/GD/AMOLRfx/emvoL4kXJs/ht4kmX70Oj3kgPoRbyH+lfzNxpTX+s00uso/ikfv3Cs7cPxb6Rl+p+ccP+pT6CpF6UyI5hX/dFOWv6Xpqysfgzd22KRmuX+McSzfDHVtw3bY1cexDrXUE4rm/i3CZ/hjrSj+G23H6BgTXHm0b4Kqv7r/I+u4Bm4cS4CSdv31P/wBKR865py9KaQQOeM04dK/nfpqf7JdAooooAKKKKACiiigBG6Vu/DD/AJKd4a/7C9n/AOj0rCbpW78MP+SneGv+wvZ/+j0oX9fgc+K/gy9GTfGX/ksXi3/sOXv/AKUPXOjpXRfGX/ksXi3/ALDl7/6UPXOjpQZ4P+BD0X5IKKKKDoCiiigAooooAKKKKAGgfNX6Kf8ABAX4SQ6hr3xB8dTw/vLCO20K0cjOzzMzT4+oEIr866/XD/gg9psNn+yBrV0qkS3vie5Mpz97ZDCo/QCunCK9T5H5t4sYqdLh6pCD+OUYv0bu/wAj7XVAo4FY3jjwDpvxA0r7JqEbFVO6ORDtkib1U/06GtrctLuAr1db3P5SpynTkp03Zrqj5s+OngC78DLpcc+tXmrW7GSO3W4XDW4G04zk5z+HSvOz1r3P9r5MaNocn/TxIn5oP8K8LJyc1/C3jPT5OLMQ11UX/wCSo/XOG8ROtgIznvd/n5H1F8Adfi1v4XaXsZmazT7LJ7MnH8sH8a7mC4ktZlkikaKRejIcNXzn8DfGTfDe4t5r6XboeuO0YlIOyCdDjk9BwR+BHoa9N+JHxetdI0uO10W5g1LWdQIitYrdhLtJ6MccYHv1+ma/sng2s62R4Sq+tOH5I/P82yif1uVOkuaMm7Pou935dT1/SfiZfWEapcxrdKv8Wdrn056VtW/xT09x++W4iPc7N36ivn34f/E7UNP1pfD/AIsAt9VYZt7kgLHdD0yON3bjAPTr19Cds4r6iO2p8tjuHaVOfLNavVNPRruj0K4+Kmmwp+7+0TH0CY/nWPqvxTurhCtlAsH+0/zN/h/OuT71558cPE979u0TQdFvJ7PVNQuVcvE2GWMZHPfGeT7LRzE5fw7Rq1VTX3vZJK92ekalfXWqTeZdSvM/T5j0/CoenH55rzKw+L+tfDzUI9P8ZWf7pvlh1OAbopAO7D/AZ9u9d7pvjTSNXtRPb6lYyxkZ3CdePrzS1PXq5XUo7K8ejjrF/cW9ReMafcNIyrGsbF2J6LjnNfGeFEh2nK7iFPqO1fQXxR8ff8Jy/wDwi3hyRbqa8/4/ruI7oreEY3fMODnvj6deK8AmRY5JFU5VWIU+oz1/Gv5g+kZy8uCa3vP/ANtPveD8LKjGbqaOVtPLu/V7eR6n+yZz431Tjj7EBn/gYr3yvCf2SUDeItab+7bxjP1Y/wCFe7Gv0TwRp8vClF95T/8ASj5rij/kZTt5fkORsV8af8FzfAcPib9jKDWtq/aPDGv2k8b4GQs263YD6mRTj/Z9q+yxwK+Y/wDgsPcw23/BPnxos+395Pp6RZ/56G9hxj8Aa/Va1nTkn2K4Uqyp5zhZQ39pH8XZ/gfmv8DpPN+GOm9SVMqn8JGrrM1x/wAA3B+GlmP+msw/8iNXYdz9a/Y8ilfL6L/ur8j+T/FKmqfF+YwXStP/ANKYUyf/AFL/AO6f5U+muMg/QivUkrpo+Fjuj9IPA0jS+CdHdmLO1hbkk9z5S1yf7VNsb79njxZHz8tmsvH+xKjf+y10Hwluft3wo8MzNy0mlWrE+uYlqz8QvDreLvAWuaXH/rNRsJ7dBnqzIQv64r+Q8PW+r5uqj0Uan5SP6Xq0/bZY4R+1DT5xPzkPJ9sUg5FK8LWsjRyK0c0ZKSIwwUYHBBHbB4pv4V/X8JqS5ovRn8ySi4vlY4dKKbjijNWPZajqKbuozg1PKgv2HUh5pDx+daHhTwrqXjrWYdP0ayn1K8mYKqQru254yx6Kvqx4A5rGtWp0oudSSSXVmlOnOpJQgm2+x9Sf8E9fD7Wnw61zU2UqNQ1EQRnPVYoxn/x6Qj8K9f8AjBL5Hwg8XMeP+JJff+k8gqD4LfDn/hU3wu0fQWZZJrOHdcyIfledzvkI9txwD6AU347Hb8D/ABjj/oC3f/opq/lbM8yjj+IXiKe0qit6JpI/onL8C8JkioPdQd/Vq7Pz0hG23UeiinjpSDp+ApR0r+q4n88u1tAPWsfx/bfa/Aesxld26xm+X1whP9K2DVXWrf7Zo17F93zbeRM+mVIrHGR5qE0+z/I9bh6v7HNcNW/lqQf3SR8uvylA6UgP7sUo4r+b3uf7S05Xgn5BRRRQWFFFFABRRRQAjdK3fhh/yU7w1/2F7P8A9HpWE3St34Yf8lO8Nf8AYXs//R6UL+vwOfFfwZejJvjL/wAli8W/9hy9/wDSh650dK6L4y/8li8W/wDYcvf/AEoeudHSgzwf8CHovyQUUUUHQFFFFABRRRQAUUUUANPGK/YD/ghpZG1/YieY5xdeI7+QfQeWn81Nfj+Wya/Yv/giMw/4YM01R95Nb1IN9TcEj9CK7MBG9S/l/kfk/jFJrJIrp7SP5SPrsDdSlTikRsU7dXr8p/MZ4/8AtfRlvDWin+7eP/6LrwcnivoD9rVN3grTW/iW/wAD8UavADX8PeOcOXiup5wh+R+pcI3/ALPXqz3v9nHTLLxf8K7zTb62huoYr51aNxkAkKw+nXqK7jwr8LND8FXEk2m6fDBNIMGRmMjj2BbOPwrxX9nv4qWngDULyz1KTyLG+IkE38MUg4+b2Ixz2wK+gtP1+01eBZra6tp42GVaOQMCPwr+jPCfiLC43h3D0I1F7SnHllG6urOy031Wp8bxBRxNDFVIJtQk76Xs/wBDJ8f/AA5sviHof2S7XZJH89vOn34W9R/UVxun+MvF/wAMl+y65pM2v2cRIjvbTmTA/vDHP44P1r1JpeM5A981Xk1S1ibDXNqnc7pVWv014qlH4ppfNHlYbGSjD2NSPNDs76ejWx53N8b9U16Iw6H4V1SS6Y7Q90uyOPPc9v1FaXwz+Fd1pGrXGva9cLfa9egru/gtk4+VffjH04HeuuuPE2nwKfN1CxVfUzpj+dU5/iP4fs03S63paKvHN0n+Nc1XNsHD460F/wBvL/M1qYmXI6eHpcie71bfld9DSvtLt9StWguIYp4ZBho5FDK31Brj7/8AZ58J30+7+zWi3HJSOd1T8s4H4VbvPjt4Rs927XLJtpwRGTIf0BrI1P8Aaa8K2YYxTX10y9BFbnn6FsV4+I40yLDJ+3xdNW/vL9LjwdDMIfwFNX7XSNLxLpWk/Cj4bavcabax2gjtnCkHLSORtXJPJ5I6+tfLZm3RquB+VegfGP46TfEu3WxtrZrPS4237XbMkzDpuxwAOoHPP0rz3pX8k+MXGmFz7M6ccDLmpUo2T7tu7a8j9D4Zy6th6MquJvzzd9dXbpc9h/ZB+bWNePpBD/6E1e6dhXhH7IkuzX9cj7vbxfox/wAa92PA+lf0R4Ju/CVD1n/6Uz4fibTMp38vyQ6vkT/gt1qDWv7Cd3EG+W81/T4mHqA7SfzQflX1zuNfEf8AwXk17+zP2TfDtjuXdqfieEbe7COCZ8j6HH51+pYj+FL0Ojg2n7TPMJFfzx/B3Pg/9nlt/wAOUz/BdSgfjg12xOa8/wD2bbjzfBN0n/PO9bA9Mopr0A8V+vcNz5stov8Auo/l3xooOjxxmUX/AM/W/vs/1CjvRQOW/GvcPzE/QL9ny7+3fAzwjMTuLaVADz6Lt/pXYHmvNf2QtRTUP2c/Dez/AJdkmtznuVnk5/IivSia/jvPqfs8yr0pdJy/M/pzJ6nPgaM1s4r8jzX4rfsoeEfizqb6hcW9xpuqTHMt3YuI2nPq6EFWPvgH3rz2T/gnbpe47fFupKvYGwjOP/H6+jCPamtHk16OC4xznB0lSw9dqK2Ts7fejjxnC+V4ip7WrSTk/VfkfPln/wAE79BjVjP4m1qY9tkEUf8A8V/Sr9v/AME/vCMY/eap4ilPqJok/wDaZr3UDC4pR0refHOez3xD+Vv0Rzx4RymP/LhHi9l+wX4Dt/8AXN4guD23XwUD/vlBWxYfsZfDnTgv/EhkuGUYzPfTvu9yNwGfpXqFB6Vx1OK84npLEz/8CZ2w4cyyHw0I/dc43Sf2d/Auij/R/CPh/dxky2omJx6788+/Wuq0zSLXRbcw2dpaWcP/ADzt4ViX8lAFWAeKcDkV5OIx2Kr6VqkperbO+jgMNSd6VOMfRJDSSRzXO/F22+2fCHxdFjc0miXwA9T9nkx+uK6M9ag1CwXVtMubRiVS6heBiOoDqVJ/WowUlTxEJvo0/wATTFRc6E4LqmvwPzPiOY1PsKepyKlv9Pk0fULizmXbLZytBIP7rI20/qKhB4r+zaMlKCktmj+X6lPlk0+mg49KZOnmQsv95SKXORSk5p1NYNeTNMG2sTTa/mX5o+VJRslkUfwsR+RoHSiYj7RJ/vt/M0L0r+bq2lRrzZ/tdgJN4Wm3/KvyQUUUVmdQUUUUAFFFFACN0rd+GH/JTvDX/YXs/wD0elYTdK3fhh/yU7w1/wBhez/9HpQv6/A58V/Bl6Mm+Mv/ACWLxb/2HL3/ANKHrnAeK6H4w/N8YPFp/wCo3ff+lElc7jihq2jM8JpRgvJfkh2aN1NxyaMdP8KDo0HZozTQOPxoI/zigB2aM03H8vSgDpQA7NGaaRx+NBFAAePev0L/AOCOH7dngb4KfDfW/APjfXbXw3JJqp1LS7u8DLazLIirJG0mCEZWTI3YBD8cjFfnoB/L0psjGKCRh95VJHFa0ans5cyPneKuH6GcZdPB120viTW6a23+4/pStvD2oXAHl2dy+4ZB2HBHYg1o2vw/1a6XP2ZYl9ZHC/411fwu1j/hJfhn4Z1IsrtqGk2lyWH3SXhRjj25roBwnavfP4BrZ1iVJwSSadvuPln9s/wdeeHvhzps9x5IVtSVRtfdyY3Pp7V81da+wP8AgoYP+LR6T/2F0/8ARUtfH2cV/EPjv/yVU3/ch+R+2eHmLnWynnqvXmkvyHFyaIZGtzlMx/7pxTep9aP+A1+P06s4O8G0/J2PuJRjLcmkvZpE2tNMw95WOP1qIbSmNqn69aTqRQWwOtbfWcRUdnNv5tmXs6cdbJAY0J/1a/lTlwn8IpuQeN35VY07SrrWLgw2drcXUijJWGMyMB74qIUa1Sfs4Jt9ldsUpU4x5m0l6kJfd2pd/tS3VtLZXLQzRyQyKcMkiFWX6g1H07GoqQnCXLNWa76MqMoyXMth2c0jUnWgH61OhopI9o/Yg0VfEXxI1WzabyWbTjIrYz92Rf8AEV9HXvwx1KAbofJuV7hW2t+Rr5r/AGFr/wCyfH+3hL7VvNPuYsDuQFcf+gk19sxrgfrX9ueBNbn4XjBfZnNfin+p+C8eYyvhs4lGD0cYu34foeQ6zo114c0m8vr63kt7LT4ZLm5mYfJFGilmYkZ4Cgmvx0/4K8ft1+GP2tfFXhfQ/A91JqXhvwqs9xLqDRPCl5dS4T92rgMURF+8QMmQ44GT+2X7TV2tj+zb8RZmbb5fhfU3BzjkWkpr+YWwbdYwnbt/dqSMdOK/V8bKSiorqfqHgXgaOZYqtmGKXvUWlFdLyTTb9Oh7B+zDfr5OsWu75t8c6j2wVP8AIfnXquzJ/GvlrRtevPD16t1Y3E1rOoxvjOCR6H1Hsa/U7/gid8LfAf7UPwF8R33jbw3p/iLxJ4d142xuLtnObeSFHjBjVgmAfMHK849q+uyDi6jg8FHD14tuPY/N/pAeCeYVs2r8VYarH2VRwUou91KyjfZ6Ox8r2kLX9ysMCyXE7HCxxKZHJ6cKMnrxXXab+zz4/wBWETWvgXxlcCb/AFZTRrjD/Q7MV+w/g/4d+Hvh3B5Ph7w/oegx9CNPsYrbdj1KKCfxrc8yRurbh7nNdNbxAlf91S082fz7Q8Oo8t61bXyS/U+GP2VfhZ4o+EfwatbHxVpdzo11eXc93a2tzhZlhbaAXUfdJYOcHnGMgV6P/FXqP7R2mKbXS70fwu8DH1BAYflg15d6V+D8RYiVfM6leSs5O+nmfqGV4SOFwsMNF3UVa4tFFFeSegMkcRqzMVVVBYkkKFA6kk8fnXhXiz9vvwvoetSWunaZqetwRNta7jdYY5D6oGGWHuQAf1r1z4j+Hrjxb8Ptd0m1ZUutS0+e2hYnADujAZP1P86/Oma3ltLh4po3hmhYxujDBjZTggjsQQRX6b4e8K5fmvtamNbbjZKKdt+p8BxpxBjculTjhdFLd2v8j9DPhb8WdD+MXh/+0tEuXljjbZPDIuya2fGdrr/Ijg9u9ea/tD/tjWvwq1p9E0G1ttY1e3JF3LM7C3sz/cwvLv6gEBe/PA+Yfhd8Xdc+D19qN1oc/wBnm1KzazdjnCZIKyAf3052k9M1zDFnZmdpJGYlmZiWZiepJPUk9z1r7HL/AAuwtLMZ1MQ+ajpyx6v19D5jG+IOJqYOFOj7tX7TS/I+8f2bvjp/wvfwRNfzWcen6jp9x9mu4Y3LRsxUMrpnkAg9DnBB5NehrxXgH/BPXSpLb4Y65et/q7zVPLT/ALZxLn9Wr39a/I+LMFQwmbVsPhlaEXou2iv+J+lcO4mviMvpVsQ7ya3BjSZ/PrTicU0jatfOvY9t7Hnes/8ABJ5fjHdTeKYPHEmhz65e3V5PaTaULqNFedzGY2WVD9wrkMDz37CvqX/BFcmIfYfiV8+DuFzoGFPpgrcZ/SvtjwXYf2b4S0yDp5drGCPfaCf5mtTymI9q/YMv4ozSjQhTjPRJLVLsfIYnhPLK9SVWpT1k7vVo/OPX/wDgjp480kTSWfirwhfQRIXLyG4tmAHJyNjDge5z6V+fPiH9pCzexkj0mznmmkVlSaUhI1zkBwOSfUA496/ef9qHx8vwu/Zo+IXiJnKNovh2+u4yG2kOsD7cHsd2Ofev5trGNorSJWO7YoUt68V6FTjDM503DnWvkfr3g34LcN5tWrYzH0nJUnHlXM0m9Xrbe2mhJs2HmnDignI60hA/T1r5WUbu5/ckYqKUVshc0ZpABx/jQQMde9TylC5ozSEDn/GlA5/+vRyhcM0ZpMDjmjAx/wDXo5QBulbvwwP/ABc7w1/2F7P/ANHpWEe9bfww4+Jvhv8A7C9n/wCj0pNWOfFfwZej/I//2Q==");
	        mapData.put(2 + "/Certificate/Page/CZR", "张男");

            // 加载customDatas.xml，构建Ofd元数据信息对象
            Path pathMeta = Paths.get(sDataPath + "customDatas.xml").toAbsolutePath();
            InputStream isMetaXml = new FileInputStream(pathMeta.toFile());
            MetaInfoModel metaInfo = OfdCreateAgent.buildMetaInfo(isMetaXml);
            isMetaXml.close();


            logger.info("03-开始生成OFD文件：" + (System.currentTimeMillis() - start));
            // 单步生成OFD文件
            result = OfdCreateAgent.createOfd(sDataPath,                    //底图文件目录
                                    templateInfo,                     		//模板信息
                                    mapData,                           		//数据信息
                                    metaInfo,                       		//元数据信息
                                    sOutPath + outFilename);                //输出OFD文件

            if (result.getStatus() == 0) {
                logger.info("04-结束生成OFD文件：" + (System.currentTimeMillis() - start));
            } else {
                logger.info("04-失败生成OFD文件：" + result.getMessage());
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }
}