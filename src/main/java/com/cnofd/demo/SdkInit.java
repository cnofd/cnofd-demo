package com.cnofd.demo;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfdSdkInit;
import com.cnofd.demo.util.ConfigUtil;

/**
 * @Title: SdkInit.java
 * @Description: SDK初始化
 * @author cnofd
 * @date 2022年5月12日
 */
public class SdkInit {
    private static final Logger logger = LoggerFactory.getLogger(SdkInit.class);

    private static String cfgLicenseFile;
    private static String cfgTempDir;
    private static String cfgWorkDir;
    private static String cfgDataBasePath;
    private static String cfgOfficeEngine;
    private static String cfgHtmlEngine;
    private static String cfgPlunginPdf2WordClassName;

    private volatile static SdkInit instance;

    private SdkInit() {
    }

    // 单例模式
    public static SdkInit getInstance() {
        if (instance == null) {
            synchronized (SdkInit.class) {
                if (instance == null) {
                    instance = new SdkInit();

                    instance.init();
                }
            }
        }
        return instance;
    }

    // 读取配置文件，SDK初始化处理
    private boolean init() {
        try {
            long start = System.currentTimeMillis();
            logger.debug("01-启动：" + System.currentTimeMillis());

            // 读取配置文件
            Properties propConfig = ConfigUtil.getConfigProperties("init");
            cfgLicenseFile = ConfigUtil.getConfigKey(propConfig, "LicenseFile");
            cfgTempDir = ConfigUtil.getConfigKey(propConfig, "TempDir");
            cfgWorkDir = ConfigUtil.getConfigKey(propConfig, "WorkDir");
            cfgDataBasePath = ConfigUtil.getConfigKey(propConfig, "DataBasePath");

            // 输出sdk版本号
            String version = OfdSdkInit.getVersion();
            logger.info("sdk版本号：" + version);

            // 加载授权文件
            if (!OfdSdkInit.cnofdSdkInit(cfgLicenseFile)) {
                // 授权失败退出
                return false;
            }
            
            // 工作目录
            OfdSdkInit.setTempDir(cfgTempDir);
            OfdSdkInit.setWorkDir(cfgWorkDir);

            // 转换引擎及程序路径设置
            cfgOfficeEngine = ConfigUtil.getConfigKey(propConfig, "OfficeEngine");
            cfgHtmlEngine = ConfigUtil.getConfigKey(propConfig, "HtmlEngine");

            OfdSdkInit.setOfficeEngine(cfgOfficeEngine);
            OfdSdkInit.setHtmlEngine(cfgHtmlEngine);

            if ("LibreOffice".equals(cfgOfficeEngine)) {
                OfdSdkInit.setLibreOfficePath(ConfigUtil.getConfigKey(propConfig, "LibreOfficePath"));
            } else if ("OnlyOffice".equals(cfgOfficeEngine)) {
                OfdSdkInit.setOnlyOfficeServiceUrl(ConfigUtil.getConfigKey(propConfig, "OnlyOfficeServiceUrl"));
                OfdSdkInit.setFileUploadServiceUrl(ConfigUtil.getConfigKey(propConfig, "FileUploadServiceUrl"));
                OfdSdkInit.setFileDownloadServiceUrl(ConfigUtil.getConfigKey(propConfig, "FileDownloadServiceUrl"));
                OfdSdkInit.setFileDownloadDirRoot(ConfigUtil.getConfigKey(propConfig, "FileDownloadDirRoot"));
            }
            
            if ("Cdp4j".equals(cfgHtmlEngine)) {
                OfdSdkInit.setChromePath(ConfigUtil.getConfigKey(propConfig, "ChromePath"));
            }
            
            //设置Pdf2Word插件的类全名（包名+类名）
            cfgPlunginPdf2WordClassName = ConfigUtil.getConfigKey(propConfig, "PlunginPdf2wordClassName");
            OfdSdkInit.setPlunginPdf2WordClassName(cfgPlunginPdf2WordClassName);

            // 字体初始化
            OfdSdkInit.fontInit(null);
            //为不规范的字体名创建映射
            OfdSdkInit.fontAddAlias("微软雅黑", "MicrosoftYaHeiUI");

            logger.debug("02-完成初始化：" + (System.currentTimeMillis() - start));
            
            return true;
        } catch (Exception e) {
            logger.error("初始化异常！" + e.getMessage());
            return false;
        }
    }
    
    // 获取测试数据目录
    public static String getDataBasePath() {
        return cfgDataBasePath;
    }
}
