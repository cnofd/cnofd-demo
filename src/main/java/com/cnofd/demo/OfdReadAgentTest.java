package com.cnofd.demo;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfdReadAgent;

/**
 * @ClassName: OfdReadAgentTest
 * @Description: OFD解析功能测试
 * @author cnofd
 * @date 2022年1月22日
 */
public class OfdReadAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(OfdReadAgentTest.class);

    private static String cfgDataBasePath;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        logger.info("01-启动：" + System.currentTimeMillis());

        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        String sDataPath = cfgDataBasePath;

        // OFD解析
        try {
            OfdReadAgent agent = new OfdReadAgent();

            agent.ofdReadOpen(sDataPath + "cnofd-result-1.ofd");

            System.out.println("OFD文件公共元数据DocID：" + agent.getDocInfo("DocID"));
            System.out.println("OFD文件公共元数据CreationDate：" + agent.getDocInfo("CreationDate"));
            System.out.println("OFD文件公共元数据certificateName：" + agent.getCustomDatas("certificateName"));

            Map<String, String> map = agent.getCustomDatas();
            System.out.println("OFD文件用户自定义元数据certificateHolderName：" + map.get("certificateHolderName"));
            for (String key : map.keySet()) {
                System.out.println("OFD文件用户自定义元数据" + key + "：" + map.get(key));
            }

            System.out.println("OFD文件的页面数量：" + agent.getOfdPageCount());
            System.out.println("OFD文件是否包含数字签名：" + agent.hasSignature());

            System.out.println("OFD文件第1页的所有文本内容：" + agent.getPageContent(1));
            System.out.println("OFD文件第1页指定区域的文本内容：" + agent.getPageRegionText(1, 30, 30, 60, 60));

            agent.ofdReadClose();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //
        }
    }

}