package com.cnofd.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfficeConvertorAgent;
import com.cnofd.ofd.model.ResultInfo;

/**
 * 
 * @ClassName: OfficeConvertorAgentTest
 * @Description: Word、Excel、PPT转图片功能测试
 * @author cnofd
 * @date 2022年5月4日
 */
public class OfficeConvertorAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(OfficeConvertorAgentTest.class);

    private static String cfgDataBasePath;
    private static String sDataPath;
    private static String sOutPath;

    static long start = System.currentTimeMillis();

    public static void main(String[] args) {
        System.out.println("01-启动：" + System.currentTimeMillis());

        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        // 设置目录路径
        sDataPath = cfgDataBasePath + "DataOffice/";
        sOutPath = cfgDataBasePath + "Office2Image/";

        // Word转图片
        wordToImageTest();

        // Excel转图片
        excelToImageTest();

        // ppt转图片
        pptToImageTest();

    }

    /*
     * Word转图片
     */
    public static void wordToImageTest() {
        ResultInfo result = new ResultInfo();

        // Word转图片
        logger.info("03-开始Word转图片：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.word2image(sDataPath + "word-data-test.doc", sOutPath + "word-data-test", "JPG", 150);
        logger.info("03-结束Word转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

        // Word转多页Tiff
        logger.info("08-开始Word转多页Tiff：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.word2tiff(sDataPath + "word-data-test.doc", sOutPath + "word-data-test-tiff-150.tif", 150);
        logger.info("08-结束Word转多页Tiff：" + (System.currentTimeMillis() - start) + result.getMessage());

        // Word转长图-Jpg
        logger.info("02-开始Word转长图：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.word2longimage(sDataPath + "word-data-test.doc", sOutPath + "word-data-test-longimage-150.jpg", "JPG", 150);
        logger.info("02-结束Word转长图：" + (System.currentTimeMillis() - start) + result.getMessage());

        // Word转Gif动图
        logger.info("02-开始Word转Gif动图：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.word2gif(sDataPath + "word-data-test.doc", sOutPath + "word-data-test-gif-150.gif", 2000, 150);
        logger.info("02-结束Word转Gif动图：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /*
     * Excel转图片
     */
    public static void excelToImageTest() {
        ResultInfo result = new ResultInfo();

        logger.info("03-开始Excel转图片：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.excel2image(sDataPath + "excel-data-test.xlsx", sOutPath + "excel-data-test", "JPG", 150);
        logger.info("03-结束Excel转图片：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /*
     * PPT转图片
     */
    public static void pptToImageTest() {
        ResultInfo result = new ResultInfo();

        logger.info("03-开始PPT转图片：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.ppt2image(sDataPath + "ppt-data-test.pptx", sOutPath + "ppt-data-test", "JPG", 150);
        logger.info("03-结束PPT转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

        // PPT转多页Tiff
        logger.info("08-开始PPT转多页Tiff：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.ppt2tiff(sDataPath + "ppt-data-test.pptx", sOutPath + "ppt-data-test-tiff-150.tif", 150);
        logger.info("08-结束PPT转多页Tiff：" + (System.currentTimeMillis() - start) + result.getMessage());

        // PPT转长图-Jpg
        logger.info("02-开始PPT转长图：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.ppt2longimage(sDataPath + "ppt-data-test.pptx", sOutPath + "ppt-data-test-longimage-150.jpg", "JPG", 150);
        logger.info("02-结束PPT转长图：" + (System.currentTimeMillis() - start) + result.getMessage());

        // PPT转Gif动图
        logger.info("02-开始PPT转Gif动图：" + (System.currentTimeMillis() - start));
        result = OfficeConvertorAgent.ppt2gif(sDataPath + "ppt-data-test.pptx", sOutPath + "ppt-data-test-gif-150.gif", 2000, 150);
        logger.info("02-结束PPT转Gif动图：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

}