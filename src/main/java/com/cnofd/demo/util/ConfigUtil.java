package com.cnofd.demo.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * @ClassName: ConfigUtil
 * @Description: 获取配置信息工具类
 * @author cnofd
 * @date   2022年1月22日
 */
public class ConfigUtil {

    /**
     * 从指定的属性文件中获取指定属性的配置值
     * @param name
     * @param key
     * @return
     * @date   2022年1月22日
     */
    public static String getConfigKey(String name, String key) {
        String re = null;
        try {
            InputStream in = ConfigUtil.class.getClassLoader().getResourceAsStream(name + ".properties");
            Properties prop = new Properties(); // 属性集合对象
            prop.load(in); // 将属性文件流装载到Properties对象中
            re = prop.getProperty(key);
        } catch (Exception e) {
            throw new RuntimeException("配置读取错误" + e.toString());
        }
        return re;
    }
    
    /**
     * 从指定的属性文件中获取属性集合对象
     * 
     * @param name
     * @return
     * @date 2022年1月22日
     */
    public static Properties getConfigProperties(String name) {
        try {
            InputStream in = ConfigUtil.class.getClassLoader().getResourceAsStream(name + ".properties");
            Properties prop = new Properties(); // 属性集合对象
            prop.load(in); // 将属性文件流装载到Properties对象中

            return prop;
        } catch (Exception e) {
            throw new RuntimeException("配置读取错误" + e.toString());
        }
    }

    /**
     * 从指定的属性集合对象中获取指定属性的配置值
     * 
     * @param name
     * @param key
     * @return
     * @date 2022年1月22日
     */
    public static String getConfigKey(Properties prop, String key) {
        String re = null;
        re = prop.getProperty(key);

        // 如果未配置，使用默认配置值
        if (re == null || re.isEmpty()) {
            switch (key) {
            case "LicenseFile": // 授权文件
                re = "D:/cnofd_dev/license.txt";
                break;
            case "TempDir":// 临时目录
                re = "D:/cnofd_dev/tempdir/";
                break;
            case "WorkDir":// 工作目录
                re = "D:/cnofd_dev/workdir/";
                break;
            case "DataBasePath":// 测试数据目录
                re = "d:/cnofd_dev/testdata/";
                break;
            default:
                //
            }
        }

        return re;
    }
}