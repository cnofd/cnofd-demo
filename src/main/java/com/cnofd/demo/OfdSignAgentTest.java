package com.cnofd.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfdSignAgent;
import com.cnofd.ofd.model.ResultInfo;

/**
 * @ClassName: OfdSignAgentTest
 * @Description: OFD签名验签功能测试
 * @author cnofd
 * @date 2022年11月03日
 */
public class OfdSignAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(OfdSignAgentTest.class);

    private static String cfgDataBasePath;

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        logger.info("01-启动：" + System.currentTimeMillis());

        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        String sDataPath = cfgDataBasePath;
        
        ResultInfo result = new ResultInfo();
        
        // OFD文件验签
		result = OfdSignAgent.ofdSignVerify(sDataPath + "DataSign/电子发票-SealV4.ofd");
		logger.info(result.getMessage());

		result = OfdSignAgent.ofdSignVerify(sDataPath + "DataSign/铁路电子客票-Signed.ofd");
		logger.info(result.getMessage());

		result = OfdSignAgent.ofdSignVerify(sDataPath + "DataSign/铁路电子客票-Signed-改.ofd");
		logger.info(result.getMessage());
  
		result = OfdSignAgent.ofdSignVerify(sDataPath + "cnofd-result.ofd");
		logger.info(result.getMessage());
    }

}