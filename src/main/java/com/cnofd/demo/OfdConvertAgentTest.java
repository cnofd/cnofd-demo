package com.cnofd.demo;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cnofd.agent.OfdConvertAgent;
import com.cnofd.ofd.model.OfdPageInfo;
import com.cnofd.ofd.model.ResultInfo;

/**
 * @ClassName: OfdConvertAgentTest
 * @Description: OFD转Pdf、图片功能测试
 * @author cnofd
 * @date 2022年1月22日
 */
public class OfdConvertAgentTest {
    private static final Logger logger = LoggerFactory.getLogger(OfdConvertAgentTest.class);

    private static String cfgDataBasePath;
    private static String sDataPath;
    private static String sOutPath;

    static long start = System.currentTimeMillis();

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        logger.info("01-启动：" + System.currentTimeMillis());
        
        // 读取配置文件，SDK初始化处理
        SdkInit.getInstance();
        // 获取测试数据目录
        cfgDataBasePath = SdkInit.getDataBasePath();

        logger.info("02-完成初始化：" + (System.currentTimeMillis() - start));

        sDataPath = cfgDataBasePath;
        sOutPath = cfgDataBasePath;

        // OFD转Pdf
        ofdToPdfTest();

        // OFD转图片
        ofdToImageTest();

        // OFD转Html
        ofdToHtmlTest();

        // OFD转Word
        ofdToWordTest();
    }

    /*
     * OFD转Text
     */
    public static void ofdToPdfTest() {
        ResultInfo result = new ResultInfo();

        // OFD转PDF
        logger.info("11-开始OFD转PDF：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2pdf(sDataPath + "cnofd-result-1.ofd", sOutPath + "Ofd2Pdf/cnofd-result.pdf");
        logger.info("11-结束OFD转PDF：" + (System.currentTimeMillis() - start) + result.getMessage());
    }

    /*
     * OFD转图片
     */
    public static void ofdToImageTest() {
        String sDataPath = "testdata/DataOfd/";
        String sOutPath = "testdata/Ofd2Image/";

        ResultInfo result = new ResultInfo();

        // OFD转图片-指定DPI
        logger.info("21-开始OFD转图片：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2image(sDataPath + "cnofd-result-1.ofd", sOutPath + "OFD2JPG-DPI-150", "JPG", 150);
        logger.info("21-结束OFD转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

        // OFD转图片-Png
        logger.info("22-开始PDF转图片：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2image(sDataPath + "cnofd-result.ofd", sOutPath + "OFD2PNG", "PNG");
        logger.info("22-结束PDF转图片：" + (System.currentTimeMillis() - start) + result.getMessage());

        // OFD转多页Tiff
        logger.info("23-开始OFD转多页Tiff：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2tiff(sDataPath + "ppt-data-test.ofd", sOutPath + "ppt-data-test-tiff-150.tif", 150);
        logger.info("23-结束OFD转多页Tiff：" + (System.currentTimeMillis() - start) + result.getMessage());

        // OFD转长图-Jpg
        logger.info("24-开始OFD转长图：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2longimage(sDataPath + "ppt-data-test.ofd", sOutPath + "ppt-data-test-longimage-150.jpg", "JPG", 150);
        logger.info("24-结束OFD转长图：" + (System.currentTimeMillis() - start) + result.getMessage());

        // OFD转Gif动图
        logger.info("25-开始OFD转Gif动图：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2gif(sDataPath + "ppt-data-test.ofd", sOutPath + "ppt-data-test-gif-150.gif", 2000, 150);
        logger.info("25-结束OFD转Gif动图：" + (System.currentTimeMillis() - start) + result.getMessage());

    }

    /*
     * OFD转Html
     */
    public static void ofdToHtmlTest() {
        ResultInfo result = new ResultInfo();

        // OFD转Html，指定页面宽度
        logger.info("31-开始OFD转HTML：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2html(sDataPath + "cnofd-result.ofd", sOutPath + "Ofd2Html/cnofd-result.html", 1000);
        logger.info("31-结束OFD转HTML：" + (System.currentTimeMillis() - start) + result.getMessage());

        //OFD转Html，指定页面缩放比例
        logger.info("32-开始OFD转HTML：" + (System.currentTimeMillis() - start));           
        result = OfdConvertAgent.ofd2html(sDataPath + "cnofd-result-1.ofd", sOutPath + "Ofd2Html/cnofd-result-1.html", 1.0f);
        logger.info("32-结束OFD转HTML：" + (System.currentTimeMillis() - start) + result.getMessage()); 

        //OFD转SVG
        logger.info("33-开始OFD转SVG：" + (System.currentTimeMillis() - start));
        List<String> outPageSvg = new ArrayList<>();
        result = OfdConvertAgent.ofd2svg(sDataPath + "cnofd-result-1.ofd", outPageSvg, 1.0f);
        logger.info("33-结束OFD转SVG：" + (System.currentTimeMillis() - start) + result.getMessage()); 

        //OFD转SVG
        logger.info("34-开始OFD转SVG：" + (System.currentTimeMillis() - start));
        OfdPageInfo ofdPageInfo = new OfdPageInfo();
        result = OfdConvertAgent.ofd2svg(sDataPath + "cnofd-result-1.ofd", ofdPageInfo, 1.0f);
        logger.info("页数：" + ofdPageInfo.getPageCount()); 
        logger.info("第1页尺寸信息：" + ofdPageInfo.getListPageSize().get(0)[0] + ", " + ofdPageInfo.getListPageSize().get(0)[1]); 
        //logger.info("第1页SVG：" + ofdPageInfo.getListPageSvg().get(0)); 
        logger.info("34-结束OFD转SVG：" + (System.currentTimeMillis() - start) + result.getMessage()); 
    }

    /*
     * OFD转Word
     */
    public static void ofdToWordTest() {
        ResultInfo result = new ResultInfo();

        // OFD转Word
        logger.info("41-开始OFD转Word：" + (System.currentTimeMillis() - start));
        result = OfdConvertAgent.ofd2word(sDataPath + "cnofd-result.ofd", sOutPath + "Ofd2Word/cnofd-result.docx");
        logger.info("41-结束OFD转Word：" + (System.currentTimeMillis() - start) + result.getMessage());
    }
}