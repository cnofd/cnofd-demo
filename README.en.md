# cnofd

## 介绍
&emsp;**OFD**(Open Fixed-layout Document) 是我国自主可控的电子文件版式文档格式。OFD版式的呈现效果与设备无关，在各种设备上阅读、打印或印刷时版面固定，是电子文档发布、数字化信息传播和存档的理想文档格式。它具有安全、便携、开放、可扩展等特性，已广泛应用于电子公文、电子证照、电子档案、电子票据以及其它行业。

&emsp;**cnofd SDK**为程序开发人员提供符合标准、使用方便、功能强大的OFD技术软件开发工具包，实现OFD版式文件生成、编辑、转换、解析、签章等功能。开发人员通过示例代码可以快速上手，实现快速、高效地将软件扩展到多个平台。

&emsp;**cnofd-demo**是cnofd SDK的Demo工程，提供主要API方法的开发样例，有利于开发者快速入门。应用系统开发人员只需关注业务处置即可完成集成开发，大大缩短开发上线周期。

## 快速上手
<ul dir="auto">
<li>
<p dir="auto">1.加载jar包（cnofd-demo\lib\cnofd-base-1.4-release.jar）到本地maven仓库，或者进入“cnofd-demo\lib\”文件夹，执行”JAR包手动添加到Maven仓库.bat”</p>
</li>
<li>
<p dir="auto">2.直接导入构建maven项目。如Eclipse：File/Import...，选择“Maven/Existing Maven Projects”。</p>
</li>
</ul>

## 技术文档
<ul dir="auto">
<li>
<p dir="auto">技术官网/在线演示：  <a href="http://www.cnofd.cn:18080" target="view_window" rel="nofollow">http://www.cnofd.cn:18080/</a></p>
</li>
</ul>

## 交流互动
<ul dir="auto">
<li>
<p dir="auto">QQ交流群：</p>
</li>
</ul>

&emsp;![avatar](./img/QQLink.png)

## 为什么选择cnofd？
<ul dir="auto">
<li>
<p dir="auto">1.<B>基础语言、开发简便</B>。采用Java主流语言开发，提供丰富的API接口。</p>
</li>
<li>
<p dir="auto">2.<B>适配全面、跨平台兼容</B>。适用于Windows、Linux、macOS以及信创环境下的多种平台，真正实现“一次开发、多平台适配”。</p>
</li>
<li>
<p dir="auto">3.<B>接口完善、快速开发</B>。提供完善的接口手册和开发样例，有利于开发者快速入门，应用系统开发人员只需关注业务处置即可完成集成开发，大大缩短开发上线周期。</p>
</li>
<li>
<p dir="auto">4.<B>套版生成强大易用</B>。丰富的文本样式与排版，字体大小自适应，支持图像元素，便捷绘制表格，满足复杂版面设计需要。</p>
</li>
<li>
<p dir="auto">5.<B>主流文件格式互转</B>。支持Word、Excel、PPT、Html、图片等文件转成PDF和OFD，支持PDF和OFD互转，支持OFD、PDF、Word等转各类图片。</p>
</li>
</ul>


## 功能特性
&emsp;![avatar](./img/cnofd-features-1.png)
<B>文件格式转换</B>
&emsp;![avatar](./img/cnofd-features-2.png)



## 部分截图
&emsp;![avatar](./img/website-01.png)

&emsp;![avatar](./img/website-02.png)

&emsp;![avatar](./img/website-03.png)

